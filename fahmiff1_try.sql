-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 08, 2021 at 04:25 PM
-- Server version: 5.7.33-log
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fahmiff1_try`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `id_admin` varchar(5) NOT NULL,
  `gambar_admin` varchar(128) NOT NULL DEFAULT 'user_male.svg',
  `nama_admin` varchar(128) NOT NULL,
  `tanggal_lahir` varchar(10) DEFAULT NULL,
  `email` varchar(64) NOT NULL,
  `nomor_hp` varchar(15) DEFAULT NULL,
  `pin_login` int(6) DEFAULT NULL,
  `permission` enum('admin','superadmin') NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_bukti_pembayaran`
--

CREATE TABLE `tbl_bukti_pembayaran` (
  `id_bukti_pembayaran` int(11) NOT NULL,
  `id_pesanan` varchar(10) NOT NULL,
  `id_pengguna` varchar(10) NOT NULL,
  `bukti_gambar` varchar(32) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_bukti_pembayaran`
--

INSERT INTO `tbl_bukti_pembayaran` (`id_bukti_pembayaran`, `id_pesanan`, `id_pengguna`, `bukti_gambar`, `created_at`) VALUES
(13, '5013580427', '101', 'bukti_pembayaran_20881.jpg', '2021-04-07 02:51:05'),
(14, '4559078551', '101', 'bukti_pembayaran_6395.png', '2021-04-07 03:17:36'),
(15, '6866936251', '101', 'bukti_pembayaran_13741.png', '2021-04-07 03:18:42');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_favorite_produk`
--

CREATE TABLE `tbl_favorite_produk` (
  `id_fav` int(10) NOT NULL,
  `id_pengguna` varchar(10) NOT NULL,
  `id_produk` varchar(10) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_favorite_produk`
--

INSERT INTO `tbl_favorite_produk` (`id_fav`, `id_pengguna`, `id_produk`, `created_at`) VALUES
(1, '123123', '2959067551', '2021-04-06 21:08:17'),
(2, '101', '2959067551', '2021-04-07 01:21:21'),
(3, '101', '2959067551', '2021-04-07 01:21:21');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_favorite_toko`
--

CREATE TABLE `tbl_favorite_toko` (
  `id_favorite_toko` int(11) NOT NULL,
  `id_pengguna` varchar(10) NOT NULL,
  `id_toko` varchar(10) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_favorite_toko`
--

INSERT INTO `tbl_favorite_toko` (`id_favorite_toko`, `id_pengguna`, `id_toko`, `created_at`) VALUES
(1, '101', '8265838634', '2021-04-06 22:46:45');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_history_transaksi`
--

CREATE TABLE `tbl_history_transaksi` (
  `id_history` varchar(10) NOT NULL,
  `id_pesanan` varchar(10) NOT NULL,
  `status` enum('pending','selesai','dibatalkan') NOT NULL DEFAULT 'pending',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_history_transaksi`
--

INSERT INTO `tbl_history_transaksi` (`id_history`, `id_pesanan`, `status`, `is_deleted`, `created_at`) VALUES
('3398088552', '5013580427', 'pending', 0, '2021-04-07 02:50:56'),
('5288745773', '6919013064', 'pending', 0, '2021-04-07 09:59:22'),
('6191834444', '6866936251', 'pending', 0, '2021-04-07 03:18:36'),
('6463021375', '4283345593', 'pending', 0, '2021-04-07 22:30:03'),
('8122495487', '4559078551', 'pending', 0, '2021-04-07 03:17:09');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_komplain_produk`
--

CREATE TABLE `tbl_komplain_produk` (
  `id_komplain` varchar(10) NOT NULL,
  `id_pesanan` varchar(10) NOT NULL,
  `alasan` enum('Tidak Sesuai','Palsu','Belum Diterima') DEFAULT NULL,
  `alasan_detail` text,
  `status_laporan` enum('tutup','berlangsung') NOT NULL DEFAULT 'berlangsung',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_komplain_produk`
--

INSERT INTO `tbl_komplain_produk` (`id_komplain`, `id_pesanan`, `alasan`, `alasan_detail`, `status_laporan`, `created_at`) VALUES
('K16529', '4559078551', 'Belum Diterima', 'asap', 'berlangsung', '2021-04-07 05:37:41'),
('K31987', '6919013064', 'Palsu', 'lorem', 'berlangsung', '2021-04-07 10:00:25');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_konversasi_komplain`
--

CREATE TABLE `tbl_konversasi_komplain` (
  `id` int(11) NOT NULL,
  `id_komplain` varchar(10) NOT NULL,
  `id_pengirim` varchar(10) NOT NULL COMMENT '= id_pengguna',
  `pengirim` enum('pembeli','toko') NOT NULL DEFAULT 'pembeli',
  `text` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_laporan_toko`
--

CREATE TABLE `tbl_laporan_toko` (
  `id_laporan_toko` varchar(10) NOT NULL,
  `id_pengguna` varchar(10) NOT NULL,
  `id_toko` varchar(10) NOT NULL,
  `deskripsi_laporan` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_notifikasi`
--

CREATE TABLE `tbl_notifikasi` (
  `id_notifikasi` varchar(10) NOT NULL,
  `id_pengguna` varchar(10) NOT NULL,
  `title` enum('Pesanan Dibatalkan','Pesanan Dikirim','Pesanan Baru','Dana Telah Dikirim') NOT NULL,
  `keterangan` varchar(64) NOT NULL,
  `seen` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pengajuan_dana`
--

CREATE TABLE `tbl_pengajuan_dana` (
  `id_pengajuan` varchar(10) NOT NULL,
  `id_toko` varchar(10) NOT NULL,
  `nominal` int(11) NOT NULL,
  `status` enum('pending','sedang diproses','diterima') NOT NULL DEFAULT 'pending',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_pengajuan_dana`
--

INSERT INTO `tbl_pengajuan_dana` (`id_pengajuan`, `id_toko`, `nominal`, `status`, `created_at`) VALUES
('1', '8265838634', 10000, 'pending', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pengguna`
--

CREATE TABLE `tbl_pengguna` (
  `id_pengguna` varchar(10) NOT NULL,
  `nama_pengguna` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `nomor_hp` varchar(15) DEFAULT NULL,
  `pin_login` int(6) DEFAULT NULL,
  `password` varchar(128) NOT NULL,
  `gambar_pengguna` varchar(128) NOT NULL DEFAULT 'user_default.svg',
  `idcard_pengguna` varchar(128) DEFAULT NULL,
  `alamat` text,
  `nama_perusahaan` varchar(64) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 = true || 0 = false',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_pengguna`
--

INSERT INTO `tbl_pengguna` (`id_pengguna`, `nama_pengguna`, `email`, `nomor_hp`, `pin_login`, `password`, `gambar_pengguna`, `idcard_pengguna`, `alamat`, `nama_perusahaan`, `is_deleted`, `created_at`) VALUES
('101', 'fa', 'fa', NULL, NULL, '$2y$10$bQ1WKLq00dELE09E6agQEu9w1CBe7Xjai1UsKbxaCU4.4.Lx9efXa', 'user_default.svg', NULL, NULL, NULL, 0, '0000-00-00 00:00:00'),
('123123', 'asdasd', 'user 2', NULL, NULL, '$2y$10$6r8eLiA.cq2tTL6.n293w.GmO6admqyy3R9ikeGptAPQULn1cynoa', 'profil_5.jpg', NULL, 'Alamat', NULL, 0, '2021-03-10 15:32:31'),
('201', 'kiki', 'kiki', NULL, NULL, '$2y$10$bQ1WKLq00dELE09E6agQEu9w1CBe7Xjai1UsKbxaCU4.4.Lx9efXa', 'user_default.svg', NULL, NULL, NULL, 0, '0000-00-00 00:00:00'),
('5', 'Alma Prayuda', 'user', NULL, NULL, '$2y$10$6r8eLiA.cq2tTL6.n293w.GmO6admqyy3R9ikeGptAPQULn1cynoa', 'profil_5.png', NULL, 'Alamat', NULL, 0, '2021-03-10 15:32:31');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pengguna_otp`
--

CREATE TABLE `tbl_pengguna_otp` (
  `otp` int(6) NOT NULL,
  `email` varchar(128) NOT NULL,
  `is_validated` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 = true || 0 = false',
  `expired` varchar(15) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_pengguna_otp`
--

INSERT INTO `tbl_pengguna_otp` (`otp`, `email`, `is_validated`, `expired`, `created_at`) VALUES
(484005, 'qwertxyz01@gmail.com', 1, '1615726246', '2021-03-14 19:45:46'),
(633720, 'qwertxyz01@gmail.com', 1, '1615726137', '2021-03-14 19:43:57');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pengguna_pesanan`
--

CREATE TABLE `tbl_pengguna_pesanan` (
  `id_pesanan` varchar(10) NOT NULL,
  `id_pengguna` varchar(10) NOT NULL COMMENT 'as pemesan',
  `id_produk_post` varchar(10) NOT NULL,
  `harga_produk` int(11) NOT NULL,
  `total_harga` int(11) NOT NULL,
  `status_pesanan` enum('menunggu_pembayaran','sedang_diproses','pesanan_selesai','dibatalkan','verifikasi_pembayaran') NOT NULL DEFAULT 'menunggu_pembayaran',
  `dibatalkan_oleh` enum('pemesan','admin') DEFAULT NULL,
  `metode_pembayaran` enum('credit_card') NOT NULL,
  `nama_perusahaan` varchar(64) DEFAULT NULL,
  `tgl_diterima` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_pengguna_pesanan`
--

INSERT INTO `tbl_pengguna_pesanan` (`id_pesanan`, `id_pengguna`, `id_produk_post`, `harga_produk`, `total_harga`, `status_pesanan`, `dibatalkan_oleh`, `metode_pembayaran`, `nama_perusahaan`, `tgl_diterima`, `created_at`) VALUES
('4283345593', '123123', '6437984262', 50000, 50593, 'pesanan_selesai', NULL, 'credit_card', 'lorem', '2021-04-07 22:30:19', '2021-04-07 22:30:03'),
('4559078551', '101', '6437984262', 50000, 50551, 'pesanan_selesai', NULL, 'credit_card', 'pure', '2021-04-07 03:47:15', '2021-04-07 03:17:09'),
('5013580427', '101', '2959067551', 50000, 50427, 'pesanan_selesai', NULL, 'credit_card', 'Inds', '2021-04-07 03:47:21', '2021-04-07 02:50:56'),
('6866936251', '101', '2959067551', 50000, 50251, 'pesanan_selesai', NULL, 'credit_card', 'ok', '2021-04-07 03:46:26', '2021-04-07 03:18:36'),
('6919013064', '5', '6437984262', 50000, 50064, 'pesanan_selesai', NULL, 'credit_card', 'asd', '2021-04-07 09:59:37', '2021-04-07 09:59:22');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pengguna_toko`
--

CREATE TABLE `tbl_pengguna_toko` (
  `id_toko` varchar(10) NOT NULL,
  `id_pengguna` varchar(10) NOT NULL,
  `gambar_toko` varchar(64) DEFAULT NULL,
  `nama_toko` varchar(64) NOT NULL,
  `alamat_toko` text NOT NULL,
  `deskripsi_toko` text,
  `saldo_toko` bigint(20) NOT NULL,
  `is_blocked` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_pengguna_toko`
--

INSERT INTO `tbl_pengguna_toko` (`id_toko`, `id_pengguna`, `gambar_toko`, `nama_toko`, `alamat_toko`, `deskripsi_toko`, `saldo_toko`, `is_blocked`, `created_at`) VALUES
('8265838634', '5', 'user_default.svg', 'toko lorem', 'Alamat', '[removed]alert&#40;1&#41;;[removed]', 700000, 0, '2021-03-11 23:29:15');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_produk_gambar`
--

CREATE TABLE `tbl_produk_gambar` (
  `id_produk_gambar` varchar(10) NOT NULL,
  `id_produk_post` varchar(10) NOT NULL,
  `gambar_produk` varchar(128) NOT NULL COMMENT 'root path = assets/pengguna/toko/produk/',
  `is_thumbnail` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_produk_gambar`
--

INSERT INTO `tbl_produk_gambar` (`id_produk_gambar`, `id_produk_post`, `gambar_produk`, `is_thumbnail`, `created_at`) VALUES
('1568221990', '371738731', 'produk_580096045.jpg', 0, '2021-03-20 18:56:03'),
('2792592803', '4640471432', 'produk_366034249.png', 1, '2021-03-22 23:21:16'),
('3207220788', '2959067551', 'produk_1607926396.jpg', 1, '2021-03-22 23:21:16'),
('3741139467', '138743766', 'produk_449462190.png', 0, '2021-03-22 23:37:53'),
('3805088242', '138743766', 'produk_1162670193.png', 1, '2021-03-22 23:12:15'),
('3942495069', '2959067551', 'produk_2096040466.png', 0, '2021-03-22 23:21:16'),
('4037894204', '7549250733', 'produk_1564077282.png', 1, '2021-03-22 23:12:52'),
('4314553177', '6437984262', 'produk_1682726590.png', 1, '2021-03-22 23:13:31'),
('5467762638', '6437984262', 'produk_1928268283.png', 0, '2021-03-22 23:13:31'),
('6791154580', '371738731', 'produk_2061719263.jpg', 1, '2021-03-22 23:33:37'),
('8036737415', '138743762', 'produk_1913014579.png', 1, '2021-03-21 21:42:08'),
('8136439046', '4640471432', 'produk_573095784.png', 0, '2021-03-22 23:33:37'),
('8466299684', '6437984262', 'produk_1512265982.png', 0, '2021-03-22 23:13:31'),
('9819221176', '4640471432', 'produk_1324265590.png', 0, '2021-03-22 23:33:37');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_produk_kategori`
--

CREATE TABLE `tbl_produk_kategori` (
  `id_kategori` varchar(5) NOT NULL,
  `nama_kategori` varchar(64) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_produk_kategori`
--

INSERT INTO `tbl_produk_kategori` (`id_kategori`, `nama_kategori`, `created_at`) VALUES
('12342', 'Kaos', '2021-03-12 14:48:36'),
('45734', 'Elektronik', '2021-03-12 14:48:28'),
('51341', 'Aset Digital', '2021-03-12 14:42:45'),
('92345', 'Lainnya', '2021-03-12 14:48:35');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_produk_post`
--

CREATE TABLE `tbl_produk_post` (
  `id_produk_post` varchar(10) NOT NULL,
  `id_kategori` varchar(5) DEFAULT NULL,
  `id_toko` varchar(10) NOT NULL,
  `nama_produk` varchar(128) NOT NULL,
  `deskripsi_produk` text NOT NULL,
  `harga_produk` bigint(20) NOT NULL,
  `terjual` int(11) NOT NULL,
  `link_demo` varchar(64) DEFAULT NULL,
  `link_file` varchar(64) NOT NULL,
  `status_post` enum('aktif','nonaktif') NOT NULL,
  `status_live` enum('verifikasi','live','ditolak') NOT NULL DEFAULT 'verifikasi',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_produk_post`
--

INSERT INTO `tbl_produk_post` (`id_produk_post`, `id_kategori`, `id_toko`, `nama_produk`, `deskripsi_produk`, `harga_produk`, `terjual`, `link_demo`, `link_file`, `status_post`, `status_live`, `is_deleted`, `created_at`) VALUES
('138743762', '51341', '8265838634', 'sepatu', 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Hic, omnis ducimus vel ipsa ipsam magni alias nobis maiores? Cum quae earum modi cupiditate voluptatem rem commodi cumque facilis aliquid amet.', 30000, 0, NULL, '', 'aktif', 'live', 0, '2021-03-12 18:08:59'),
('138743766', '51341', '8265838634', 'sepatu', 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Hic, omnis ducimus vel ipsa ipsam magni alias nobis maiores? Cum quae earum modi cupiditate voluptatem rem commodi cumque facilis aliquid amet.', 50000, 0, NULL, '', 'aktif', 'live', 0, '2021-03-12 18:08:59'),
('2959067551', '12342', '8265838634', 'sepatu', '', 50000, 5, NULL, '', 'aktif', 'live', 0, '2021-03-22 23:21:16'),
('371738731', '51341', '8265838634', 'gambar', 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Hic, omnis ducimus vel ipsa ipsam magni alias nobis maiores? Cum quae earum modi cupiditate voluptatem rem commodi cumque facilis aliquid amet.', 50000, 0, NULL, '', 'aktif', 'live', 0, '2021-03-12 18:08:59'),
('4640471432', '45734', '8265838634', 'sepatu', '', 50000, 0, NULL, '', 'aktif', 'live', 0, '2021-03-22 23:33:37'),
('6437984262', '12342', '8265838634', 'sepatu', '', 50000, 4, NULL, '', 'aktif', 'live', 0, '2021-03-22 23:13:31'),
('7549250733', '12342', '8265838634', 'sepatu', '', 50000, 0, NULL, '', 'aktif', 'live', 0, '2021-03-22 23:12:52');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_produk_rating`
--

CREATE TABLE `tbl_produk_rating` (
  `id_rating` varchar(10) NOT NULL,
  `id_produk_post` varchar(10) NOT NULL,
  `id_pengguna` varchar(10) NOT NULL,
  `rate` tinyint(1) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_produk_rating`
--

INSERT INTO `tbl_produk_rating` (`id_rating`, `id_produk_post`, `id_pengguna`, `rate`, `created_at`) VALUES
('05279', '6437984262', '123123', 1, '2021-04-08 10:31:29'),
('63589', '6437984262', '101', 3, '2021-04-07 05:06:53');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_produk_ulasan`
--

CREATE TABLE `tbl_produk_ulasan` (
  `id_ulasan` varchar(10) NOT NULL,
  `id_produk_post` varchar(10) NOT NULL,
  `bintang` tinyint(1) NOT NULL COMMENT 'value-nya hanya 1-5'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `tbl_bukti_pembayaran`
--
ALTER TABLE `tbl_bukti_pembayaran`
  ADD PRIMARY KEY (`id_bukti_pembayaran`),
  ADD KEY `id_pengguna_fktbp` (`id_pengguna`),
  ADD KEY `id_pesanan` (`id_pesanan`);

--
-- Indexes for table `tbl_favorite_produk`
--
ALTER TABLE `tbl_favorite_produk`
  ADD PRIMARY KEY (`id_fav`),
  ADD KEY `id_pengguna` (`id_pengguna`),
  ADD KEY `id_produk` (`id_produk`);

--
-- Indexes for table `tbl_favorite_toko`
--
ALTER TABLE `tbl_favorite_toko`
  ADD PRIMARY KEY (`id_favorite_toko`),
  ADD KEY `id_pengguna` (`id_pengguna`),
  ADD KEY `id_toko` (`id_toko`);

--
-- Indexes for table `tbl_history_transaksi`
--
ALTER TABLE `tbl_history_transaksi`
  ADD PRIMARY KEY (`id_history`),
  ADD KEY `id_pesanan` (`id_pesanan`);

--
-- Indexes for table `tbl_komplain_produk`
--
ALTER TABLE `tbl_komplain_produk`
  ADD PRIMARY KEY (`id_komplain`),
  ADD KEY `id_pesanan_fktkp` (`id_pesanan`);

--
-- Indexes for table `tbl_konversasi_komplain`
--
ALTER TABLE `tbl_konversasi_komplain`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_komplain` (`id_komplain`),
  ADD KEY `id_pengirim_fktkk` (`id_pengirim`);

--
-- Indexes for table `tbl_laporan_toko`
--
ALTER TABLE `tbl_laporan_toko`
  ADD PRIMARY KEY (`id_laporan_toko`),
  ADD KEY `id_pengguna` (`id_pengguna`),
  ADD KEY `id_toko` (`id_toko`);

--
-- Indexes for table `tbl_notifikasi`
--
ALTER TABLE `tbl_notifikasi`
  ADD PRIMARY KEY (`id_notifikasi`),
  ADD KEY `id_pengguna` (`id_pengguna`);

--
-- Indexes for table `tbl_pengajuan_dana`
--
ALTER TABLE `tbl_pengajuan_dana`
  ADD PRIMARY KEY (`id_pengajuan`),
  ADD KEY `id_toko` (`id_toko`);

--
-- Indexes for table `tbl_pengguna`
--
ALTER TABLE `tbl_pengguna`
  ADD PRIMARY KEY (`id_pengguna`);

--
-- Indexes for table `tbl_pengguna_otp`
--
ALTER TABLE `tbl_pengguna_otp`
  ADD PRIMARY KEY (`otp`);

--
-- Indexes for table `tbl_pengguna_pesanan`
--
ALTER TABLE `tbl_pengguna_pesanan`
  ADD PRIMARY KEY (`id_pesanan`),
  ADD KEY `id_produk_post` (`id_produk_post`),
  ADD KEY `id_pengguna` (`id_pengguna`);

--
-- Indexes for table `tbl_pengguna_toko`
--
ALTER TABLE `tbl_pengguna_toko`
  ADD PRIMARY KEY (`id_toko`),
  ADD KEY `id_pengguna` (`id_pengguna`);

--
-- Indexes for table `tbl_produk_gambar`
--
ALTER TABLE `tbl_produk_gambar`
  ADD PRIMARY KEY (`id_produk_gambar`),
  ADD KEY `id_produk_post` (`id_produk_post`);

--
-- Indexes for table `tbl_produk_kategori`
--
ALTER TABLE `tbl_produk_kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `tbl_produk_post`
--
ALTER TABLE `tbl_produk_post`
  ADD PRIMARY KEY (`id_produk_post`),
  ADD KEY `id_kategori` (`id_kategori`),
  ADD KEY `id_toko` (`id_toko`);

--
-- Indexes for table `tbl_produk_rating`
--
ALTER TABLE `tbl_produk_rating`
  ADD PRIMARY KEY (`id_rating`);

--
-- Indexes for table `tbl_produk_ulasan`
--
ALTER TABLE `tbl_produk_ulasan`
  ADD PRIMARY KEY (`id_ulasan`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_bukti_pembayaran`
--
ALTER TABLE `tbl_bukti_pembayaran`
  MODIFY `id_bukti_pembayaran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `tbl_favorite_produk`
--
ALTER TABLE `tbl_favorite_produk`
  MODIFY `id_fav` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_favorite_toko`
--
ALTER TABLE `tbl_favorite_toko`
  MODIFY `id_favorite_toko` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_konversasi_komplain`
--
ALTER TABLE `tbl_konversasi_komplain`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_bukti_pembayaran`
--
ALTER TABLE `tbl_bukti_pembayaran`
  ADD CONSTRAINT `id_pengguna_fktbp` FOREIGN KEY (`id_pengguna`) REFERENCES `tbl_pengguna` (`id_pengguna`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `id_pesanan_fktbp` FOREIGN KEY (`id_pesanan`) REFERENCES `tbl_pengguna_pesanan` (`id_pesanan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_favorite_produk`
--
ALTER TABLE `tbl_favorite_produk`
  ADD CONSTRAINT `id_pengguna_fktfp` FOREIGN KEY (`id_pengguna`) REFERENCES `tbl_pengguna` (`id_pengguna`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `id_produk_fktfp` FOREIGN KEY (`id_produk`) REFERENCES `tbl_produk_post` (`id_produk_post`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_history_transaksi`
--
ALTER TABLE `tbl_history_transaksi`
  ADD CONSTRAINT `id_pesanan_fktht` FOREIGN KEY (`id_pesanan`) REFERENCES `tbl_pengguna_pesanan` (`id_pesanan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_komplain_produk`
--
ALTER TABLE `tbl_komplain_produk`
  ADD CONSTRAINT `id_pesanan_fktkp` FOREIGN KEY (`id_pesanan`) REFERENCES `tbl_pengguna_pesanan` (`id_pesanan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_konversasi_komplain`
--
ALTER TABLE `tbl_konversasi_komplain`
  ADD CONSTRAINT `id_komplain` FOREIGN KEY (`id_komplain`) REFERENCES `tbl_komplain_produk` (`id_komplain`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `id_pengirim_fktkk` FOREIGN KEY (`id_pengirim`) REFERENCES `tbl_pengguna` (`id_pengguna`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_notifikasi`
--
ALTER TABLE `tbl_notifikasi`
  ADD CONSTRAINT `id_pengguna_fktn` FOREIGN KEY (`id_pengguna`) REFERENCES `tbl_pengguna` (`id_pengguna`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_pengajuan_dana`
--
ALTER TABLE `tbl_pengajuan_dana`
  ADD CONSTRAINT `id_toko_fktpd` FOREIGN KEY (`id_toko`) REFERENCES `tbl_pengguna_toko` (`id_toko`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_pengguna_pesanan`
--
ALTER TABLE `tbl_pengguna_pesanan`
  ADD CONSTRAINT `id_pengguna_fktppes` FOREIGN KEY (`id_pengguna`) REFERENCES `tbl_pengguna` (`id_pengguna`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `id_produk_post_tppes` FOREIGN KEY (`id_produk_post`) REFERENCES `tbl_produk_post` (`id_produk_post`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_pengguna_toko`
--
ALTER TABLE `tbl_pengguna_toko`
  ADD CONSTRAINT `id_pengguna_fktpt` FOREIGN KEY (`id_pengguna`) REFERENCES `tbl_pengguna` (`id_pengguna`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_produk_gambar`
--
ALTER TABLE `tbl_produk_gambar`
  ADD CONSTRAINT `id_produk_post_fktpg` FOREIGN KEY (`id_produk_post`) REFERENCES `tbl_produk_post` (`id_produk_post`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_produk_post`
--
ALTER TABLE `tbl_produk_post`
  ADD CONSTRAINT `id_kategori_fktpp` FOREIGN KEY (`id_kategori`) REFERENCES `tbl_produk_kategori` (`id_kategori`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `id_toko_fktpp` FOREIGN KEY (`id_toko`) REFERENCES `tbl_pengguna_toko` (`id_toko`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
