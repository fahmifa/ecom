$(function () {
    /* ChartJS
     * -------
     * Data and config for chartjs
     */
    'use strict';
    var areaData = {
        labels: ["2013", "2014", "2015", "2016", "2017", "2017", "2017", "2017", "2017", "2017", "2017", "2017", "2017", "2017", "2017", "2017"],
        datasets: [
            {
                label: '#Humidity',
                data: [15, 8, 18, 5, 14, 12, 15, 8, 15, 8, 18, 5, 14, 12, 15, 8, 15, 8, 18, 5, 14, 12, 15, 8, 15, 8, 18, 5, 14, 12, 15],
                backgroundColor: [
                    'rgb(255, 255, 255, .5)'
                ],
                borderColor: '#E84545',
                borderWidth: 2,
                lineTension: 0,
                fill: 1
            },
            {
                label: '#Temperature',
                data: [14, 7, 18, 5, 14, 12, 15, 8, 18, 5, 14, 12, 15, 8, 18, 5, 14, 12, 15, 8, 18, 5, 14, 12, 15, 8, 18, 5, 14, 12, 19],
                backgroundColor: [
                    'rgb(255, 255, 255, .5)'
                ],
                borderColor: '#892CDC',
                borderWidth: 2,
                lineTension: 0,
                fill: -1
            },
        ]
    };

    var areaOptions = {
        plugins: {
            filler: {
                propagate: true
            }
        },
        scales: {
            yAxes: [
                {
                    gridLines: {
                        color: "rgba(204, 204, 204,0.1)"
                    },
                }],
            xAxes: [{
                gridLines: {
                    color: "rgba(204, 204, 204,0.1)"
                }
            }]
        }
    }

    // Get context with jQuery - using jQuery's .get() method.
    if ($("#areaChart").length) {
        var areaChartCanvas = $("#areaChart").get(0).getContext("2d");
        new Chart(areaChartCanvas, {
            type: 'line',
            data: areaData,
            options: areaOptions
        });
    }

});