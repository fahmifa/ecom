/*
    Syarat Wajib:
    - jquery-loading.js
*/


function formatNumber(angka) {
    let number_string = parseFloat(angka.toString().replace(/[^,\d]/g, "")).toString(),
        split = number_string.split(","),
        sisa = split[0].length % 3,
        number = split[0].substr(0, sisa),
        ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
        separator = sisa ? "." : "";
        number += separator + ribuan.join(".");
    }

    return number;
}

function getNumber(string) {
    let result = parseFloat(string.replace(/[^,\d]/g, '').toString());
    return result;
}

function loadingStart(msg = 'Loading...') {
    $('body').loading({
        message: msg
    });
}

function loadingStop() {
    $('body').loading('stop');
}

function sweetalert(icon, title, msg) {
    Swal.fire({
        icon: icon,
        title: title,
        text: msg,
    });
}