<?php

if (!function_exists('userdata')) {
    function userdata($row)
    {
        $ci = &get_instance();
        $ci->load->model("auth/m_auth", "auth");
        $email = $ci->session->email;
        $user_data = $ci->auth->getWhereEmail($email)->row_array();

        if (!empty($user_data)) {
            return $user_data[$row];
        } else {
            return false;
        }
    }
}



function sold($d)
{
    // ambil terjual tanpa status
    $ci = &get_instance();
    $ci->load->model("produk/m_produk", "produk");
    $cc = $ci->produk->check('tbl_pengguna_pesanan', ['id_produk_post' => $d]);
    if ($cc) {
        return count($cc);
    } else {
        return 0;
    }
}

function rating($d)
{
    $ci = &get_instance();
    $ci->load->model("produk/m_produk", "produk");

    $cc = $ci->produk->avg('tbl_produk_rating', ['id_produk_post' => $d], 'rate');
    return round($cc[0]->rate);
}

function gambar($d)
{
    $ci = &get_instance();
    $ci->load->model("produk/m_produk", "produk");
    $cc = $ci->produk->check('tbl_produk_gambar', ['id_produk_post' => $d]);
    return $cc;
}


function comp($da)
{
    $ci = &get_instance();
    $ci->load->model("produk/m_produk", "produk");
    $cc = $ci->produk->check('tbl_komplain_produk', ['id_pesanan' => $da]);
    return $cc;
}

// if (!function_exists('admindata')) {
//     function admindata($row)
//     {
//         $ci = &get_instance();
//         $ci->load->model("auth/m_auth", "auth");
//         $email = $ci->session->email;
//         $user_data = $ci->auth->getWhereEmail($email)->row_array();

//         if (!empty($user_data)) {
//             return $user_data[$row];
//         } else {
//             return false;
//         }
//     }
// }

if (!function_exists('getNumber')) {
    function getNumber($string)
    {
        $result = preg_replace('/[^0-9]/', '', $string);
        return $result;
    }
}

if (!function_exists('formatNumber')) {
    function formatNumber($string, $prefix = null)
    {
        $result = number_format($string, 0, '.', '.');
        return $prefix . $result;
    }
}
