<style>
	.nice-select {
		opacity: 0 !important;
	}

	.gl-star-rating-text {
		opacity: 0 !important;
	}
</style>

<!-- Breadcrumbs -->
<div class="breadcrumbs pb-0">
	<div class="container">
		<div class="d-flex justify-content-center justify-content-md-start content-content-center content-content-md-start flex-column flex-md-row">
			<img src="<?= base_url('assets/pengguna/profil/' . userdata('gambar_pengguna')); ?>" alt="" class="produk-selector align-self-center align-self-md-start" style="height: 80px; width: 80px">
			<div class="ml-0 ml-md-3 mt-3 mt-md-0 align-self-center align-self-md-start text-center text-md-left">
				<h5><?= userdata('nama_pengguna'); ?></h5>
				<small>Bergabung sejak <?= date('Y', strtotime(userdata('created_at'))); ?></small>
			</div>
		</div>

		<div style="overflow: auto;">
			<div class="d-flex mt-3">
				<a href="<?= base_url('pengguna/profile'); ?>" class="card-link text-body py-2 d-flex align-items-center">
					<div class="col-auto text-center">Profile</div>
				</a>
				<a href="<?= base_url('pengguna/settings/biodata'); ?>" class="card-link text-body py-2 d-flex align-items-center">
					<div class="col-auto text-center">Settings</div>
				</a>
				<a href="<?= base_url('pengguna/myitems'); ?>" class="card-link text-success bg-white border border-bottom-0 py-2 d-flex align-items-center">
					<div class="col-auto text-center">Item saya</div>
				</a>
				<a href="<?= base_url('pengguna/toko'); ?>" class="card-link text-body py-2 d-flex align-items-center">
					<div class="col-auto text-center">Tokoku</div>
				</a>
			</div>
		</div>

	</div>
</div>
<!-- End Breadcrumbs -->

<!-- Product Style -->
<section class="product-area shop-sidebar shop section pt-4">
	<div class="container">

		<div class="container">

			<p class="mb-2"><?= !$this->input->get('q') ? '' : ($hasil_pencarian . ' Produk ditemukan hasil pencarian dari : ' . $this->input->get('q')); ?></p>

			<form action="" method="get">
				<div class="input-group mb-3">
					<input type="text" name="q" class="form-control px-3 border-right-0" placeholder="Cari nama produknya disini...">
					<div class="input-group-append">
						<button class="btn" type="button">Cari</button>
					</div>
				</div>
			</form>
		</div>

		<!-- Shopping Cart -->
		<div class="shopping-cart section py-0">
			<div class="container">
				<div class="row">
					<div class="col-12 bg-white">
						<div class="table-responsive">
							<!-- Shopping Summery -->
							<table class="table shopping-summery">
								<thead>
									<tr class="main-hading">
										<th class="text-center">PRODUK</th>
										<th width="5%" class="text-center">NAMA</th>
										<th width="5%" class="text-center">HARGA</th>
										<th width="5%" class="text-center">DOWNLOAD/BUKA TAUTAN</th>
										<th width="5%" class="text-center">METODE PEMBAYARAN</th>
										<th width="5%" class="text-center">TGL DITERIMA</th>
										<th width="50%" class="text-center">RATING</th>
										<th width="5%" class="text-center">Action</th>
									</tr>
								</thead>
								<tbody>
									<?php if ($koleksi_pembelian) : ?>
										<?php foreach ($koleksi_pembelian as $result) : ?>
											<tr>
												<td class="image" data-title="PRODUK" id="openModalProduk" targetid="<?= $result['id_produk_post']; ?>"><img style="object-fit: cover;" src="<?= base_url('assets/pengguna/toko/produk/' . $result['gambar_produk']); ?>" alt="#"></td>
												<td class="product-des" data-title="NAMA" id="openModalProduk" data-toggle="modal" targetid="<?= $result['id_produk_post']; ?>">
													<p class="product-name"><a href="#"><?= $result['nama_produk']; ?></a></p>
												</td>
												<td class="price" data-title="HARGA"><span><?= formatNumber($result['harga_produk'], "Rp"); ?></span></td>
												<td class="price" data-title="DOWNLOAD"><a class="text-primary" href="<?= $result['link_file']; ?>" target="_blank">Buka Tautan</a></td>
												<td class="price" data-title="METODE"><span><?= ucwords(str_replace('_', ' ', $result['metode_pembayaran'])); ?></span></td>
												<td class="total-amount" data-title="TGL DITERIMA"><span><?= date('d/m/Y H:i:s', strtotime($result['tgl_diterima'])); ?></span></td>
												<td>
													<select class="star-rating-4" data-options='{"initialText":"<?= $result['id_produk_post'] ?>"}'>
														<option <?= rating($result['id_produk_post']) == '5' ? 'selected' : ''; ?> value="5">5</option>
														<option <?= rating($result['id_produk_post']) == '4' ? 'selected' : ''; ?> value="4">4</option>
														<option <?= rating($result['id_produk_post']) == '3' ? 'selected' : ''; ?> value="3">3</option>
														<option <?= rating($result['id_produk_post']) == '2' ? 'selected' : ''; ?> value="2">2</option>
														<option <?= rating($result['id_produk_post']) == '1' ? 'selected' : ''; ?> value="1">1</option>
													</select>
												</td>
												<td width="5%">
													<?php $c = comp($result['id_pesanan']);
													if (!$c) { ?>
														<button class="btn-warning btn-sm com" data-pes="<?= $result['id_pesanan'] ?>">Complain</button>
													<?php } else { ?>
														<button class="btn-primary btn-sm text-capitalize"><?= $c[0]->status_laporan ?></button>
													<?php } ?>
												</td>
											</tr>
										<?php endforeach; ?>
									<?php else : ?>
										<tr>
											<td colspan="6" class="text-center">Item kosong.</td>
										</tr>
									<?php endif; ?>
								</tbody>
							</table>
						</div>
						<!--/ End Shopping Summery -->
					</div>
				</div>
			</div>
		</div>
		<!--/ End Shopping Cart -->


		<div class="modal fade" id="myModal">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">

					<!-- Modal body -->
					<div class="modal-body">
						<form method="post" action="<?= base_url('komplain') ?>" id="form">
							<div class="form-group">
								<select class="form-control d-block" name="cas" id="cas" required>
									<option value="">Pilih Kasus</option>
									<option value="Palsu">Palsu</option>
									<option value="Tidak Sesuai">Tidak Sesuai</option>
									<option value="Belum Diterima">Belum Diterima</option>
								</select>
							</div>
							<div class="form-group">
								<textarea class="form-control" name="al" id="al" placeholder="Alasan" required></textarea>
							</div>
							<input type="hidden" id="pes" name="pes">
							<button type="submit" class="btn btn-primary">Submit</button>
						</form>
					</div>

					<!-- Modal footer -->
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
					</div>

				</div>
			</div>
		</div>

		<div class="container">
			<?= $this->pagination->create_links(); ?>
		</div>

	</div>

</section>
<!--/ End Product Style 1  -->