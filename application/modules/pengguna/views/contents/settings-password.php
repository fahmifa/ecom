<!-- Breadcrumbs -->
<div class="breadcrumbs pb-0">
	<div class="container">
		<div class="d-flex justify-content-center justify-content-md-start content-content-center content-content-md-start flex-column flex-md-row">
			<img src="<?= base_url('assets/pengguna/profil/' . userdata('gambar_pengguna')); ?>" alt="" class="produk-selector align-self-center align-self-md-start" style="height: 80px; width: 80px">
			<div class="ml-0 ml-md-3 mt-3 mt-md-0 align-self-center align-self-md-start text-center text-md-left">
				<h5><?= userdata('nama_pengguna'); ?></h5>
				<small>Bergabung sejak <?= date('Y', strtotime(userdata('created_at'))); ?></small>
			</div>
		</div>

		<div style="overflow: auto;">
			<div class="d-flex mt-3">
				<a href="<?= base_url('pengguna/profile'); ?>" class="card-link text-body py-2 d-flex align-items-center">
					<div class="col-auto text-center">Profile</div>
				</a>
				<a href="<?= base_url('pengguna/settings/biodata'); ?>" class="card-link text-success bg-white border border-bottom-0 py-2 d-flex align-items-center">
					<div class="col-auto text-center">Settings</div>
				</a>
				<a href="<?= base_url('pengguna/myitems'); ?>" class="card-link text-body py-2 d-flex align-items-center">
					<div class="col-auto text-center">Item saya</div>
				</a>
				<a href="<?= base_url('pengguna/toko'); ?>" class="card-link text-body py-2 d-flex align-items-center">
					<div class="col-auto text-center">Tokoku</div>
				</a>
			</div>
		</div>

	</div>
</div>
<!-- End Breadcrumbs -->

<!-- Product Style -->
<section class="product-area shop-sidebar shop section pt-3">
	<div class="container">

		<div class="row">

			<div class="col-lg-3 col-md-4 col-12">
				<div class="shop-sidebar">
					<!-- Single Widget -->
					<div class="single-widget category">
						<h3 class="title">Informasi Detail</h3>
						<ul class="categor-list">
							<li><a class="w-100" href="<?= base_url('pengguna/settings/biodata'); ?>">Informasi Personal</a></li>
							<li><a class="w-100" href="<?= base_url('pengguna/settings/avatar'); ?>">Foto Profile</a></li>
							<li><a class="bg-success text-white px-2 w-100" href="<?= base_url('pengguna/settings/password'); ?>">Password</a></li>
						</ul>
					</div>
				</div>
			</div>

			<div class="col-lg-9 col-md-8 col-12 mt-3 mt-md-0">
				<div class="card">
					<div class="card-header">
						<h5>Informasi Personal</h5>
						<p>Kami tidak akan menjual/membagikan/menyebarluaskan Informasi Detail anda ke publik</p>
					</div>
					<div class="card-body">

						<?= $this->session->flashdata('msg'); ?>

						<form action="" method="POST">
							<div class="form-group">
								<label for="oldPassword">Password Lama</label>
								<input id="oldPassword" class="form-control px-2" type="password" name="oldPassword" required>
								<?= form_error('oldPassword'); ?>
							</div>

							<div class="form-group">
								<label for="newPassword">Password Baru</label>
								<input id="newPassword" class="form-control px-2" type="password" name="newPassword" required>
								<?= form_error('newPassword'); ?>
							</div>

							<div class="form-group">
								<label for="newPassword2">Ketika Ulang Password</label>
								<input id="newPassword2" class="form-control px-2" type="password" name="newPassword2" required>
								<?= form_error('newPassword2'); ?>
							</div>

							<div class="checkbox">
								<label class="checkbox-inline checked" for="eye"><input name="news" id="eye" type="checkbox"> Tampilkan Password</label>
							</div>

							<button type="submit" class="btn">Simpan</button>
						</form>
					</div>
				</div>
			</div>

		</div>

	</div>

</section>
<!--/ End Product Style 1  -->