<!-- Breadcrumbs -->
<div class="breadcrumbs pb-0">
	<div class="container">
		<div class="d-flex justify-content-center justify-content-md-start content-content-center content-content-md-start flex-column flex-md-row">
			<img src="<?= base_url('assets/pengguna/profil/' . userdata('gambar_pengguna')); ?>" alt="" class="produk-selector align-self-center align-self-md-start" style="height: 80px; width: 80px">
			<div class="ml-0 ml-md-3 mt-3 mt-md-0 align-self-center align-self-md-start text-center text-md-left">
				<h5><?= userdata('nama_pengguna'); ?></h5>
				<small>Bergabung sejak <?= date('Y', strtotime(userdata('created_at'))); ?></small>
			</div>
		</div>

		<div style="overflow: auto;">
			<div class="d-flex mt-3">
				<a href="<?= base_url('pengguna/profile'); ?>" class="card-link text-body py-2 d-flex align-items-center">
					<div class="col-auto text-center">Profile</div>
				</a>
				<a href="<?= base_url('pengguna/settings/biodata'); ?>" class="card-link text-body py-2 d-flex align-items-center">
					<div class="col-auto text-center">Settings</div>
				</a>
				<a href="<?= base_url('pengguna/myitems'); ?>" class="card-link text-body py-2 d-flex align-items-center">
					<div class="col-auto text-center">Item saya</div>
				</a>
				<a href="<?= base_url('pengguna/toko'); ?>" class="card-link text-success bg-white border border-bottom-0 py-2 d-flex align-items-center">
					<div class="col-auto text-center">Tokoku</div>
				</a>
			</div>
		</div>

	</div>
</div>
<!-- End Breadcrumbs -->

<!-- Product Style -->
<section class="product-area shop-sidebar shop section pt-3">
	<div class="container">

		<div class="row">

			<div class="col-12 col-md-4">
				<img class="img-fluid" src="<?= base_url('assets/laptop.svg'); ?>" alt="">
			</div>

			<div class="col-12 col-md-8 d-flex align-content-around flex-wrap">

				<?php if ($toko->num_rows() > 0) : ?>
					<div class="my-3 my-md-0">
						<h5>Hai, selamat datang kembali!</h5>
						<p>
							<strong><?= userdata('nama_pengguna'); ?></strong>,
							Toko Kamu <strong><?= $toko->row_array()['nama_toko']; ?></strong> saat ini memiliki saldo senilai Rp<?= formatNumber($toko->row_array()['saldo_toko']) ?: 0; ?>
						</p>
					</div>

					<div class="w-100">
						<a href="<?= base_url('tokoku'); ?>" type="button" class="btn text-white">Buka halaman toko</a>
					</div>
				<?php else : ?>
					<div class="my-3 my-md-0">
						<h5>Anda belum membuka toko!</h5>
						<p>Yuk gabung bersama ratusan pengguna lainnya untuk membuka toko dan berbisnis bersama kami!</p>
					</div>

					<div class="w-100">
						<a href="<?= base_url('bukatoko'); ?>" type="button" class="btn text-white">Ingin Gabung!</a>
					</div>
				<?php endif; ?>

			</div>

		</div>

	</div>

</section>
<!--/ End Product Style 1  -->