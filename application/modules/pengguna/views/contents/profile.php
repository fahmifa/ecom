<!-- Breadcrumbs -->
<div class="breadcrumbs pb-0">
	<div class="container">
		<div class="d-flex justify-content-center justify-content-md-start content-content-center content-content-md-start flex-column flex-md-row">
			<img src="<?= base_url('assets/pengguna/profil/' . userdata('gambar_pengguna')); ?>" alt="" class="produk-selector align-self-center align-self-md-start" style="height: 80px; width: 80px">
			<div class="ml-0 ml-md-3 mt-3 mt-md-0 align-self-center align-self-md-start text-center text-md-left">
				<h5><?= userdata('nama_pengguna'); ?></h5>
				<small>Bergabung sejak <?= date('Y', strtotime(userdata('created_at'))); ?></small>
			</div>
		</div>

		<div style="overflow: auto;">
			<div class="d-flex mt-3">
				<a href="<?= base_url('pengguna/profile'); ?>" class="card-link text-success bg-white border border-bottom-0 py-2 d-flex align-items-center">
					<div class="col-auto text-center">Profile</div>
				</a>
				<a href="<?= base_url('pengguna/settings/biodata'); ?>" class="card-link text-body py-2 d-flex align-items-center">
					<div class="col-auto text-center">Settings</div>
				</a>
				<a href="<?= base_url('pengguna/myitems'); ?>" class="card-link text-body py-2 d-flex align-items-center">
					<div class="col-auto text-center">Item saya</div>
				</a>
				<a href="<?= base_url('pengguna/toko'); ?>" class="card-link text-body py-2 d-flex align-items-center">
					<div class="col-auto text-center">Tokoku</div>
				</a>
			</div>
		</div>

	</div>
</div>
<!-- End Breadcrumbs -->

<!-- Product Style -->
<section class="product-area shop-sidebar shop section pt-3">
	<div class="container">

		<div class="row">

			<div class="col-12 col-md-6 mx-auto">
				<div class="form-group">
					<label>Nama Pengguna</label>
					<input class="form-control" type="text" disabled value="<?= userdata('nama_pengguna'); ?>">
				</div>

				<div class="form-group">
					<label>Nama Perusahaan</label>
					<input class="form-control" type="text" disabled value="<?= userdata('nama_perusahaan'); ?>">
				</div>

				<div class="form-group">
					<label>Alamat</label>
					<textarea class="form-control" rows="3" disabled><?= userdata('alamat'); ?></textarea>
				</div>

				<div class="form-group">
					<label>No Handphone</label>
					<input class="form-control" type="text" disabled value="<?= userdata('nomor_hp'); ?>">
				</div>

				<div class="form-group">
					<label>Alamat Email</label>
					<input class="form-control" type="text" disabled value="<?= userdata('email'); ?>">
				</div>

				<a class="btn w-100 text-center" href="<?= base_url('pengguna/settings'); ?>">Ubah Biodata</a>

			</div>
		</div>

	</div>

</section>
<!--/ End Product Style 1  -->