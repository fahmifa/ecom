<script>
    $(document).ready(function() {
        $('input#eye').click(function() {
            let elm = $(this).closest('form').find('input').not('[type="checkbox"]');

            if ($(this).is(':checked')) {
                elm.attr('type', 'text');
            } else {
                elm.attr('type', 'password');
            }
        });
    });
</script>