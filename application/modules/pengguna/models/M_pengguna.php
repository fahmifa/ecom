<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_pengguna extends CI_Model
{
    public function ubahPassword($newpassword, $id_pengguna)
    {
        $this->db->set('password', $newpassword);
        $this->db->where('id_pengguna', $id_pengguna);
        $this->db->update('tbl_pengguna');

        return $this->db->affected_rows();
    }

    public function updateAkun($id_pengguna, $data)
    {
        $this->db->set($data);
        $this->db->where('id_pengguna', $id_pengguna);
        $this->db->update('tbl_pengguna');
        return $this->db->affected_rows();
    }

    public function updateFotoProfil($gambar, $id_pengguna)
    {
        $this->db->set('gambar_pengguna', $gambar);
        $this->db->where('id_pengguna', $id_pengguna);
        $this->db->update('tbl_pengguna');
    }

    public function koleksiPembelian($id_pengguna, $limit, $start, $q)
    {
        $joinTblGambar = "(SELECT * FROM tbl_produk_gambar WHERE is_thumbnail = 1) tbl_produk_gambar";
        $this->db->select("tbl_pengguna_pesanan.*, tbl_produk_post.nama_produk, tbl_produk_post.link_file, tbl_produk_gambar.gambar_produk")
            ->from('tbl_pengguna_pesanan')
            ->join('tbl_produk_post', 'tbl_produk_post.id_produk_post = tbl_pengguna_pesanan.id_produk_post')
            ->join($joinTblGambar, 'tbl_produk_gambar.id_produk_post = tbl_produk_post.id_produk_post', "LEFT")
            ->where('tbl_pengguna_pesanan.id_pengguna', $id_pengguna)
            ->where('tbl_pengguna_pesanan.status_pesanan', 'pesanan_selesai')
            ->like('nama_produk', $q)
            ->order_by('tbl_pengguna_pesanan.tgl_diterima', 'ASC')
            ->limit($limit, $start);

        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function totalKoleksiPembelian($id_pengguna, $q)
    {

        $this->db->select("
                tbl_pengguna_pesanan.*,
                tbl_produk_post.nama_produk,
                tbl_produk_gambar.gambar_produk
            ")
            ->from('tbl_pengguna_pesanan')
            ->join('tbl_produk_post', 'tbl_produk_post.id_produk_post = tbl_pengguna_pesanan.id_produk_post')
            ->where('tbl_pengguna_pesanan.id_pengguna', $id_pengguna)
            ->where('tbl_pengguna_pesanan.status_pesanan', 'pesanan_selesai')
            ->like('tbl_produk_post.nama_produk', $q);

        return $this->db->count_all_results();
    }

    public function cekTokoUser($id_pengguna)
    {
        return $this->db->get_where('tbl_pengguna_toko', ['id_pengguna' => $id_pengguna]);
    }
}
