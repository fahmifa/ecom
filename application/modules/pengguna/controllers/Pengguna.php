<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pengguna extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_pengguna', 'pengguna');
		if (!userdata('id_pengguna')) {
			redirect(base_url('auth/login'));
		}

		// CI Paging
		$this->full_tag_open	= '<nav class="my-3"><ul class="d-flex pagination-sm">';
		$this->full_tag_close	= '</ul></nav>';
		$this->first_link		= 'First';
		$this->first_tag_open	= '<li class="page-item">';
		$this->first_tag_close	= '</li>';
		$this->last_link		= 'Last';
		$this->last_tag_open	= '<li class="page-item">';
		$this->last_tag_close	= '</li>';
		$this->next_link		= '&gt;';
		$this->next_tag_open	= '<li class="page-item">';
		$this->next_tag_close	= '</li>';
		$this->prev_link		= '&lt;';
		$this->prev_tag_open	= '<li class="page-item">';
		$this->prev_tag_close	= '</li>';
		$this->cur_tag_open		= '<li class="page-item active"><a class="page-link" href="#">';
		$this->cur_tag_close	= '</a></li>';
		$this->num_tag_open		= '<li class="page-item">';
		$this->num_tag_close	= '</li>';
		$this->attributes		= ['class' => 'page-link text-success'];
	}

	// Page
	public function profile()
	{
		$data['title'] = 'Profile';
		$this->load->view('home/template/header', $data);
		$this->load->view('home/template/topbar');
		$this->load->view('home/css/global');
		$this->load->view('contents/profile');
		$this->load->view('home/javascript/global');
		$this->load->view('home/template/footer');
	}

	public function settings($section)
	{
		if ($section === 'biodata') {
			$this->_settings_biodata();
		} elseif ($section === 'avatar') {
			$this->_settings_avatar();
		} elseif ($section === 'password') {
			$this->_settings_password();
		} else {
			redirect(base_url());
		}
	}

	public function myitems()
	{
		$this->load->library('pagination');

		$start = $this->input->get('p');
		$q = $this->input->get('q');
		$jumlahKoleksiPembelian = $this->pengguna->totalKoleksiPembelian(userdata('id_pengguna'), $q);
		$data['title'] = 'My Items';
		$data['koleksi_pembelian'] = $this->pengguna->koleksiPembelian(userdata('id_pengguna'), 10, $start, $q);
		$data['hasil_pencarian'] = $jumlahKoleksiPembelian;

		// print_r($data['koleksi_pembelian']);
		// die();

		$config['base_url'] = base_url('pengguna/myitems');
		$config['total_rows'] = $jumlahKoleksiPembelian;
		$config['per_page'] = 10;
		$config['num_links'] = 2;
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'p';

		$config['full_tag_open']	= $this->full_tag_open;
		$config['full_tag_close']	= $this->full_tag_close;
		$config['first_link']		= $this->first_link;
		$config['first_tag_open']	= $this->first_tag_open;
		$config['first_tag_close']	= $this->first_tag_close;
		$config['last_link']		= $this->last_link;
		$config['last_tag_open']	= $this->last_tag_open;
		$config['last_tag_close']	= $this->last_tag_close;
		$config['next_link']		= $this->next_link;
		$config['next_tag_open']	= $this->next_tag_open;
		$config['next_tag_close']	= $this->next_tag_close;
		$config['prev_link']		= $this->prev_link;
		$config['prev_tag_open']	= $this->prev_tag_open;
		$config['prev_tag_close']	= $this->prev_tag_close;
		$config['cur_tag_open']		= $this->cur_tag_open;
		$config['cur_tag_close']	= $this->cur_tag_close;
		$config['num_tag_open']		= $this->num_tag_open;
		$config['num_tag_close']	= $this->num_tag_close;
		$config['attributes']		= $this->attributes;

		$this->pagination->initialize($config);

		$this->load->view('home/template/header', $data);
		$this->load->view('home/template/topbar');
		$this->load->view('home/css/global');
		$this->load->view('contents/my-items');
		$this->load->view('home/javascript/global');
		$this->load->view('home/template/footer');
	}

	public function toko()
	{
		$data['title'] = 'Toko';
		$data['toko'] = $this->pengguna->cekTokoUser(userdata('id_pengguna'));
		$this->load->view('home/template/header', $data);
		$this->load->view('home/template/topbar');
		$this->load->view('home/css/global');
		$this->load->view('contents/toko');
		$this->load->view('home/javascript/global');
		$this->load->view('home/template/footer');
	}

	public function _settings_biodata()
	{
		$this->form_validation->set_rules('nama', 'Xama', 'trim|required', ['required' => 'Mohon isi kolom ini terlebih dahulu!']);
		$this->form_validation->set_rules('nama_perusahaan', 'Xama_perusahaan', 'trim');
		$this->form_validation->set_rules('alamat', 'Xlamat', 'trim');
		$this->form_validation->set_rules('nohp', 'Xohp', 'trim|is_unique[tbl_pengguna.nomor_hp]', ['is_unique' => 'Nomor Handphone/Whatsapp sudah digunakan oleh akun lain.']);
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');

		if ($this->form_validation->run() === FALSE) {
			$data['title'] = 'Informasi Personal';
			$this->load->view('home/template/header', $data);
			$this->load->view('home/template/topbar');
			$this->load->view('home/css/global');
			$this->load->view('contents/settings-biodata');
			$this->load->view('home/javascript/global');
			$this->load->view('home/template/footer');
		} else {
			$data['nama_pengguna']		= $this->input->post('nama', true);
			$data['nomor_hp']			= getNumber($this->input->post('nohp')) ?: null;
			$data['alamat']				= $this->input->post('alamat', true);
			$data['nama_perusahaan']	= $this->input->post('nama_perusahaan', true) ?: null;
			$return = $this->pengguna->updateAkun(userdata('id_pengguna'), $data);

			if ($return) {
				$this->session->set_flashdata('msg', $this->bootstrapalert('Informasi Personal berhasil diperbarui.', 'alert-success'));
			}
			redirect(base_url('pengguna/settings/biodata'));
		}
	}

	public function _settings_avatar()
	{
		$data['title'] = 'Foto Profile';
		$this->load->view('home/template/header', $data);
		$this->load->view('home/template/topbar');
		$this->load->view('home/css/global');
		$this->load->view('contents/settings-avatar');
		$this->load->view('home/javascript/global');
		$this->load->view('home/template/footer');
	}

	public function _settings_password()
	{
		$this->form_validation->set_rules('oldPassword', 'Password lama', 'trim|required', ['required' => 'Mohon isi Password Lama terlebih dahulu']);
		$this->form_validation->set_rules('newPassword', 'Password baru', 'trim|required|min_length[6]', ['required' => 'Mohon isi Password Baru terlebih dahulu', 'min_length' => 'Mohon masukkan password setidaknya 6 karakter!']);
		$this->form_validation->set_rules('newPassword2', 'Password baru 2', 'trim|required|matches[newPassword]', ['required' => 'Mohon ketikan ulang Password Baru terlebih dahulu', 'matches' => 'Password Baru yang anda masukkan tidak sama!']);
		$this->form_validation->set_error_delimiters('<small class="text-danger">', '</small>');

		if ($this->form_validation->run() === FALSE) {
			$data['title'] = 'Password';
			$this->load->view('home/template/header', $data);
			$this->load->view('home/template/topbar');
			$this->load->view('home/css/global');
			$this->load->view('contents/settings-password');
			$this->load->view('home/javascript/global');
			$this->load->view('javascript/settings-password');
			$this->load->view('home/template/footer');
		} else {
			$oldpassword = $this->input->post('oldPassword');
			$newpassword = $this->input->post('newPassword');
			$this->_ubahPassword($oldpassword, $newpassword);
		}
	}
	// Page

	// Function
	public function updateFotoProfile()
	{
		if (!$_FILES) {
			redirect(base_url());
		} else {
			$config['upload_path']		= './assets/pengguna/profil/';
			$config['allowed_types']	= 'jpg|jpeg|png';
			$config['file_name']		= "profil_" . userdata('id_pengguna');
			$config['max_height']		= '100';
			$config['max_width']		= '100';

			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			if ($this->upload->do_upload('foto')) {

				if (userdata('gambar_pengguna') !== 'user_default.svg') {
					unlink(FCPATH . '/assets/pengguna/profil/' . userdata('gambar_pengguna'));
				}

				$namagambar = $this->upload->data("file_name");
				$this->pengguna->updateFotoProfil($namagambar, userdata('id_pengguna'));
			} else {
				$this->session->set_flashdata('msg', $this->bootstrapalert($this->upload->display_errors('', ''), 'alert-danger'));
			}

			redirect(base_url('pengguna/settings/avatar'));
		}
	}

	private function _ubahpassword($oldpassword, $newpassword)
	{
		if (password_verify($oldpassword, userdata('password'))) {
			if ($oldpassword === $newpassword) {
				$this->session->set_flashdata('msg', $this->bootstrapalert('Password baru tidak boleh sama dengan passwrod lama!', 'alert-danger'));
				redirect(base_url('pengguna/settings/password'));
			} else {
				$newpassword = password_hash($newpassword, PASSWORD_DEFAULT);
				$return = $this->pengguna->ubahPassword($newpassword, userdata('id_pengguna'));

				if ($return) {
					$this->session->set_flashdata('msg', $this->bootstrapalert('Password berhasil diperbarui.', 'alert-success'));
					redirect(base_url('pengguna/settings/password'));
				} else {
					$this->session->set_flashdata('msg', $this->bootstrapalert('Oops! Sepertinya ada kesalahan server.', 'alert-danger'));
					redirect(base_url('pengguna/settings/password'));
				}
			}
		} else {
			$this->session->set_flashdata('msg', $this->bootstrapalert('Password lama yang anda masukkan salah!', 'alert-danger'));
			redirect(base_url('pengguna/settings/password'));
		}
	}
	// Function

	// api
	public function apiubahpassword()
	{
		$this->form_validation->set_rules('oldpassword', 'oldPassword', 'trim|required', ['required' => 'Mohon isi Password Lama terlebih dahulu']);
		$this->form_validation->set_rules('newpassword', 'newPassword', 'trim|required|min_length[6]|matches[newpassword2]', ['required' => 'Mohon isi Password Baru terlebih dahulu', 'matches' => 'Password Baru yang anda masukkan tidak sama!', 'min_length' => 'Mohon masukkan password setidaknya 6 karakter!']);
		$this->form_validation->set_rules('newpassword2', 'newPassword2', 'trim|required', ['required' => 'Mohon ketikan ulang Password Baru terlebih dahulu']);
		$this->form_validation->set_error_delimiters('', '');

		$oldpassword = $this->input->post('oldpassword');
		$newpassword = $this->input->post('newpassword');
		$newpassword2 = $this->input->post('newpassword2');

		if ($this->form_validation->run() === FALSE) {
			$output = [
				'kode'          => '0',
				'oldpassworderror'   => form_error('oldpassword'),
				'newpassworderror'   => form_error('newpassword'),
				'newpassword2error'  => form_error('newpassword2')
			];

			$this->output($output);
		} else {
			$this->_apiUbahpassword($oldpassword, $newpassword, $newpassword2);
		}
	}

	private function _apiUbahpassword($oldpassword, $newpassword, $newpassword2)
	{
		$output = [];

		if (!password_verify($oldpassword, userdata('password'))) {
			$output = [
				'kode'          => '00',
				'oldpassworderror'   => 'Password lama yang anda masukkan salah!',
			];
		} elseif (password_verify($newpassword, userdata('password'))) {
			$output = [
				'kode'          => '000',
				'newpassworderror'   => 'Password Baru tidak boleh sama dengan Password Lama!',
			];
		} else {
			$newpassword = password_hash($newpassword, PASSWORD_DEFAULT);

			$ubahPasswordWhere = $this->pengguna->ubahPassword($newpassword, userdata('id_pengguna'));

			if (!$ubahPasswordWhere) {
				$output = [
					'kode' => '0000',
				];
			} else {
				$output = [
					'kode' => '1'
				];
			}
		}

		$this->output($output);
	}
	// api

	// helper
	private function output($data)
	{
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($data));
	}

	private function bootstrapalert($msg, $class)
	{
		$template = "<div class='alert $class' role='alert'>$msg</div>";

		return $template;
	}
	// helper

	// logout
	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url());
	}
	// logout

}
