<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Toko extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_toko', 'toko');

		// CI Paging
		$this->full_tag_open	=  '<nav class="my-3 container mx-sm-3"><ul class="d-flex pagination-sm">';
		$this->full_tag_close	=  '</ul></nav>';
		$this->first_link		= 'First';
		$this->first_tag_open	= '<li class="page-item">';
		$this->first_tag_close	= '</li>';
		$this->last_link		= 'Last';
		$this->last_tag_open	= '<li class="page-item">';
		$this->last_tag_close	= '</li>';
		$this->next_link		= '&gt;';
		$this->next_tag_open	= '<li class="page-item">';
		$this->next_tag_close	= '</li>';
		$this->prev_link		= '&lt;';
		$this->prev_tag_open	= '<li class="page-item">';
		$this->prev_tag_close	= '</li>';
		$this->cur_tag_open		= '<li class="page-item active"><a class="page-link" href="#">';
		$this->cur_tag_close	= '</a></li>';
		$this->num_tag_open		= '<li class="page-item">';
		$this->num_tag_close	= '</li>';
		$this->attributes		= ['class' => 'page-link text-success'];
	}

	public function index()
	{
		$id = $this->session->userdata('id');
		$fa = $this->uri->segment(2);
		$tab = array('tbl_pengguna_toko', 'tbl_produk_post', 'tbl_produk_kategori', 'tbl_pengguna');
		$where = ['tbl_pengguna_toko.id_toko' => $fa];
		$sama = array(
			null,
			'tbl_pengguna_toko.id_toko=tbl_produk_post.id_toko',
			'tbl_produk_post.id_kategori=tbl_produk_kategori.id_kategori',
			'tbl_pengguna_toko.id_pengguna=tbl_pengguna.id_pengguna'
		);
		$data['toko'] = $this->toko->join($tab, $sama, $where)->result();

		$data['produk_favorit'] = !userdata('id_pengguna') ? [] : explode(',', $this->toko->selectFavoriteWhere(userdata('id_pengguna'))[0]['result']);

		$data['num'] = $this->toko->numdata('tbl_favorite_toko', ['id_toko' => $fa]); // jumlah follow toko


		$data['title'] = $data['toko'][0]->nama_toko;
		$this->load->view('template/header', $data);
		$this->load->view('template/topbar');
		$this->load->view('css/global');
		$this->load->view('contents/toko', $data);
		$this->load->view('javascript/global');
		$this->load->view('template/footer');
	}

	public function follow()
	{
		$id = $this->session->userdata('id');
		$val = $this->input->post('to');
		if ($val) {

			$wh = [
				'id_toko' => $val,
				'id_pengguna' => $this->session->userdata('id'),
			];
			$cc = $this->toko->check('tbl_favorite_toko', $wh);
			if ($cc) {
				$da = [
					'id_toko' => $val,
					'id_pengguna' => $this->session->userdata('id'),
					'created_at' => date("Y-m-d H:i:s")
				];
				$this->toko->update_all($wh, $da, 'tbl_favorite_toko');
				echo 'false';
			} else {

				$da = [
					'id_toko' => $val,
					'id_pengguna' => $this->session->userdata('id'),
					'created_at' => date("Y-m-d H:i:s")
				];
				$this->toko->insert_all('tbl_favorite_toko', $da);
				echo 1;
			}
		}
	}

	public function search()
	{
		$this->load->library('pagination');

		$kolomTblList = ['nama_produk', 'harga_produk', 'created_at'];
		$listKategori = explode(',', $this->home->concatIdKategori()['id_kategori']); // get list id kategori 
		$q = $this->input->get('q'); // nama produk
		$f = $this->input->get('f') ?: 'created_at'; // pick kolom table 
		$f = in_array($f, $kolomTblList) ? $f : 'created_at'; // cek apakah kolom table pada database valid ? (untuk menghindari log eror)
		$s = $this->input->get('s') ?: 'DESC'; // order by
		$k = $this->input->get('k'); // kategori
		$k = in_array($k, $listKategori) ? $f : ''; // cek apakah id terdaftar pada list kategori pada database
		$start = $this->input->get('p'); // page ke berapa. output = /?p=12 (12 = start get data dari database)

		if ($q) {
			$totalProduk = $this->home->totalProdukWhere($q, $k);
			$data['title'] = 'Pencarian';
			$data['produk'] = $this->home->selectProdukWhere($q, $f, $s, $k, 12, $start);
			$data['kategori'] = $this->home->getKategori();
			$data['pencarian'] = $q;
			$data['total'] = $totalProduk;
			$data['produk_favorit'] = !userdata('id_pengguna') ? [] : explode(',', $this->home->selectFavoriteWhere(userdata('id_pengguna'))[0]['result']);
			$data['full_url'] = base_url("home/search?q=$q&f=$f&s=$s");
			$data['id_kategori_lainnya'] = $this->home->getIdKategoriLainnya()['id_kategori'];

			$config['base_url'] = base_url("home/search?q=$q");
			$config['total_rows'] = $totalProduk;
			$config['per_page'] = 12;
			$config['uri_segment'] = 3;
			$config['num_links'] = 2;
			$config['page_query_string'] = TRUE;
			$config['query_string_segment'] = 'p';

			$config['full_tag_open']	= '<nav class="my-3"><ul class="d-flex pagination-sm">';
			$config['full_tag_close']	= $this->full_tag_close;
			$config['first_link']		= $this->first_link;
			$config['first_tag_open']	= $this->first_tag_open;
			$config['first_tag_close']	= $this->first_tag_close;
			$config['last_link']		= $this->last_link;
			$config['last_tag_open']	= $this->last_tag_open;
			$config['last_tag_close']	= $this->last_tag_close;
			$config['next_link']		= $this->next_link;
			$config['next_tag_open']	= $this->next_tag_open;
			$config['next_tag_close']	= $this->next_tag_close;
			$config['prev_link']		= $this->prev_link;
			$config['prev_tag_open']	= $this->prev_tag_open;
			$config['prev_tag_close']	= $this->prev_tag_close;
			$config['cur_tag_open']		= $this->cur_tag_open;
			$config['cur_tag_close']	= $this->cur_tag_close;
			$config['num_tag_open']		= $this->num_tag_open;
			$config['num_tag_close']	= $this->num_tag_close;
			$config['attributes']		= $this->attributes;

			$this->pagination->initialize($config);

			$this->load->view('template/header', $data);
			$this->load->view('template/topbar');
			$this->load->view('css/global');
			$this->load->view('contents/search');
			$this->load->view('javascript/global');
			$this->load->view('template/footer');
		} else {
			redirect(base_url());
		}
	}

	public function checkout()
	{
		$id_produk	= $this->input->post('id_produk');
		$dataProduk	= $this->home->getProdukWhere($id_produk);
		if ($dataProduk && userdata('id_pengguna')) {
			$data['title'] = 'Proses Pembelian';
			$data['data_produk'] = $dataProduk;
			$this->session->set_userdata('id_produk', $id_produk);
			$this->session->set_userdata('total_harga', $dataProduk['harga_produk']);
			$this->load->view('template/header', $data);
			$this->load->view('template/topbar');
			$this->load->view('contents/checkout');
			$this->load->view('javascript/global');
			$this->load->view('template/footer');
		} else {
			redirect(base_url());
		}
	}

	public function processorder()
	{
		$this->form_validation->set_rules('nama_perusahaan', 'Nama_perusahaan', 'trim');

		if ($this->form_validation->run() === FALSE) {
			redirect(base_url());
		} else {
			$id_pesanan			= rand(10000, 99999) . rand(10000, 99999);
			$id_produk 			= $this->session->id_produk;
			$harga_produk		= (int)$this->session->total_harga;
			$total_harga 		= $harga_produk + (int)substr($id_pesanan, -3, 3);
			$nama_perusahaan	= $this->input->post('nama_perusahaan');
			$data = [
				'id_pesanan'		=> $id_pesanan,
				'id_pengguna'		=> userdata('id_pengguna'),
				'id_produk_post'	=> $id_produk,
				'harga_produk'		=> $harga_produk,
				'total_harga'		=> $total_harga,
				'nama_perusahaan'	=> $nama_perusahaan
			];
			$processorder = $this->home->processorder($data);
			if ($processorder) {
				$data = [
					'id_history'	=> rand(10000, 99999) . rand(10000, 99999),
					'id_pesanan'	=> $id_pesanan,
				];
				$this->home->insertHistoryTransaksi($data);
				redirect(base_url("home/orderpayment/$id_pesanan"));
			} else {
				exit("Oops, sepertinya ada kesalahan server.");
			}
		}
	}

	public function orderpayment($id_pesanan = false)
	{
		$getPesananWhere = $this->home->getPesananWhere($id_pesanan, userdata('id_pengguna'));
		if ($getPesananWhere->num_rows() > 0) {
			$nominal_pembayaran = $getPesananWhere->row_array()['total_harga'];
			$data['nominal_pembayaran'] = formatNumber(floatval(substr($nominal_pembayaran, 0, (strlen($nominal_pembayaran) - 3))), "Rp") . ".";
			$data['kode_unik'] = substr($nominal_pembayaran, -3, 3);
			$this->load->view('contents/order-payment', $data);
		} else {
			redirect(base_url());
		}
	}

	public function formbuktipembayaran($id_pesanan = false)
	{
		$getPesananWhere = $this->home->getPesananWhere($id_pesanan, userdata('id_pengguna'));
		if ($getPesananWhere->num_rows() > 0) {
			$this->load->view('contents/form-bukti-pembayaran');
		} else {
			redirect(base_url());
		}
	}

	public function uploadbuktipembayaran()
	{

		$id_pesanan					= $this->input->post('id_pesanan');
		$gambar						= $_FILES['bukti_bayar'];
		$config['upload_path']		= './assets/pengguna/bukti_pembayaran/';
		$config['allowed_types']	= 'jpg|jpeg|png';
		$config['file_name']		= "bukti_pembayaran_" . rand();

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if (!$this->upload->do_upload('bukti_bayar')) {
			redirect(base_url('home/formbuktipembayaran/'));
		} else {
			$data = [
				'id_pesanan'	=> $id_pesanan,
				'id_pengguna'	=> userdata('id_pengguna'),
				'bukti_gambar'	=> $this->upload->data("file_name")
			];

			$getBuktiPembayaranWhere = $this->home->getBuktiPembayaranWhere($id_pesanan, userdata('id_pengguna'));
			if ($getBuktiPembayaranWhere->num_rows() > 0) {
				$old_file = $getBuktiPembayaranWhere->row_array()['bukti_gambar'];
				$this->home->deleteBuktiPembayaranWhere($id_pesanan, userdata('id_pengguna'));
				unlink(FCPATH . '/assets/pengguna/bukti_pembayaran/' . $old_file);
			}

			$return = $this->home->uploadBuktiPembayaran($data);

			if (!$return) {
				exit("Oops, sepertinya ada kesalahan server.");
			} else {
				$this->session->set_flashdata('msg', $this->bootstrapalert('Bukti telah dikirim, dan pembayaran akan kami verifikasi lebih lanjut.<br> Produk akan langsung kami kirim ke alamat email kamu setelah pembayaran berhasil terverifikasi.', 'alert-success'));
				redirect(base_url("home/formbuktipembayaran/$id_pesanan"));
			}
		}
	}

	public function historytransaksi($section = false)
	{
		$this->load->library('pagination');
		if ($section) {
			$q = $this->input->get('q');
			$totalHistory = $this->home->totalHistoryPesanan(userdata('id_pengguna'), $section, $q);
			$start = $this->uri->segment(4);
			$data['title'] = 'History Transaksi';
			$data['history'] = $this->home->selectPesananWhere(userdata('id_pengguna'), $section, 10, $start, $q);
			$config['base_url'] = base_url("home/historytransaksi/$section");
			$config['total_rows'] = $totalHistory;
			$config['per_page'] = 10;
			$config['uri_segment'] = 4;
			$config['num_links'] = 2;

			$config['full_tag_open']	= $this->full_tag_open;
			$config['full_tag_close']	= $this->full_tag_close;
			$config['first_link']		= $this->first_link;
			$config['first_tag_open']	= $this->first_tag_open;
			$config['first_tag_close']	= $this->first_tag_close;
			$config['last_link']		= $this->last_link;
			$config['last_tag_open']	= $this->last_tag_open;
			$config['last_tag_close']	= $this->last_tag_close;
			$config['next_link']		= $this->next_link;
			$config['next_tag_open']	= $this->next_tag_open;
			$config['next_tag_close']	= $this->next_tag_close;
			$config['prev_link']		= $this->prev_link;
			$config['prev_tag_open']	= $this->prev_tag_open;
			$config['prev_tag_close']	= $this->prev_tag_close;
			$config['cur_tag_open']		= $this->cur_tag_open;
			$config['cur_tag_close']	= $this->cur_tag_close;
			$config['num_tag_open']		= $this->num_tag_open;
			$config['num_tag_close']	= $this->num_tag_close;
			$config['attributes']		= $this->attributes;

			$this->pagination->initialize($config);

			$this->load->view('template/header', $data);
			$this->load->view('template/topbar');
			$this->load->view('css/global');
			$this->load->view('contents/history-transaksi');
			$this->load->view('javascript/global');
			$this->load->view('javascript/history-transaksi');
			$this->load->view('template/footer');
		}
	}

	public function myfavorite()
	{
		$this->load->library('pagination');

		$q = $this->input->get('q');
		$start = $this->input->get('p');
		if (userdata('id_pengguna')) {
			$data['total'] = $this->home->totalFavoriteProduk(userdata('id_pengguna'), $q);
			$data['favorite_produk'] = $this->home->selectFavoriteProduk(userdata('id_pengguna'), $q, 12, $start);
			$data['title'] = 'Pencarian';

			$config['base_url'] = base_url("home/myfavorite?q=$q");
			$config['total_rows'] = $data['total'];
			$config['per_page'] = 12;
			$config['uri_segment'] = 3;
			$config['num_links'] = 2;
			$config['page_query_string'] = TRUE;
			$config['query_string_segment'] = 'p';

			$config['full_tag_open']	= '<nav class="my-3"><ul class="d-flex pagination-sm">';
			$config['full_tag_close']	= $this->full_tag_close;
			$config['first_link']		= $this->first_link;
			$config['first_tag_open']	= $this->first_tag_open;
			$config['first_tag_close']	= $this->first_tag_close;
			$config['last_link']		= $this->last_link;
			$config['last_tag_open']	= $this->last_tag_open;
			$config['last_tag_close']	= $this->last_tag_close;
			$config['next_link']		= $this->next_link;
			$config['next_tag_open']	= $this->next_tag_open;
			$config['next_tag_close']	= $this->next_tag_close;
			$config['prev_link']		= $this->prev_link;
			$config['prev_tag_open']	= $this->prev_tag_open;
			$config['prev_tag_close']	= $this->prev_tag_close;
			$config['cur_tag_open']		= $this->cur_tag_open;
			$config['cur_tag_close']	= $this->cur_tag_close;
			$config['num_tag_open']		= $this->num_tag_open;
			$config['num_tag_close']	= $this->num_tag_close;
			$config['attributes']		= $this->attributes;

			$this->pagination->initialize($config);

			$this->load->view('template/header', $data);
			$this->load->view('template/topbar');
			$this->load->view('css/global');
			$this->load->view('contents/favorite-produk');
			$this->load->view('javascript/global');
			$this->load->view('javascript/favorite-produk');
			$this->load->view('template/footer');
		} else {
			$this->session->set_flashdata('msg', $this->bootstrapalert('Silahkan login terlebih dahulu!', 'alert-danger'));
			redirect(base_url('auth/login'));
		}
	}

	public function apimodalproduk()
	{
		$id_produk = $this->input->post('id_produk');
		$data_produk = $this->home->getProdukWhere($id_produk);
		$gambar_produk = $this->home->getGambarProdukWhere($id_produk);
		$output = [
			'data_produk'	=> $data_produk,
			'gambar_produk'		=> $gambar_produk
		];

		return $this->output($output);
	}

	private function output($output)
	{
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($output));
	}

	private function bootstrapalert($msg, $class)
	{
		$template = "<div class='alert $class' role='alert'>$msg</div>";
		return $template;
	}
}
