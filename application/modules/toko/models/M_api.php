<?php

defined('BASEPATH') or exit('No direct script access allowed');

class M_Api extends CI_Model
{
    public function batalkanPesanan($id_pengguna, $id_pesanan)
    {
        $set = [
            'status_pesanan'    => 'dibatalkan',
            'dibatalkan_oleh'   => 'pemesan'
        ];
        $where = [
            'id_pengguna'   => $id_pengguna,
            'id_pesanan'    => $id_pesanan
        ];
        $this->db->set($set);
        $this->db->where($where);
        $this->db->update('tbl_pengguna_pesanan');
        return $this->db->affected_rows();
    }

    public function tambahFavorit($id_pengguna, $id_produk)
    {
        $data = [
            'id_pengguna'   => $id_pengguna,
            'id_produk'     => $id_produk
        ];
        return $this->db->insert('tbl_favorite_produk', $data);
    }

    public function hapusFavorit($id_pengguna, $id_produk)
    {
        $data = [
            'id_pengguna'   => $id_pengguna,
            'id_produk'     => $id_produk
        ];
        return $this->db->delete('tbl_favorite_produk', $data);
    }
}
