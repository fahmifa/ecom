<script>
    $(document).ready(function() {
        let baseUrl = `<?= base_url('home'); ?>`;

        $(document).on('click', 'a#hapusDaftarFavorit', function() {
            let id_produk = $(this).attr('targetid');
            Swal.fire({
                text: 'Hapus dari daftar favorit?',
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonText: 'Tutup',
                confirmButtonText: 'Hapus!',
                reverseButtons: true,
            }).then((result) => {
                if (result.isConfirmed) {
                    hapusDaftarFavorit(id_produk);
                }
            });
        });

        function hapusDaftarFavorit(id_produk) {
            $.ajax({
                url: `${ baseUrl }/api/hapusfavorit`,
                method: "POST",
                data: {
                    id_produk
                }
            }).done((res) => {
                if (res.kode) {
                    location.reload();
                }
            });
        }
    });
</script>