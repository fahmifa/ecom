<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" />
    <title>Proses Pembayaran</title>
</head>

<body>

    <div class="container d-flex justify-content-center align-items-center" style="min-height: 100vh;">
        <div class="col-12 col-md-9">
            <a href="<?= base_url('home/historytransaksi/pending'); ?>"><i class="fas fa-arrow-left"></i> Riwayat transaksi </a>

            <div class="card my-2">
                <div class="card-body px-5">
                    <h5 class="text-center pt-3">Silahkan transfer ke salah satu Rekening yang tertera di bawah.</h5>

                    <div class="row my-5">
                        <div class="col-12 col-md-3 text-center">
                            <img class="img-fluid" style="height: 50px;" src="<?= base_url('assets/assets-home/images/bni.svg'); ?>" alt="bni">
                            <h5 class="align-self-end">xxx xxx xxx xxx</h5>
                            <p>A/n Admin</p>
                        </div>
                        <div class="col-12 col-md-3 text-center">
                            <img class="img-fluid" style="height: 50px;" src="<?= base_url('assets/assets-home/images/mandiri.svg'); ?>" alt="mandiri">
                            <h5 class="align-self-end">xxx xxx xxx xxx</h5>
                            <p>A/n Admin</p>
                        </div>
                        <div class="col-12 col-md-3 text-center">
                            <img class="img-fluid" style="height: 50px;" src="<?= base_url('assets/assets-home/images/bri.svg'); ?>" alt="bri">
                            <h5 class="align-self-end">xxx xxx xxx xxx</h5>
                            <p>A/n Admin</p>
                        </div>
                        <div class="col-12 col-md-3 text-center">
                            <img class="img-fluid" style="height: 50px;" src="<?= base_url('assets/assets-home/images/bca.svg'); ?>" alt="bca">
                            <h5 class="align-self-end">xxx xxx xxx xxx</h5>
                            <p>A/n Admin</p>
                        </div>
                    </div>

                    <div class="text-center">
                        Silahkan transfer dengan nominal yang sudah ditentukan tanpa ada pembulatan nilai untuk memudahkan verifikasi pembayaran.<br>
                        <h5><?= $nominal_pembayaran; ?><strong class="text-success"><?= $kode_unik; ?></strong></h5>

                        <a href="<?= base_url('home/formbuktipembayaran/' . $this->uri->segment(3)); ?>" class="btn btn-success my-3">Upload Bukti Pembayaran</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
</body>

</html>