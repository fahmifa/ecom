<!-- Breadcrumbs -->
<div class="breadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="bread-inner">
					<ul class="bread-list">
						<li><a href="<?= base_url(); ?>">Home<i class="ti-arrow-right"></i></a></li>
						<li class="active"><a href="#">Produk Favoritku</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- End Breadcrumbs -->

<!-- Product Style -->
<section class="product-area shop-sidebar shop section pt-3">

	<div class="container">

		<form action="" method="get">
			<div class="input-group mb-3">
				<input type="text" name="q" class="form-control px-3 border-right-0" placeholder="Cari nama produknya disini..." />
				<div class="input-group-append">
					<button class="btn" type="button">Cari</button>
				</div>
			</div>
		</form>

		<div class="row">

			<?php if ($favorite_produk) : ?>
				<?php foreach ($favorite_produk as $result) : ?>

					<div class="col-md-4 col-sm-6 col-12">
						<div class="single-product">
							<div class="product-img">
								<img class="default-img produk-selector" id="openModalProduk" style="height: 200px; max-height: 200px;" targetid="<?= $result['id_produk_post']; ?>" src="<?= base_url('assets/pengguna/toko/produk/') . $result['gambar_produk']; ?>" alt="Gambar Produk">
								<div class="button-head text-center">
									<div class="product-action mr-2">
										<a title="Lihat Produk" href="#" id="openModalProduk" data-toggle="modal" targetid="<?= $result['id_produk_post']; ?>">
											<i class="ti-eye"></i>
										</a>
									</div>
									<div class="product-action-2">
										<a class="text-danger" title="Hapus dari daftar" href="#" data-toggle="modal" id="hapusDaftarFavorit" targetid="<?= $result['id_produk_post']; ?>">Hapus dari daftar</a>
									</div>
								</div>
							</div>
							<div class="product-content">
								<p><?= $result['nama_produk']; ?></p>
								<div class="product-price">
									<span><?= formatNumber($result['harga_produk'], "Rp"); ?></span>
								</div>
							</div>
						</div>
					</div>

				<?php endforeach; ?>

			<?php else : ?>

				<div class="col-12 my-3">
					<div class="text-center">
						-- Maaf, Kami tidak dapat menemukan yang kamu cari --
					</div>
				</div>

			<?php endif; ?>

		</div>

		<!-- Paging -->
		<?= $this->pagination->create_links(); ?>

	</div>
</section>
<!--/ End Product Style 1  -->