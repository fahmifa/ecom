<!-- Breadcrumbs -->
<div class="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="bread-inner">
                    <ul class="bread-list">
                        <li><a href="<?= base_url(); ?>">Home<i class="ti-arrow-right"></i></a></li>
                        <li class="active"><a href="#">Checkout</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Breadcrumbs -->

<!-- Start Checkout -->
<section class="shop checkout section">
    <div class="container">
        <form method="POST" action="<?= base_url('home/processorder'); ?>">
            <div class="row">
                <div class="col-lg-8 col-12">
                    <div class="checkout-form">
                        <!-- Form -->
                        <div class="form">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-12">
                                    <div class="form-group">
                                        <label>Nama Lengkap<span>*</span></label>
                                        <input type="text" name="name" required="required" value="<?= userdata('nama_pengguna'); ?>" disabled>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
                                    <div class="form-group">
                                        <label>Alamat Email<span>*</span></label>
                                        <input type="email" name="email" required="required" value="<?= userdata('email'); ?>" disabled>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
                                    <div class="form-group">
                                        <label>Nama Perusahaan</label>
                                        <input type="text" name="nama_perusahaan">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-12">
                    <div class="order-details">
                        <!-- Order Widget -->
                        <div class="single-widget">
                            <h2>Informasi Pembelian</h2>
                            <div class="content">
                                <ul>
                                    <li class="text-truncate"> <?= $data_produk['nama_produk']; ?> <span><?= formatNumber($data_produk['harga_produk'], "Rp"); ?></span> </li>
                                    <li class="last">Total Harga<span><?= formatNumber($data_produk['harga_produk'], "Rp"); ?></span></li>
                                </ul>
                            </div>
                        </div>
                        <!--/ End Order Widget -->
                        <!-- Order Widget -->
                        <div class="single-widget">
                            <h2>Metode Pembayaran</h2>
                            <div class="content">
                                <div class="checkbox">
                                    <label class="checkbox-inline checked" for="1"><input type="checkbox" name="metode_pembayaran" id="1" checked="checked"> Transfer antar Bank (Verifikasi Manual)</label>
                                </div>
                            </div>
                        </div>
                        <!--/ End Order Widget -->
                        <!-- Payment Method Widget -->
                        <div class="single-widget payement">
                            <div class="content">
                                <img src="<?= base_url('assets/assets-home/images/payment-method.jpg'); ?>" alt="Payment Method">
                            </div>
                        </div>
                        <!--/ End Payment Method Widget -->
                        <!-- Button Widget -->
                        <div class="single-widget get-button">
                            <div class="content">
                                <div class="button">
                                    <button type="submit" class="btn">Proses Pembayaran</button>
                                </div>
                            </div>
                        </div>
                        <!--/ End Button Widget -->
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
<!--/ End Checkout -->