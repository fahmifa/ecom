<!-- Start Product Area -->
<div class="product-area section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-title">
                    <h2><?= $title ?></h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-12">
                        <img src="<?= base_url('assets/pengguna/toko/profil/' . $toko[0]->gambar_toko) ?>" class="img-profil rounded-circle mb-3 mx-auto d-block" alt="Cinque Terre">
                        <p class="text-center mb-2"><?= $toko[0]->deskripsi_toko ?></p>
                        <button class="btn btn-old text-white fo" data-id="<?= $toko[0]->id_toko ?>" type="button">Follow
                            <span class="badge badge-light"><?= $num ?></span>
                        </button>
                        <table class="table table-borderless mt-3" style="height: auto;">
                            <tbody>
                                <tr>
                                    <td>Owner</td>
                                    <td>: <?= $toko[0]->nama_pengguna ?></td>
                                </tr>
                                <tr>
                                    <td>No. HP</td>
                                    <td>: <?= $toko[0]->nomor_hp ?></td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td>: <?= $toko[0]->email ?></td>
                                </tr>
                                <tr>
                                    <td>Alamat</td>
                                    <td>: <?= $toko[0]->alamat_toko ?></td>
                                </tr>
                                <tr>
                                    <td>Bergabung</td>
                                    <td>: <?= $toko[0]->created_at ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-lg-8 col-md-8 col-12">
                        <div class="row">
                            <?php foreach ($toko as $row) : ?>
                                <div class="col-xl-3 col-lg-4 col-md-4 col-12">
                                    <div class="single-product">


                                        <?php
                                        $gam = gambar($row->id_produk_post);
                                        if ($gam) {
                                            $img = $gam[0]->gambar_produk;
                                        } else {
                                            $img = 'sample.jpg';
                                        }
                                        ?>


                                        <div class="product-img">
                                            <a target="_blank" href="<?= base_url('produk/' . $row->id_produk_post) ?>">
                                                <img class="default-img bg-img" src="<?= base_url('assets/pengguna/toko/produk/' . $img) ?>" alt="#">
                                            </a>


                                            <div class="button-head">
                                                <div class="product-action">

                                                    <a title="Lihat Produk" href="#" id="openModalProduk" data-toggle="modal" targetid="<?= $row->id_produk_post; ?>">
                                                        <i class="ti-eye"></i>
                                                    </a>

                                                    <?php if (in_array($row->id_produk_post, $produk_favorit)) : ?>
                                                        <a title="Hapus Favorit" href="#" data-toggle="modal" id="hapusFavorit" targetid="<?= $row->id_produk_post; ?>">
                                                            <i class="fa fa-heart text-success"></i>
                                                        </a>
                                                    <?php else : ?>
                                                        <a title="Favoritkan" href="#" data-toggle="modal" id="tambahFavorit" targetid="<?= $row->id_produk_post; ?>">
                                                            <i class="fa fa-heart-o"></i>
                                                        </a>
                                                    <?php endif; ?>

                                                </div>
                                                <div class="product-action-2">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-content">
                                            <div class="row mb-3">
                                                <div class="col">
                                                    <?php
                                                    $r = rating($row->id_produk_post);
                                                    $n = ($r != 0) ? $r : 0;
                                                    $m = 5 - $n;
                                                    ?>
                                                    <ul class="rating-stars">
                                                        <li class="stars-active">
                                                            <?php for ($a = 0; $a < $n; $a++) {  ?>
                                                                <i class="fa fa-star active"></i>
                                                            <?php } ?>
                                                            <?php for ($a = 0; $a < $m; $a++) {  ?>
                                                                <i class="fa fa-star"></i>
                                                            <?php } ?>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col">
                                                    <p class="label-rating text-muted"><?= $r ?> Rating</p>
                                                </div>
                                            </div>
                                            <h3><a href="<?= base_url('produk/' . $row->id_produk_post) ?>"><?= $row->nama_produk ?></a></h3>
                                            <div class="product-price">
                                                <span><?= formatNumber($row->harga_produk, "Rp "); ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>
<!-- End Product Area -->