<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" />
    <title>Upload Bukti Pembayaran</title>
</head>

<body>

    <div class="container d-flex justify-content-center align-items-center" style="min-height: 100vh;">
        <div class="col-12 col-md-9">
            <a href="<?= base_url('home/historytransaksi/pending'); ?>"><i class="fas fa-arrow-left"></i> Riwayat transaksi </a>
            <div class="card my-2">
                <div class="card-body px-5">
                    <h5 class="text-center py-3">Silahkan pilih gambar bukti pembayaran.</h5>

                    <?= $this->session->flashdata('msg'); ?>

                    <form method="POST" action="<?= base_url('home/uploadbuktipembayaran'); ?>" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="file">File</label>
                            <input type="hidden" name="id_pesanan" value="<?= $this->uri->segment(3); ?>">
                            <input id="file" class="form-control" type="file" name="bukti_bayar" accept=".jpg, .jpeg, .png">
                            <small class="text-secondary">*Catatan: File yang anda pernah kirim sebelumnya akan tertimpa oleh file baru yang anda upload.</small>
                        </div>
                        <button class="btn btn-success w-100 my-3">Kirim</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
</body>

</html>