</head>

<body class="js">

    <!-- Preloader -->
    <div class="preloader">
        <div class="preloader-inner">
            <div class="preloader-icon">
                <span></span>
                <span></span>
            </div>
        </div>
    </div>
    <!-- End Preloader -->

    <!-- Header -->
    <header class="header shop">
        <!-- Topbar -->
        <div class="topbar d-none d-md-block">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-12 col-12">
                        <!-- Top Left -->
                        <div class="top-left">
                            <ul class="list-main">
                                <li><i class="ti-headphone-alt"></i> +060 (800) 801-582</li>
                                <li><i class="ti-email"></i> support@shophub.com</li>
                            </ul>
                        </div>
                        <!--/ End Top Left -->
                    </div>
                    <div class="col-lg-8 col-md-12 col-12">
                        <!-- Top Right -->
                        <div class="right-content">
                            <ul class="list-main">
                                <li><i class="ti-location-pin"></i> Store location</li>
                                <li><i class="ti-alarm-clock"></i> <a href="#">Daily deal</a></li>
                                <li><i class="ti-user"></i> <a href="#">My account</a></li>
                                <li><i class="ti-power-off"></i><a href="login.html#">Login</a></li>
                            </ul>
                        </div>
                        <!-- End Top Right -->
                    </div>
                </div>
            </div>
        </div>
        <!-- End Topbar -->
        <div class="middle-inner py-md-0 d-block d-lg-none">
            <div class="container">
                <div class="row">
                    <div class="col-12 d-md-flex justify-content-md-center">
                        <!-- Logo -->
                        <div class="logo my-md-3 d-none d-md-block">
                            <a href="<?= base_url(); ?>">
                                <h3>AweShop.</h3>
                                <!-- <img src="<?= base_url('assets/assets-home/images/logo.png'); ?>" alt="logo"> -->
                            </a>
                        </div>
                        <!--/ End Logo -->

                        <div class="d-flex align-items-center d-md-none">
                            <div class="sinlge-bar shopping mr-3">
                                <a href="#" class="single-icon"><i class="fa fa-user-circle-o" aria-hidden="true" style="color: #333!important; font-size: 20px;"></i></a>
                                <!-- Shopping Item -->
                                <div class="shopping-item p-0" style="top: 30px; left: 10px; max-width: 300px; width: 100%;">
                                    <div class="list-group">
                                        <?php if (userdata('id_pengguna')) : ?>
                                            <a href="<?= base_url('pengguna/profile'); ?>" class="list-group-item list-group-item-action border-0"><i class="fa fa-user fa-fw"></i> Profile</a>
                                            <a href="<?= base_url('pengguna/myitems'); ?>" class="list-group-item list-group-item-action border-0"><i class="fa fa-shopping-bag fa-fw"></i> Item saya</a>
                                            <a href="<?= base_url('pengguna/logout'); ?>" class="list-group-item list-group-item-action border-0"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                                        <?php else : ?>
                                            <a href="<?= base_url('auth/login'); ?>" class="list-group-item list-group-item-action border-0"><i class="fa fa-sign-in fa-fw"></i> Login</a>
                                            <a href="<?= base_url('auth/daftar'); ?>" class="list-group-item list-group-item-action border-0"><i class="fa fa-sign-in fa-fw"></i> Daftar</a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <!--/ End Shopping Item -->
                            </div>
                            <div class="sinlge-bar mr-3">
                                <a href="<?= base_url('home/historytransaksi/pending'); ?>" class="single-icon"><i class="fa fa-history" aria-hidden="true" style="color: #333!important; font-size: 20px;"></i></a>
                            </div>
                            <div class="sinlge-bar mr-3">
                                <a href="<?= base_url('home/myfavorite'); ?>" class="single-icon"><i class="fa fa-heart-o" aria-hidden="true" style="color: #333!important; font-size: 20px;"></i></a>
                            </div>
                        </div>

                        <!-- Search Form -->
                        <div class="search-top">
                            <div class="top-search"><a href="#0"><i class="ti-search"></i></a></div>
                            <!-- Search Form -->
                            <div class="search-top">
                                <form method="GET" action="<?= base_url('home/search'); ?>" class="search-form">
                                    <input type="text" placeholder="Cari nama produk disini" name="q">
                                    <button value="search" type="submit"><i class="ti-search"></i></button>
                                </form>
                            </div>
                            <!--/ End Search Form -->
                        </div>
                        <!--/ End Search Form -->

                        <div class="mobile-nav" style="margin-top: -25px;"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Header Inner -->
        <div class="header-inner">
            <div class="container">
                <div class="cat-nav-head">
                    <div class="row">
                        <div class="col-12">
                            <div class="menu-area">
                                <!-- Main Menu -->
                                <nav class="navbar navbar-expand-lg">
                                    <div class="navbar-collapse">
                                        <div class="nav-inner d-flex justify-content-between w-100">
                                            <ul class="nav main-menu menu navbar-nav">
                                                <li class="d-flex align-items-center">
                                                    <a href="<?= base_url(); ?>" class="py-0 bg-transparent d-none d-lg-block">
                                                        <h3>AweShop.</h3>
                                                        <!-- <img src="<?= base_url('assets/assets-home/images/logo.png'); ?>" alt="logo"> -->
                                                    </a>
                                                </li>
                                                <li class="active"><a href="#">Home</a></li>
                                                <li><a href="#">Product</a></li>
                                                <li><a href="#">Service</a></li>
                                                <li><a href="#">Shop<i class="ti-angle-down"></i><span class="new">New</span></a>
                                                    <ul class="dropdown">
                                                        <li><a href="shop-grid.html">Shop Grid</a></li>
                                                        <li><a href="cart.html">Cart</a></li>
                                                        <li><a href="checkout.html">Checkout</a></li>
                                                    </ul>
                                                </li>
                                                <li><a href="#">Pages</a></li>
                                                <li><a href="#">Blog<i class="ti-angle-down"></i></a>
                                                    <ul class="dropdown">
                                                        <li><a href="blog-single-sidebar.html">Blog Single Sidebar</a></li>
                                                    </ul>
                                                </li>
                                                <li><a href="contact.html">Contact Us</a></li>
                                            </ul>
                                            <div class="d-flex align-items-center">
                                                <!-- Search Form -->
                                                <div class="sinlge-bar mr-3">
                                                    <a href="<?= base_url('home/myfavorite'); ?>" class="single-icon" style="color: white;"><i class="fa fa-heart-o" aria-hidden="true" style="font-size: 20px;"></i></a>
                                                </div>
                                                <div class="sinlge-bar mr-3">
                                                    <a href="<?= base_url('home/historytransaksi/pending'); ?>" class="single-icon" style="color: white;"><i class="fa fa-history" aria-hidden="true" style="font-size: 20px;"></i></a>
                                                </div>
                                                <div class="sinlge-bar shopping mr-3">
                                                    <a href="#" class="single-icon" style="color: white;"><i class="fa fa-user-circle-o" aria-hidden="true" style="font-size: 20px;"></i></a>
                                                    <!-- Shopping Item -->
                                                    <div class="shopping-item p-0" style="top: 40px;">
                                                        <div class="list-group">
                                                            <?php if (userdata('id_pengguna')) : ?>
                                                                <a href="<?= base_url('pengguna/profile'); ?>" class="list-group-item list-group-item-action border-0"><i class="fa fa-user fa-fw"></i> Profile</a>
                                                                <a href="<?= base_url('pengguna/myitems'); ?>" class="list-group-item list-group-item-action border-0"><i class="fa fa-shopping-bag fa-fw"></i> Item saya</a>
                                                                <a href="<?= base_url('pengguna/logout'); ?>" class="list-group-item list-group-item-action border-0"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                                                            <?php else : ?>
                                                                <a href="<?= base_url('auth/login'); ?>" class="list-group-item list-group-item-action border-0"><i class="fa fa-sign-in fa-fw"></i> Login</a>
                                                                <a href="<?= base_url('auth/daftar'); ?>" class="list-group-item list-group-item-action border-0"><i class="fa fa-sign-in fa-fw"></i> Daftar</a>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                    <!--/ End Shopping Item -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </nav>
                                <!--/ End Main Menu -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/ End Header Inner -->
    </header>
    <!--/ End Header -->