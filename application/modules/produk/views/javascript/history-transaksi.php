<script>
    $(document).ready(function() {
        let baseUrl = `<?= base_url('home'); ?>`;

        $('button#batalkan_pesanan').click(function() {
            let id_pesanan = $(this).attr('target');
            Swal.fire({
                text: 'Apakah Anda yakin ingin membatalkan pesanan?',
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonText: 'Tutup',
                confirmButtonText: 'Batalkan Pesanan!',
                reverseButtons: true,
            }).then((result) => {
                if (result.isConfirmed) {
                    batalkanPesanan(id_pesanan);
                }
            });
        });

        function batalkanPesanan(id_pesanan) {
            loadingStart();
            $.ajax({
                url: `${ baseUrl }/api/batalkanpesanan`,
                method: "POST",
                data: {
                    id_pesanan
                },
            }).done((res) => {
                if (res.kode) {
                    location.reload();
                } else {
                    sweetalert('error', 'Oops...', 'Gagal membatalkan pesanan, silahkan muat ulang halaman dan coba lagi!');
                }
            }).fail(() => {
                sweetalert('error', 'Oops...', 'Ada kesalahan pada server!');
            }).always(() => {
                loadingStop();
            });
        }
    });
</script>