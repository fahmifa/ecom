<!-- Jquery -->
<script src="<?= base_url('assets/assets-home/js/jquery.min.js'); ?>"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

<script src="<?= base_url('assets/assets-home/js/jquery-migrate-3.0.0.js'); ?>"></script>
<script src="<?= base_url('assets/assets-home/js/jquery-ui.min.js'); ?>"></script>
<!-- Popper JS -->
<script src="<?= base_url('assets/assets-home/js/popper.min.js'); ?>"></script>
<!-- Bootstrap JS -->
<script src="<?= base_url('assets/assets-home/js/bootstrap.min.js'); ?>"></script>

<!-- Slicknav JS -->
<script src="<?= base_url('assets/assets-home/js/slicknav.min.js'); ?>"></script>
<!-- Owl Carousel JS -->
<script src="<?= base_url('assets/assets-home/js/owl-carousel.js'); ?>"></script>
<!-- Magnific Popup JS -->
<script src="<?= base_url('assets/assets-home/js/magnific-popup.js'); ?>"></script>
<!-- Waypoints JS -->
<script src="<?= base_url('assets/assets-home/js/waypoints.min.js'); ?>"></script>
<!-- Countdown JS -->
<script src="<?= base_url('assets/assets-home/js/finalcountdown.min.js'); ?>"></script>
<!-- Nice Select JS -->
<script src="<?= base_url('assets/assets-home/js/nicesellect.js'); ?>"></script>
<!-- Flex Slider JS -->
<script src="<?= base_url('assets/assets-home/js/flex-slider.js'); ?>"></script>
<!-- ScrollUp JS -->
<script src="<?= base_url('assets/assets-home/js/scrollup.js'); ?>"></script>
<!-- Onepage Nav JS -->
<script src="<?= base_url('assets/assets-home/js/onepage-nav.min.js'); ?>"></script>
<!-- Easing JS -->
<script src="<?= base_url('assets/assets-home/js/easing.js'); ?>"></script>
<!-- Active JS -->
<script src="<?= base_url('assets/assets-home/js/active.js'); ?>"></script>

<!-- jquery loading -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easy-loading/1.3.0/jquery.loading.js" integrity="sha512-l9jYjbia7nXf4ZpR3dFSAjOOygUAytRrqmT32a5cBZjVpIUdFgBzIPQPPhJ6gh/NwaIerUEsn3vkEVQzQExGag==" crossorigin="anonymous"></script>

<!-- sweetalert -->
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<!-- sweetalert -->

<script src="<?= base_url('assets/assets-home/js/') ?>rater.min.js"></script>
<!-- Custom -->
<script src="<?= base_url('assets/js/global.js'); ?>"></script>
<script>
    var options = {
        max_value: 5,
        step_size: 1,
        initial_value: 0,
        selected_symbol_type: 'utf8_star', // Must be a key from symbols
        cursor: 'default',
        readonly: false,
        change_once: false, // Determines if the rating can only be set once
        ajax_method: 'POST',
        url: '<?= base_url('rating/' . $this->uri->segment(2)) ?>',
        additional_data: {} // Additional data to send to the server
    }

    $(".rating").rate(options);

    $(document).ready(function() {
        let baseUrl = `<?= base_url('home'); ?>`,
            iconfavorit = 'fa-heart-o',
            iconunfavorit = 'fa-heart text-success';

        $(document).on('click', '#openModalProduk', function() {
            let id_produk = $(this).attr('targetid');
            getProduk(id_produk);
        });

        function getProduk(id_produk) {
            loadingStart();
            $.ajax({
                url: `${ baseUrl }/apimodalproduk`,
                method: "POST",
                data: {
                    id_produk
                }
            }).done((res) => {
                if (res.data_produk) {
                    let modalParent = $('div#modalproduk');
                    let gambar_produk = '';

                    res.gambar_produk.forEach((data) => {
                        gambar_produk += `<div class="list_gambar_produk col-3 border text-center" style="cursor: pointer;"><img class="produk-selector" src="<?= base_url('assets/pengguna/toko/produk/'); ?>${data.gambar_produk}" alt="Gambar Produk"></div>`
                    });

                    modalParent.find('#gambar_utama').html(`<img class="modal-produk-thumbnail" src="<?= base_url('assets/pengguna/toko/produk/'); ?>${res.gambar_produk[0].gambar_produk}" alt="Gambar Produk">`);
                    modalParent.find('#gambar_produk').html(gambar_produk);
                    modalParent.find('#terjual').text(formatNumber(res.data_produk.terjual));
                    modalParent.find('#nama_produk').text(res.data_produk.nama_produk);
                    modalParent.find('#harga_produk').text("Rp" + formatNumber(res.data_produk.harga_produk));
                    modalParent.find('#deskripsi_produk').text(res.data_produk.deskripsi_produk);
                    modalParent.find('input[name="id_produk"]').val(res.data_produk.id_produk_post);
                    modalParent.modal('show');
                    console.log(res);
                }
            }).fail((res) => {
                // console.log(res);
            }).always(() => {
                loadingStop();
            });
        }

        $(document).on('click', '.list_gambar_produk', function() {
            let selector = $(this).children().attr('src');
            let gambar_utama = $('.modal-produk-thumbnail');
            gambar_utama.attr('src', selector);
        });


        $(document).on('click', '.list_gambar_produk', function() {
            let selector = $(this).children().attr('src');
            let gambar_utama = $('.modal-bg-thumbnail');
            gambar_utama.attr('src', selector);
        });

        $(document).on('click', '#tambahFavorit', function() {
            let elm = $(this);
            let id_produk = elm.attr('targetid');
            tambahFavorit(elm, id_produk);
        });

        function tambahFavorit(elm, id_produk) {
            $.ajax({
                url: `${ baseUrl }/api/tambahfavorit`,
                method: "POST",
                data: {
                    id_produk
                }
            }).done((res) => {
                if (res.kode) {
                    elm.attr({
                        'title': 'Hapus Favorit',
                        'id': 'hapusFavorit'
                    });
                    elm.children().addClass(iconunfavorit).removeClass(iconfavorit);
                }
            });
        }

        $(document).on('click', '#hapusFavorit', function() {
            let elm = $(this);
            let id_produk = elm.attr('targetid');
            hapusFavorit(elm, id_produk);
        });

        function hapusFavorit(elm, id_produk) {
            $.ajax({
                url: `${ baseUrl }/api/hapusfavorit`,
                method: "POST",
                data: {
                    id_produk
                }
            }).done((res) => {
                if (res.kode) {
                    elm.attr({
                        'title': 'Tambah Favorit',
                        'id': 'tambahFavorit'
                    });
                    elm.children().addClass(iconfavorit).removeClass(iconunfavorit);
                }
            });
        }


    });
</script>