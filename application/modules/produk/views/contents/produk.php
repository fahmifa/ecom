<!-- Start Product Area -->
<div class="product-area section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-title">
                    <h2><?= $title ?></h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-12">
                        <div class="row">
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">

                                <div id="gambar_utama">
                                    <img class="modal-bg-thumbnail" src="<?= base_url('assets/pengguna/toko/produk/' . $produk[0]->gambar_produk) ?>" alt="Gambar Produk">
                                </div>

                                <div class="row no-gutters" id="gambar_produk">
                                    <?php foreach ($produk as $row) : ?>
                                        <div class="list_gambar_produk col-3 border text-center" style="cursor: pointer;">
                                            <img class="bg-img" src="<?= base_url('assets/pengguna/toko/produk/' . $row->gambar_produk) ?>" alt="Gambar Produk">
                                        </div>
                                    <?php endforeach; ?>
                                </div>

                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                <div class="quickview-content">
                                    <a href="<?= base_url('toko/' . $row->id_toko) ?>">
                                        <h2 class="bot mb-3 text-capitalize"><?= $row->nama_toko ?></h2>
                                    </a>
                                    <h5 id="nama_produk">sepatu</h5>
                                    <div class="quickview-ratting-review">
                                        <span>
                                            Terjual:
                                            <span id="terjual"><?= sold($row->id_produk_post) ?></span>
                                        </span>
                                    </div>
                                    <h3 id="harga_produk"><?= formatNumber($produk[0]->harga_produk, "Rp "); ?></h3>
                                    <div class="quickview-peragraph">
                                        <p id="deskripsi_produk" class="mb-3"></p>
                                    </div>
                                    <form method="POST" action="<?= base_url('home/checkout'); ?>" class="mb-3">
                                        <input type="hidden" name="id_produk" value="<?= $produk[0]->id_produk_post ?>">
                                        <?php if (userdata('id_pengguna')) : ?>
                                            <div class="add-to-cart">
                                                <button type="submit" class="btn" id="btnBeli">Beli Sekarang</button>
                                            </div>
                                    </form>
                                <?php else : ?>
                                    <a href="<?= base_url('auth/login'); ?>" class="btn btn-new text-white">Login!</a>
                                <?php endif; ?>



                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-12">
                        <div class="shop-sidebar">
                            <div class="single-widget category">
                                <h3 class="title">Pembeli <?= $title ?></h3>
                                <ul class="list-group">
                                    <?php foreach ($pe as $row) : ?>
                                        <li class="list-group-item d-flex justify-content-between align-items-center">
                                            <?= $row->nama_pengguna ?>
                                            <span class="badge badge-primary badge-pill"><?= $row->total ?></span>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>
<!-- End Product Area -->