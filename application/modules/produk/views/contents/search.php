<!-- Breadcrumbs -->
<div class="breadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="bread-inner">
					<ul class="bread-list">
						<li><a href="<?= base_url(); ?>">Home<i class="ti-arrow-right"></i></a></li>
						<li class="active"><a href="#">Pencarian</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- End Breadcrumbs -->

<!-- Product Style -->
<section class="product-area shop-sidebar shop section pt-3">

	<div class="container">

		<p class="mb-3"><?= $total; ?> Produk ditemukan hasil pencarian dari : <strong><?= $pencarian; ?></strong></p>

		<form action="" method="get">
			<div class="input-group mb-3">
				<input type="text" name="q" class="form-control px-3 border-right-0" placeholder="Cari yang lain yuk...">
				<div class="input-group-append">
					<button class="btn" type="button">Cari</button>
				</div>
			</div>
		</form>

		<div class="row">
			<div class="col-lg-3 col-md-4 col-12">
				<div class="shop-sidebar">
					<!-- Single Widget -->
					<div class="single-widget category">
						<h3 class="title">Kategori</h3>
						<ul class="categor-list">
							<li><a class="<?= !$this->input->get('k') ? 'bg-success text-white w-100 px-2' : 'w-100'; ?>" href="<?= $full_url; ?>">Semua</a></li>
							<?php foreach ($kategori as $result) : ?>
								<li><a class="<?= $this->input->get('k') === $result['id_kategori'] ? 'bg-success text-white w-100 px-2' : 'w-100'; ?>" href="<?= $full_url . "&k=" . $result['id_kategori']; ?>"><?= $result['nama_kategori']; ?></a></li>
							<?php endforeach; ?>
							<li><a class="<?= $this->input->get('k') === $id_kategori_lainnya ? 'bg-success text-white w-100 px-2' : 'w-100'; ?>" href="<?= $full_url . "&k=" . $id_kategori_lainnya; ?>">Lainnya</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-lg-9 col-md-8 col-12">
				<div class="row">
					<div class="col-12">
						<!-- Shop Top -->
						<div class="shop-top">
							<form action="" method="get">
								<input type="hidden" name="q" value="<?= $this->input->get('q'); ?>">
								<input type="hidden" name="k" value="<?= $this->input->get('k'); ?>">
								<div class="shop-shorter w-100 d-flex flex-wrap">
									<div class="single-shorter w-auto">
										<label>Saring berdasarkan :</label>
										<select name="f">
											<option <?= $this->input->get('f') === 'nama_produk' ? 'selected' : ''; ?> value="nama_produk">Nama</option>
											<option <?= $this->input->get('f') === 'harga_produk' ? 'selected' : ''; ?> value="harga_produk">Harga</option>
											<option <?= $this->input->get('f') === 'created_at' || $this->input->get('f') === '' ? 'selected' : ''; ?> value="created_at">Waktu</option>
										</select>
										<label class="ml-0 ml-md-3">Urutkan berdasarkan :</label>
										<select name="s">
											<option <?= $this->input->get('s') === 'asc' ? 'selected' : ''; ?> value="asc">A-z</option>
											<option <?= $this->input->get('s') === 'desc' || $this->input->get('s') === '' ? 'selected' : ''; ?> value="desc">Z-a</option>
										</select>
									</div>
									<button class="btn-default btn-sm ml-0 ml-md-3 mt-2 mt-md-0"><i class="fa fa-search fa-fw"></i></button>
								</div>
							</form>
						</div>
						<!--/ End Shop Top -->
					</div>
				</div>
				<div class="row">

					<?php if ($produk) : ?>
						<?php foreach ($produk as $result) : ?>

							<div class="col-lg-4 col-md-6 col-12">
								<div class="single-product">
									<div class="product-img">
										<img class="default-img produk-selector" id="openModalProduk" style="height: 150px; max-height: 150px;" targetid="<?= $result['id_produk_post']; ?>" src="<?= base_url('assets/pengguna/toko/produk/') . $result['gambar_produk']; ?>" alt="Gambar Produk">
										<div class="button-head text-center">
											<div class="product-action mr-2">
												<a title="Lihat Produk" href="#" id="openModalProduk" data-toggle="modal" targetid="<?= $result['id_produk_post']; ?>">
													<i class="ti-eye"></i>
												</a>

												<?php if (in_array($result['id_produk_post'], $produk_favorit)) : ?>
													<a title="Hapus Favorit" href="#" data-toggle="modal" id="hapusFavorit" targetid="<?= $result['id_produk_post']; ?>">
														<i class="fa fa-heart text-success"></i>
														<!-- <span>Favoritkan</span> -->
													</a>
												<?php else : ?>
													<a title="Favoritkan" href="#" data-toggle="modal" id="tambahFavorit" targetid="<?= $result['id_produk_post']; ?>">
														<i class="fa fa-heart-o"></i>
													</a>
												<?php endif; ?>

											</div>
											<!-- <div class="product-action-2">
												<a title="Masukkan ke Keranjang" href="#">Masukkan ke Keranjang</a>
											</div> -->
										</div>
									</div>
									<div class="product-content">
										<p><?= $result['nama_produk']; ?></p>
										<div class="product-price">
											<span><?= formatNumber($result['harga_produk'], "Rp "); ?></span>
										</div>
									</div>
								</div>
							</div>

						<?php endforeach; ?>

					<?php else : ?>

						<div class="col-12 my-3">
							<div class="text-center">
								-- Maaf, Kami tidak dapat menemukan yang kamu cari --
							</div>
						</div>

					<?php endif; ?>

				</div>

				<!-- Paging -->
				<?= $this->pagination->create_links(); ?>

			</div>
		</div>
	</div>
</section>
<!--/ End Product Style 1  -->