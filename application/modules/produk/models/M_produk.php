<?php

defined('BASEPATH') or exit('No direct script access allowed');

class M_produk extends CI_Model
{

    public function view_all($table)
    {
        $query = $this->db->get($table);
        return $query->result();
    }


    function check($table, $where)
    {
        $query = $this->db->get_where($table, $where);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }


    public function insert_all($table, $data)
    {
        $this->db->insert($table, $data);
    }

    public function update_all($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }



    public function join($table, $sama, $where)
    {
        $this->db->select('*');
        $this->db->from($table[0]);
        if ($where != 0) {
            $this->db->where($where);
        }
        $gb = count($sama);
        for ($i = 1; $i < $gb; $i++) {

            $this->db->join($table[$i], $sama[$i]);
        }
        $query = $this->db->get();
        return $query;
    }

    public function join_cus($sel, $table, $sama, $where)
    {
        $this->db->select($sel);
        $this->db->from($table[0]);
        $gb = count($sama);
        for ($i = 1; $i < $gb; $i++) {

            $this->db->join($table[$i], $sama[$i], 'left');
        }
        if ($where != 0) {
            $this->db->where($where);
        }

        $this->db->group_by('tbl_pengguna.nama_pengguna');
        $query = $this->db->get();
        return $query;
    }


    public function numdata($table, $data)
    {
        $this->db->select("*");
        $this->db->from($table);
        $this->db->where($data);
        $query = $this->db->get();
        return $query->num_rows();
    }


    public function avg($table, $data, $av)
    {
        $this->db->select_avg($av);
        $this->db->from($table);
        $this->db->where($data);
        $query = $this->db->get();
        return $query->result();
    }


    public function getWhereEmail($email)
    {
        return $this->db->get_where('tbl_pengguna', ['email' => $email]);
    }

    public function selectProdukWhere($q, $f, $s, $k, $limit, $start)
    {
        $joinTblGambar = "(SELECT id_produk_post, gambar_produk FROM tbl_produk_gambar WHERE is_thumbnail = 1) tbl_produk_gambar";

        $this->db->select('tbl_produk_post.*, tbl_produk_gambar.gambar_produk')
            ->from('tbl_produk_post')
            ->join($joinTblGambar, 'tbl_produk_gambar.id_produk_post = tbl_produk_post.id_produk_post', "LEFT")
            ->where('tbl_produk_post.status_post', "aktif")
            ->where('tbl_produk_post.status_live', "live")
            ->where('tbl_produk_post.is_deleted', 0);

        if ($k) {
            $this->db->where('tbl_produk_post.id_kategori', $k);
        }

        $this->db->like('tbl_produk_post.nama_produk', $q)
            ->or_like('tbl_produk_post.deskripsi_produk', $q)
            ->order_by("tbl_produk_post.$f", $s)
            ->limit($limit, $start);


        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function concatIdKategori()
    {
        return $this->db->query("SELECT GROUP_CONCAT(id_kategori) AS id_kategori FROM tbl_produk_kategori WHERE id_kategori != '12345'")->row_array();
    }

    public function totalProdukWhere($q, $k)
    {
        $this->db->from('tbl_produk_post')
            ->where('status_post', "aktif")
            ->where('status_live', "live")
            ->where('is_deleted', 0);

        if ($k) {
            $this->db->where('tbl_produk_post.id_kategori', $k);
        }

        $this->db->like('tbl_produk_post.nama_produk', $q)
            ->or_like('tbl_produk_post.deskripsi_produk', $q);
        return $this->db->count_all_results();
    }

    public function getKategori()
    {
        $result = $this->db->query("SELECT `id_kategori`, `nama_kategori` FROM tbl_produk_kategori WHERE id_kategori != '12345' ORDER BY `nama_kategori` ASC");

        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return null;
        }
    }

    public function getProdukWhere($id_produk)
    {
        $this->db->select('tbl_produk_post.*, tbl_pengguna_toko.*')
            ->from('tbl_produk_post')
            ->join('tbl_pengguna_toko', 'tbl_pengguna_toko.id_toko = tbl_produk_post.id_toko')
            ->where('tbl_produk_post.status_post', "aktif")
            ->where('tbl_produk_post.status_live', "live")
            ->where('tbl_produk_post.is_deleted', 0)
            ->where('tbl_produk_post.id_produk_post', $id_produk);

        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array()[0];
        } else {
            return false;
        }
    }

    public function getGambarProdukWhere($id_produk)
    {
        $this->db->select('id_produk_gambar, id_produk_post, gambar_produk')
            ->from('tbl_produk_gambar')
            ->where('id_produk_post', $id_produk)
            ->order_by('is_thumbnail', 'desc')
            ->order_by('created_at', 'asc');

        return $this->db->get()->result_array();
    }

    public function processorder($obj)
    {
        $this->db->insert('tbl_pengguna_pesanan', $obj);
        return $this->db->affected_rows();
    }

    public function getPesananWhere($id_pesanan, $id_pengguna)
    {
        $this->db
            ->where('id_pesanan', $id_pesanan)
            ->where('id_pengguna', $id_pengguna)
            ->where_in('status_pesanan', ['menunggu_pembayaran', 'verifikasi_pembayaran']);

        $result = $this->db->get('tbl_pengguna_pesanan');
        return $result;
    }

    public function uploadBuktiPembayaran($obj)
    {
        $where = [
            'id_pesanan'    => $obj['id_pesanan'],
            'id_pengguna'   => $obj['id_pengguna']
        ];
        $this->db->insert('tbl_bukti_pembayaran', $obj);
        $this->db->set('status_pesanan', 'verifikasi_pembayaran');
        $this->db->where($where);
        $this->db->update('tbl_pengguna_pesanan');
        return $this->db->affected_rows();
    }

    public function getBuktiPembayaranWhere($id_pesanan, $id_pengguna)
    {
        return $this->db->get_where('tbl_bukti_pembayaran', ['id_pesanan' => $id_pesanan, 'id_pengguna' => $id_pengguna]);
    }

    public function deleteBuktiPembayaranWhere($id_pesanan, $id_pengguna)
    {
        $this->db->delete('tbl_bukti_pembayaran', ['id_pesanan' => $id_pesanan, 'id_pengguna' => $id_pengguna]);
    }

    public function insertHistoryTransaksi($data)
    {
        $this->db->insert('tbl_history_transaksi', $data);
    }

    public function selectPesananWhere($id_pengguna, $section, $limit, $start, $q)
    {
        $joinTblGambar = "(SELECT * FROM tbl_produk_gambar WHERE is_thumbnail = 1) tbl_produk_gambar";
        $this->db->select("
                tbl_pengguna_pesanan.*,
                tbl_produk_post.nama_produk,
                tbl_produk_gambar.gambar_produk
            ")
            ->from('tbl_pengguna_pesanan')
            ->join('tbl_produk_post', 'tbl_produk_post.id_produk_post = tbl_pengguna_pesanan.id_produk_post')
            ->join($joinTblGambar, 'tbl_produk_gambar.id_produk_post = tbl_produk_post.id_produk_post')
            ->where('tbl_pengguna_pesanan.id_pengguna', $id_pengguna)
            ->like('nama_produk', $q)
            ->order_by('tbl_pengguna_pesanan.created_at', 'desc')
            ->limit($limit, $start);

        if ($section === 'pending') {
            $this->db->where_in("tbl_pengguna_pesanan.status_pesanan", ['menunggu_pembayaran', 'sedang_diproses', 'verifikasi_pembayaran']);
        } elseif ($section === 'dibatalkan') {
            $this->db->where('tbl_pengguna_pesanan.status_pesanan', 'dibatalkan');
        } elseif ($section === 'selesai') {
            $this->db->where('tbl_pengguna_pesanan.status_pesanan', 'pesanan_selesai');
        }

        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function totalHistoryPesanan($id_pengguna, $section, $q)
    {

        $this->db->select("
                tbl_pengguna_pesanan.*,
                tbl_produk_post.nama_produk,
                tbl_produk_gambar.gambar_produk
            ")
            ->from('tbl_pengguna_pesanan')
            ->join('tbl_produk_post', 'tbl_produk_post.id_produk_post = tbl_pengguna_pesanan.id_produk_post')
            ->where('tbl_pengguna_pesanan.id_pengguna', $id_pengguna)
            ->like('tbl_produk_post.nama_produk', $q);

        if ($section === 'pending') {
            $this->db->where_in("tbl_pengguna_pesanan.status_pesanan", ['menunggu_pembayaran', 'sedang_diproses', 'verifikasi_pembayaran']);
        } elseif ($section === 'dibatalkan') {
            $this->db->where('tbl_pengguna_pesanan.status_pesanan', 'dibatalkan');
        } elseif ($section === 'selesai') {
            $this->db->where('tbl_pengguna_pesanan.status_pesanan', 'pesanan_selesai');
        }
        return $this->db->count_all_results();
    }

    public function selectFavoriteWhere($id_pengguna)
    {
        return $this->db->query("SELECT GROUP_CONCAT(id_produk) AS result FROM `tbl_favorite_produk` WHERE id_pengguna = $id_pengguna")->result_array();
    }

    public function getIdKategoriLainnya()
    {
        return $this->db->get_where('tbl_produk_kategori', ['nama_kategori' => 'Lainnya'])->row_array();
    }

    public function selectFavoriteProduk($id_pengguna, $q, $limit, $start)
    {
        $joinTblGambar = "(SELECT * FROM tbl_produk_gambar WHERE is_thumbnail = 1) tbl_produk_gambar";
        $this->db
            ->select("tbl_favorite_produk.*, tbl_produk_post.*, tbl_produk_gambar.gambar_produk")
            ->from('tbl_favorite_produk')
            ->join('tbl_produk_post', 'tbl_produk_post.id_produk_post = tbl_favorite_produk.id_produk')
            ->join($joinTblGambar, 'tbl_produk_gambar.id_produk_post = tbl_produk_post.id_produk_post', "LEFT")
            ->where('tbl_produk_post.status_post', "aktif")
            ->where('tbl_produk_post.status_live', "live")
            ->where('tbl_produk_post.is_deleted', 0)
            ->where('tbl_favorite_produk.id_pengguna', $id_pengguna)
            ->like('tbl_produk_post.nama_produk', $q)
            ->limit($limit, $start);


        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function totalFavoriteProduk($id_pengguna, $q)
    {
        $this->db
            ->select("tbl_favorite_produk.*, tbl_produk_post.*")
            ->from('tbl_favorite_produk')
            ->join('tbl_produk_post', 'tbl_produk_post.id_produk_post = tbl_favorite_produk.id_produk')
            ->where('tbl_produk_post.status_post', "aktif")
            ->where('tbl_produk_post.status_live', "live")
            ->where('tbl_produk_post.is_deleted', 0)
            ->where('tbl_favorite_produk.id_pengguna', $id_pengguna)
            ->like('tbl_produk_post.nama_produk', $q);


        return $this->db->count_all_results();
    }
}
