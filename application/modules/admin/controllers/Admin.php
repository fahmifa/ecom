<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_admin', 'admin');
		// if (!userdata('id_admin')) {
		// 	redirect(base_url('auth/login'));
		// }

		// CI Paging
		$this->load->library('pagination');
		$this->full_tag_open	=  '<nav><ul class="pagination justify-content-end">';
		$this->full_tag_close	=  '</ul></nav>';
		$this->first_link		= 'First';
		$this->first_tag_open	= '<li class="page-item">';
		$this->first_tag_close	= '</li>';
		$this->last_link		= 'Last';
		$this->last_tag_open	= '<li class="page-item">';
		$this->last_tag_close	= '</li>';
		$this->next_link		= '&gt;';
		$this->next_tag_open	= '<li class="page-item">';
		$this->next_tag_close	= '</li>';
		$this->prev_link		= '&lt;';
		$this->prev_tag_open	= '<li class="page-item">';
		$this->prev_tag_close	= '</li>';
		$this->cur_tag_open		= '<li class="page-item active"><a class="page-link" href="#">';
		$this->cur_tag_close	= '</a></li>';
		$this->num_tag_open		= '<li class="page-item">';
		$this->num_tag_close	= '</li>';
		$this->attributes		= ['class' => 'page-link'];
	}

	public function index()
	{
		$data['pengajuan_live'] = $this->admin->dashboardPengajuanLive();
		$data['komplain_pembeli'] = $this->admin->dashboardKomplainProduk();
		$data['pesanan_baru'] = $this->admin->dashboardPesananBaru();
		$data['pengajuan_dana'] = $this->admin->dashboardPengajuanDana();
		// print_r($data['pengajuan_dana']);
		// die();
		$this->load->view('templates/header', $data);
		$this->load->view('contents/dashboard');
		$this->load->view('templates/footer');
		$this->load->view('cores/dashboard');
	}

	public function toko($id_toko)
	{
		$toko = $this->admin->getTokoPenggunaWhere($id_toko);

		if ($toko->num_rows() > 0) {
			$data['toko'] = $toko->row_array();
			$data['latest_produk'] = $this->admin->selectProdukWhere($id_toko);
			$this->load->view('templates/header', $data);
			$this->load->view('contents/toko');
			$this->load->view('templates/footer');
			// $this->load->view('cores/dashboard');
		} else {
			exit('Not Found!');
		}
	}

	public function komplain_produk($id_komplain = false)
	{
		$komplain = $this->admin->getKomplainProdukWhere($id_komplain);

		if ($komplain) {
			$data['data_komplain'] = $komplain;
			$data['data_konversasi'] = $this->admin->dataKonversasiKomplain($id_komplain);

			// print_r($data['data_konversasi']);
			// die();

			$this->load->view('templates/header', $data);
			$this->load->view('contents/komplain-produk');
			$this->load->view('templates/footer');
			// $this->load->view('cores/dashboard');
		} else {
			exit('Not Found!');
		}
	}

	public function pengajuan_launching_produk()
	{
		$q = $this->input->get('q');
		$start = $this->input->get('p');

		$totalPengajuanLaunching = $this->admin->totalPengajuanLaunching($q);
		$config['base_url'] = base_url("admin/pengajuan_launching_produk?q=$q");
		$config['total_rows'] = $totalPengajuanLaunching;
		$config['per_page'] = 10;
		$config['num_links'] = 2;
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'p';

		$config['full_tag_open']	= $this->full_tag_open;
		$config['full_tag_close']	= $this->full_tag_close;
		$config['first_link']		= $this->first_link;
		$config['first_tag_open']	= $this->first_tag_open;
		$config['first_tag_close']	= $this->first_tag_close;
		$config['last_link']		= $this->last_link;
		$config['last_tag_open']	= $this->last_tag_open;
		$config['last_tag_close']	= $this->last_tag_close;
		$config['next_link']		= $this->next_link;
		$config['next_tag_open']	= $this->next_tag_open;
		$config['next_tag_close']	= $this->next_tag_close;
		$config['prev_link']		= $this->prev_link;
		$config['prev_tag_open']	= $this->prev_tag_open;
		$config['prev_tag_close']	= $this->prev_tag_close;
		$config['cur_tag_open']		= $this->cur_tag_open;
		$config['cur_tag_close']	= $this->cur_tag_close;
		$config['num_tag_open']		= $this->num_tag_open;
		$config['num_tag_close']	= $this->num_tag_close;
		$config['attributes']		= $this->attributes;

		$this->pagination->initialize($config);

		$data['data_pengajuan'] = $this->admin->pengajuanLaunchingProduk($q, $start);
		$data['total_pencarian'] = $totalPengajuanLaunching;
		$this->load->view('templates/header', $data);
		$this->load->view('contents/pengajuan-launching-produk');
		$this->load->view('templates/footer');
		// $this->load->view('cores/dashboard');
	}

	public function komplain()
	{
		$q = $this->input->get('q');
		$start = $this->input->get('p');

		$totalPengajuanLaunching = $this->admin->totalKomplainProduk($q);
		$config['base_url'] = base_url("admin/komplain?q=$q");
		$config['total_rows'] = $totalPengajuanLaunching;
		$config['per_page'] = 10;
		$config['num_links'] = 2;
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'p';

		$config['full_tag_open']	= $this->full_tag_open;
		$config['full_tag_close']	= $this->full_tag_close;
		$config['first_link']		= $this->first_link;
		$config['first_tag_open']	= $this->first_tag_open;
		$config['first_tag_close']	= $this->first_tag_close;
		$config['last_link']		= $this->last_link;
		$config['last_tag_open']	= $this->last_tag_open;
		$config['last_tag_close']	= $this->last_tag_close;
		$config['next_link']		= $this->next_link;
		$config['next_tag_open']	= $this->next_tag_open;
		$config['next_tag_close']	= $this->next_tag_close;
		$config['prev_link']		= $this->prev_link;
		$config['prev_tag_open']	= $this->prev_tag_open;
		$config['prev_tag_close']	= $this->prev_tag_close;
		$config['cur_tag_open']		= $this->cur_tag_open;
		$config['cur_tag_close']	= $this->cur_tag_close;
		$config['num_tag_open']		= $this->num_tag_open;
		$config['num_tag_close']	= $this->num_tag_close;
		$config['attributes']		= $this->attributes;

		$this->pagination->initialize($config);

		$data['data_komplain'] = $this->admin->dataKomplainProduk($q, $start);
		$data['total_pencarian'] = $totalPengajuanLaunching;
		$this->load->view('templates/header', $data);
		$this->load->view('contents/komplain');
		$this->load->view('templates/footer');
		// $this->load->view('cores/dashboard');
	}

	public function pesanan_baru()
	{
		$q = $this->input->get('q');
		$start = $this->input->get('p');

		$totalPengajuanLaunching = $this->admin->totalPesananBaru($q);
		$config['base_url'] = base_url("admin/pesanan_baru?q=$q");
		$config['total_rows'] = $totalPengajuanLaunching;
		$config['per_page'] = 10;
		$config['num_links'] = 2;
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'p';

		$config['full_tag_open']	= $this->full_tag_open;
		$config['full_tag_close']	= $this->full_tag_close;
		$config['first_link']		= $this->first_link;
		$config['first_tag_open']	= $this->first_tag_open;
		$config['first_tag_close']	= $this->first_tag_close;
		$config['last_link']		= $this->last_link;
		$config['last_tag_open']	= $this->last_tag_open;
		$config['last_tag_close']	= $this->last_tag_close;
		$config['next_link']		= $this->next_link;
		$config['next_tag_open']	= $this->next_tag_open;
		$config['next_tag_close']	= $this->next_tag_close;
		$config['prev_link']		= $this->prev_link;
		$config['prev_tag_open']	= $this->prev_tag_open;
		$config['prev_tag_close']	= $this->prev_tag_close;
		$config['cur_tag_open']		= $this->cur_tag_open;
		$config['cur_tag_close']	= $this->cur_tag_close;
		$config['num_tag_open']		= $this->num_tag_open;
		$config['num_tag_close']	= $this->num_tag_close;
		$config['attributes']		= $this->attributes;

		$this->pagination->initialize($config);

		$data['data_pesanan'] = $this->admin->dataPesananBaru($q, $start);
		$data['total_pencarian'] = $totalPengajuanLaunching;
		$this->load->view('templates/header', $data);
		$this->load->view('contents/pesanan-baru');
		$this->load->view('templates/footer');
		// $this->load->view('cores/dashboard');
	}

	public function detail_pesanan_baru($id_pesanan = false)
	{

		$data_pesanan = $this->admin->detail_pesanan($id_pesanan);
		if ($data_pesanan) {
			$data['detail_pesanan'] = $data_pesanan;
			$data['gambar_produk'] = $this->admin->getGambarProdukWhere($data_pesanan['id_produk_post']);
			$data['bukti_pembayaran'] = $this->admin->getBuktiPembayaran($data_pesanan['id_pesanan']);
			// print_r($data['detail_pesanan']);
			// die();
			$this->load->view('templates/header', $data);
			$this->load->view('contents/detail-pesanan');
			$this->load->view('templates/footer');
			$this->load->view('cores/detail-pesanan');
		} else {
			exit('Not Found!');
		}
	}
}
