<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Api extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_api', 'api');
		// if (!userdata('id_admin')) {
		// 	redirect(base_url('auth/login'));
		// }

	}

	public function produkModal()
	{
		$id_produk = $this->input->get('id_produk');
		$data = $this->api->getProdukWhere($id_produk);
		$gambar_produk = $this->api->getGambarProdukWhere($id_produk);
		$output = [
			'data'	=> $data,
			'gambar_produk'	=> $gambar_produk
		];

		return $this->output($output);
	}

	public function approvePengajuanLaunching()
	{
		$id_produk = $this->input->post('id_produk');
		$return = $this->api->approvePengajuanLaunching($id_produk);
		$output['kode'] = $return;
		$this->output($output);
	}

	public function tolakPengajuanLaunching()
	{
		$id_produk = $this->input->post('id_produk');
		$return = $this->api->tolakPengajuanLaunching($id_produk);
		$output['kode'] = $return;
		$this->output($output);
	}

	public function kirimPesanan()
	{
		$id_pesanan = $this->input->post('id_pesanan');
		$id_toko = $this->input->post('id_toko');
		$harga_produk = $this->input->post('harga_produk');
		$id_produk = $this->input->post('id_produk');

		$cc = $this->api->check('tbl_produk_post',['id_produk_post'=>$id_produk]);
		$n = $cc[0]->terjual + 1;
        $this->api->update_all(['id_produk_post'=>$id_produk],['terjual'=>$n],'tbl_produk_post');
        $this->api->update_all(['id_pesanan'=>$id_pesanan],['tgl_diterima'=>date("Y-m-d H:i:s")],'tbl_pengguna_pesanan');

		$return = $this->api->kirimPesanan($id_pesanan);

		$dataToko = $this->api->dataToko($id_toko);
		$saldoToko = getNumber($dataToko['saldo_toko']) + getNumber($harga_produk);

		$this->api->kirimSaldoToko($id_toko, $saldoToko);

		$output['kode'] = $return;

		$this->output($output);
	}

	public function batalkanPesanan()
	{
		$id_pesanan = $this->input->post('id_pesanan');
		$return = $this->api->batalkanPesanan($id_pesanan);
		$output['kode'] = $return;
		$this->output($output);
	}

	private function output($output)
	{
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($output));
	}
}
