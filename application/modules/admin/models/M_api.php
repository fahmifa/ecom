<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_api extends CI_Model
{
    public function getProdukWhere($id_produk)
    {
        $this->db->select("tbl_produk_post.*, tbl_pengguna_toko.id_toko, tbl_pengguna_toko.nama_toko, tbl_pengguna_toko.gambar_toko, DATE_FORMAT(tbl_pengguna_toko.created_at, '%Y') AS registered_toko")
            ->from('tbl_produk_post')
            ->join('tbl_pengguna_toko', 'tbl_pengguna_toko.id_toko = tbl_produk_post.id_toko')
            ->where('tbl_produk_post.status_live', "verifikasi")
            ->where('tbl_produk_post.is_deleted', 0)
            ->where('tbl_produk_post.id_produk_post', $id_produk);

        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array()[0];
        } else {
            return false;
        }
    }

    public function getGambarProdukWhere($id_produk)
    {
        $this->db->select('id_produk_gambar, id_produk_post, gambar_produk')
            ->from('tbl_produk_gambar')
            ->where('id_produk_post', $id_produk)
            ->order_by('is_thumbnail', 'desc')
            ->order_by('created_at', 'asc');

        return $this->db->get()->result_array();
    }

    public function approvePengajuanLaunching($id_produk)
    {
        $set['status_live'] = 'live';
        $this->db->set($set);
        $this->db->where('id_produk_post', $id_produk);
        $this->db->update('tbl_produk_post');
        return $this->db->affected_rows();
    }

    public function tolakPengajuanLaunching($id_produk)
    {
        $set['status_live'] = 'ditolak';
        $this->db->set($set);
        $this->db->where('id_produk_post', $id_produk);
        $this->db->update('tbl_produk_post');
        return $this->db->affected_rows();
    }


    function check($table, $where)
    {
        $query = $this->db->get_where($table, $where);
        return $query->result();  
    }

        //update data
    public function update_all($where,$data,$table) 
    {
        $this->db->where($where);
        $this->db->update($table,$data);
    }

    public function kirimPesanan($id_pesanan)
    {
        $this->db->set('status_pesanan', 'pesanan_selesai');
        $this->db->where('id_pesanan', $id_pesanan);
        $this->db->update('tbl_pengguna_pesanan');

        return $this->db->affected_rows();
    }

    public function batalkanPesanan($id_pesanan)
    {
        $set = ['status_pesanan' => 'dibatalkan', 'dibatalkan_oleh' => 'admin'];
        $this->db->set($set);
        $this->db->where('id_pesanan', $id_pesanan);
        $this->db->update('tbl_pengguna_pesanan');

        return $this->db->affected_rows();
    }

    public function dataToko($id_toko)
    {
        return $this->db->get_where('tbl_pengguna_toko', ['id_toko' => $id_toko])->row_array();
    }

    public function kirimSaldoToko($id_toko, $saldoToko)
    {
        $set = ['saldo_toko' => $saldoToko];
        $this->db->set($set);
        $this->db->where('id_toko', $id_toko);
        $this->db->update('tbl_pengguna_toko');
    }
}
