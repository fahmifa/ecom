<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_admin extends CI_Model
{

    public function dashboardPengajuanLive()
    {
        $joinTblGambar = "(SELECT id_produk_post, gambar_produk FROM tbl_produk_gambar WHERE is_thumbnail = 1) tbl_produk_gambar";
        $this->db
            ->select('tbl_produk_post.*, tbl_pengguna_toko.nama_toko, tbl_produk_gambar.gambar_produk')
            ->from('tbl_produk_post')
            ->join($joinTblGambar, 'tbl_produk_gambar.id_produk_post = tbl_produk_post.id_produk_post', "LEFT")
            ->join('tbl_pengguna_toko', 'tbl_pengguna_toko.id_toko = tbl_produk_post.id_toko')
            ->where('tbl_produk_post.status_live', 'verifikasi')
            ->where('tbl_produk_post.is_deleted', 0)
            ->order_by('tbl_produk_post.created_at', 'DESC')
            ->limit(7);

        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function dashboardKomplainProduk()
    {
        $joinTblGambar = "(SELECT id_produk_post, gambar_produk FROM tbl_produk_gambar WHERE is_thumbnail = 1) tbl_produk_gambar";
        $this->db
            ->select('tbl_komplain_produk.*, tbl_pengguna.nama_pengguna, tbl_produk_post.nama_produk, tbl_produk_gambar.gambar_produk')
            ->from('tbl_komplain_produk')
            ->join('tbl_pengguna_pesanan', 'tbl_pengguna_pesanan.id_pesanan = tbl_komplain_produk.id_pesanan')
            ->join('tbl_pengguna', 'tbl_pengguna.id_pengguna = tbl_pengguna_pesanan.id_pengguna')
            ->join('tbl_produk_post', 'tbl_produk_post.id_produk_post = tbl_pengguna_pesanan.id_produk_post')
            ->join($joinTblGambar, 'tbl_produk_gambar.id_produk_post = tbl_produk_post.id_produk_post', "LEFT")
            ->order_by('tbl_komplain_produk.created_at', 'DESC')
            ->limit(7);

        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function dashboardPesananBaru()
    {
        $joinTblGambar = "(SELECT id_produk_post, gambar_produk FROM tbl_produk_gambar WHERE is_thumbnail = 1) tbl_produk_gambar";
        $this->db
            ->select("tbl_pengguna_pesanan.*, tbl_produk_gambar.gambar_produk, tbl_pengguna.nama_pengguna, tbl_produk_post.nama_produk")
            ->from('tbl_pengguna_pesanan')
            ->join('tbl_pengguna', 'tbl_pengguna.id_pengguna = tbl_pengguna_pesanan.id_pengguna')
            ->join('tbl_produk_post', 'tbl_produk_post.id_produk_post = tbl_pengguna_pesanan.id_produk_post')
            ->join($joinTblGambar, 'tbl_produk_gambar.id_produk_post = tbl_produk_post.id_produk_post', "LEFT")
            ->where_in('tbl_pengguna_pesanan.status_pesanan', ['menunggu_pembayaran', 'verifikasi_pembayaran'])
            ->order_by('tbl_pengguna_pesanan.created_at', 'DESC')
            ->limit(7);

        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function dashboardPengajuanDana()
    {
        $this->db
            ->select("tbl_pengajuan_dana.*, tbl_pengguna_toko.nama_toko")
            ->from('tbl_pengajuan_dana')
            ->join('tbl_pengguna_toko', 'tbl_pengguna_toko.id_toko = tbl_pengajuan_dana.id_toko')
            ->where('tbl_pengguna_toko.is_blocked', 0)
            ->order_by('tbl_pengajuan_dana.created_at', 'DESC');

        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function getTokoPenggunaWhere($id_toko)
    {
        $this->db
            ->select("tbl_pengguna_toko.*, tbl_pengguna.*")
            ->from('tbl_pengguna_toko')
            ->join('tbl_pengguna', 'tbl_pengguna.id_pengguna = tbl_pengguna_toko.id_pengguna')
            ->where('tbl_pengguna_toko.id_toko', $id_toko);

        return $this->db->get();
    }

    public function selectProdukWhere($id_toko)
    {
        $joinTblGambar = "(SELECT id_produk_post, gambar_produk FROM tbl_produk_gambar WHERE is_thumbnail = 1) tbl_produk_gambar";
        $this->db
            ->select("tbl_produk_post.*, tbl_produk_gambar.gambar_produk")
            ->from('tbl_produk_post')
            ->join($joinTblGambar, 'tbl_produk_gambar.id_produk_post = tbl_produk_post.id_produk_post', "LEFT")
            ->join('tbl_pengguna_toko', 'tbl_pengguna_toko.id_toko = tbl_produk_post.id_toko')
            ->where('tbl_produk_post.status_live', 'live')
            ->where('tbl_pengguna_toko.id_toko', $id_toko)
            ->order_by('tbl_produk_post.created_at', 'DESC')
            ->limit(5);

        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function getKomplainProdukWhere($id_komplain)
    {
        $joinTblGambar = "(SELECT id_produk_post, gambar_produk FROM tbl_produk_gambar WHERE is_thumbnail = 1) tbl_produk_gambar";
        $this->db
            ->select("tbl_komplain_produk.*, tbl_produk_post.*, tbl_produk_gambar.gambar_produk, tbl_pengguna_pesanan.*, tbl_pengguna.nama_pengguna")
            ->from('tbl_komplain_produk')
            ->join('tbl_pengguna_pesanan', 'tbl_pengguna_pesanan.id_pesanan = tbl_komplain_produk.id_pesanan')
            ->join('tbl_pengguna', 'tbl_pengguna.id_pengguna = tbl_pengguna_pesanan.id_pengguna')
            ->join('tbl_produk_post', 'tbl_produk_post.id_produk_post = tbl_pengguna_pesanan.id_produk_post')
            ->join($joinTblGambar, 'tbl_produk_gambar.id_produk_post = tbl_produk_post.id_produk_post', "LEFT")
            ->where('tbl_komplain_produk.id_komplain', $id_komplain);

        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->row_array();
        } else {
            return false;
        }
    }

    public function dataKonversasiKomplain($id_komplain)
    {
        $this->db
            ->select("tbl_konversasi_komplain.*, tbl_pengguna.nama_pengguna, tbl_pengguna.gambar_pengguna")
            ->from('tbl_konversasi_komplain')
            ->join('tbl_pengguna', 'tbl_pengguna.id_pengguna = tbl_konversasi_komplain.id_pengirim')
            ->where('tbl_konversasi_komplain.id_komplain', $id_komplain)
            ->order_by('tbl_konversasi_komplain.id', 'DESC');

        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function pengajuanLaunchingProduk($q = false, $start)
    {
        $joinTblGambar = "(SELECT id_produk_post, gambar_produk FROM tbl_produk_gambar WHERE is_thumbnail = 1) tbl_produk_gambar";
        $this->db
            ->select("tbl_produk_post.*, tbl_produk_gambar.gambar_produk, tbl_pengguna_toko.nama_toko")
            ->from('tbl_produk_post')
            ->join($joinTblGambar, 'tbl_produk_gambar.id_produk_post = tbl_produk_post.id_produk_post', "LEFT")
            ->join('tbl_pengguna_toko', 'tbl_pengguna_toko.id_toko = tbl_produk_post.id_toko')
            ->where('tbl_produk_post.status_live', 'verifikasi')
            ->where('tbl_produk_post.is_deleted', 0)
            ->where('tbl_pengguna_toko.is_blocked', 0)
            ->limit(10, $start);

        if ($q) {
            $this->db->like('tbl_pengguna_toko.nama_toko', $q);
            $this->db->or_like('tbl_produk_post.nama_produk', $q);
        }

        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function totalPengajuanLaunching($q = false)
    {
        $this->db
            ->from('tbl_produk_post')
            ->join('tbl_pengguna_toko', 'tbl_pengguna_toko.id_toko = tbl_produk_post.id_toko')
            ->where('tbl_produk_post.status_live', 'verifikasi')
            ->where('tbl_produk_post.is_deleted', 0)
            ->where('tbl_pengguna_toko.is_blocked', 0);

        if ($q) {
            $this->db->like('tbl_pengguna_toko.nama_toko', $q);
            $this->db->or_like('tbl_produk_post.nama_produk', $q);
        }

        return $this->db->count_all_results();
    }

    public function dataKomplainProduk($q = false, $start)
    {
        $joinTblGambar = "(SELECT id_produk_post, gambar_produk FROM tbl_produk_gambar WHERE is_thumbnail = 1) tbl_produk_gambar";
        $this->db
            ->select('tbl_komplain_produk.*, tbl_pengguna.nama_pengguna, tbl_produk_post.nama_produk, tbl_produk_gambar.gambar_produk')
            ->from('tbl_komplain_produk')
            ->join('tbl_pengguna_pesanan', 'tbl_pengguna_pesanan.id_pesanan = tbl_komplain_produk.id_pesanan')
            ->join('tbl_pengguna', 'tbl_pengguna.id_pengguna = tbl_pengguna_pesanan.id_pengguna')
            ->join('tbl_produk_post', 'tbl_produk_post.id_produk_post = tbl_pengguna_pesanan.id_produk_post')
            ->join($joinTblGambar, 'tbl_produk_gambar.id_produk_post = tbl_produk_post.id_produk_post', "LEFT");

        if ($q) {
            $this->db->like('tbl_komplain_produk.alasan', $q);
            $this->db->or_like('tbl_pengguna.nama_pengguna', $q);
            $this->db->or_like('tbl_komplain_produk.status_laporan', $q);
            $this->db->or_like('tbl_produk_post.nama_produk', $q);
        }

        $this->db->order_by('tbl_komplain_produk.created_at', 'DESC')
            ->limit(10, $start);

        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function totalKomplainProduk($q = false)
    {
        $this->db
            ->from('tbl_komplain_produk')
            ->join('tbl_pengguna_pesanan', 'tbl_pengguna_pesanan.id_pesanan = tbl_komplain_produk.id_pesanan')
            ->join('tbl_pengguna', 'tbl_pengguna.id_pengguna = tbl_pengguna_pesanan.id_pengguna')
            ->join('tbl_produk_post', 'tbl_produk_post.id_produk_post = tbl_pengguna_pesanan.id_produk_post');

        if ($q) {
            $this->db->like('tbl_komplain_produk.alasan', $q);
            $this->db->or_like('tbl_pengguna.nama_pengguna', $q);
            $this->db->or_like('tbl_komplain_produk.status_laporan', $q);
            $this->db->or_like('tbl_produk_post.nama_produk', $q);
        }

        return $this->db->count_all_results();
    }

    public function dataPesananBaru($q = false, $start)
    {
        $joinTblGambar = "(SELECT id_produk_post, gambar_produk FROM tbl_produk_gambar WHERE is_thumbnail = 1) tbl_produk_gambar";
        $this->db
            ->select("tbl_pengguna_pesanan.*, tbl_produk_gambar.gambar_produk, tbl_pengguna.nama_pengguna, tbl_produk_post.nama_produk")
            ->from('tbl_pengguna_pesanan')
            ->join('tbl_pengguna', 'tbl_pengguna.id_pengguna = tbl_pengguna_pesanan.id_pengguna')
            ->join('tbl_produk_post', 'tbl_produk_post.id_produk_post = tbl_pengguna_pesanan.id_produk_post')
            ->join($joinTblGambar, 'tbl_produk_gambar.id_produk_post = tbl_produk_post.id_produk_post', "LEFT")
            ->where_in('tbl_pengguna_pesanan.status_pesanan', ['menunggu_pembayaran', 'verifikasi_pembayaran']);

        if ($q) {
            $this->db->like('tbl_produk_post.nama_produk', $q);
            $this->db->or_like('tbl_pengguna.nama_pengguna', $q);
            $this->db->or_like('tbl_pengguna_pesanan.metode_pembayaran', str_replace(' ', '_', strtolower($q)));
            $this->db->or_like('tbl_pengguna_pesanan.status_pesanan', str_replace(' ', '_', strtolower($q)));
        }

        $this->db->order_by('tbl_pengguna_pesanan.created_at', 'DESC')
            ->limit(10, $start);

        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function totalPesananBaru($q = false)
    {
        $this->db
            ->from('tbl_pengguna_pesanan')
            ->join('tbl_pengguna', 'tbl_pengguna.id_pengguna = tbl_pengguna_pesanan.id_pengguna')
            ->join('tbl_produk_post', 'tbl_produk_post.id_produk_post = tbl_pengguna_pesanan.id_produk_post')
            ->where_in('tbl_pengguna_pesanan.status_pesanan', ['menunggu_pembayaran', 'verifikasi_pembayaran']);

        if ($q) {
            $this->db->like('tbl_produk_post.nama_produk', $q);
            $this->db->or_like('tbl_pengguna.nama_pengguna', $q);
            $this->db->or_like('tbl_pengguna_pesanan.metode_pembayaran', str_replace(' ', '_', strtolower($q)));
            $this->db->or_like('tbl_pengguna_pesanan.status_pesanan', str_replace(' ', '_', strtolower($q)));
        }

        return $this->db->count_all_results();
    }

    public function detail_pesanan($id_pesanan)
    {
        $joinTblGambar = "(SELECT id_produk_post, gambar_produk FROM tbl_produk_gambar WHERE is_thumbnail = 1) tbl_produk_gambar";
        $this->db
            ->select("tbl_pengguna_pesanan.*, tbl_pengguna_pesanan.created_at AS tgl_pesan, tbl_produk_gambar.*, tbl_pengguna.*, tbl_produk_post.*, tbl_pengguna_toko.*")
            ->from('tbl_pengguna_pesanan')
            ->join('tbl_pengguna', 'tbl_pengguna.id_pengguna = tbl_pengguna_pesanan.id_pengguna')
            ->join('tbl_produk_post', 'tbl_produk_post.id_produk_post = tbl_pengguna_pesanan.id_produk_post')
            ->join('tbl_pengguna_toko', 'tbl_pengguna_toko.id_toko = tbl_produk_post.id_toko')
            ->join($joinTblGambar, 'tbl_produk_gambar.id_produk_post = tbl_produk_post.id_produk_post', "LEFT")
            ->where('tbl_pengguna_pesanan.id_pesanan', $id_pesanan)
            ->where_in('tbl_pengguna_pesanan.status_pesanan', ['menunggu_pembayaran', 'verifikasi_pembayaran']);

        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->row_array();
        } else {
            return false;
        }
    }


    public function getGambarProdukWhere($id_produk)
    {
        $this->db->select('id_produk_gambar, id_produk_post, gambar_produk')
            ->from('tbl_produk_gambar')
            ->where('id_produk_post', $id_produk)
            ->order_by('is_thumbnail', 'desc')
            ->order_by('created_at', 'asc');

        return $this->db->get()->result_array();
    }

    public function getBuktiPembayaran($id_pesanan)
    {
        $result = $this->db->get_where('tbl_bukti_pembayaran', ['id_pesanan' => $id_pesanan]);

        if ($result->num_rows() > 0) {
            return $result->row_array();
        } else {
            return false;
        }
    }
}
