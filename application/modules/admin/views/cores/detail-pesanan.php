<script>
    $(document).ready(function() {

        let id_pesanan = `<?= $detail_pesanan['id_pesanan']; ?>`,
            id_toko = `<?= $detail_pesanan['id_toko']; ?>`,
            harga_produk = `<?= $detail_pesanan['harga_produk']; ?>`;

        $(document).on('click', 'button#btnKirimPesanan', function() {
            swal({
                title: 'Apakah anda yakin?',
                buttons: {
                    cancel: {
                        text: "Cancel",
                        value: null,
                        visible: true,
                        className: "btn btn-danger",
                        closeModal: true,
                    },
                    confirm: {
                        text: "OK",
                        value: true,
                        visible: true,
                        className: "btn btn-primary",
                        closeModal: true
                    }
                }
            }).then((res) => {
                if (res) {
                    kirimPesanan(id_pesanan)
                }
            });
        });

        function kirimPesanan() {
            loadingStart();
            var id_produk = $("#id_produk").val();
            $.ajax({
                method: "POST",
                url: api_endpoint + '/kirimPesanan',
                data: {
                    id_pesanan,
                    id_toko,
                    harga_produk,
                    id_produk
                },
            }).done((res) => {
                if (res.kode) {
                    window.location.replace(`<?= base_url('admin'); ?>`);
                } else {
                    swalErrorServer();
                }
            }).fail(
                swalErrorServer
            ).always(
                loadingStop
            );
        }

        $(document).on('click', 'button#btnBatalkanPesanan', function() {
            swal({
                title: 'Apakah anda yakin?',
                buttons: {
                    cancel: {
                        text: "Cancel",
                        value: null,
                        visible: true,
                        className: "btn btn-danger",
                        closeModal: true,
                    },
                    confirm: {
                        text: "OK",
                        value: true,
                        visible: true,
                        className: "btn btn-primary",
                        closeModal: true
                    }
                }
            }).then((res) => {
                if (res) {
                    batalkanPesanan()
                }
            });
        });

        function batalkanPesanan() {
            loadingStart();
            $.ajax({
                method: "POST",
                url: api_endpoint + '/batalkanPesanan',
                data: {
                    id_pesanan
                },
            }).done((res) => {
                if (res.kode) {
                    window.location.replace(`<?= base_url('admin'); ?>`);
                } else {
                    swalErrorServer();
                }
            }).fail(
                swalErrorServer
            ).always(
                loadingStop
            );
        }

        function swalErrorServer() {
            swal(
                'Oops, Sepertinya ada kesalahan pada server!',
                '',
                'error'
            );
        }

    });
</script>