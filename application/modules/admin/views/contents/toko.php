<!-- partial -->
<div class="main-panel">
    <div class="content-wrapper">

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="border-bottom text-center pb-4">
                                    <img src="<?= base_url('assets/pengguna/toko/profil/' . $toko['gambar_toko']); ?>" class="img-lg rounded-circle mb-3 thumbnail-cover" />
                                    <p><?= $toko['deskripsi_toko']; ?></p>
                                </div>
                                <div class="py-4">
                                    <p class="clearfix">
                                        <span class="float-left"> Nomor HP/WA </span>
                                        <span class="float-right text-muted"> <?= $toko['nomor_hp'] ?: '-'; ?> </span>
                                    </p>
                                    <p class="clearfix">
                                        <span class="float-left"> Email </span>
                                        <span class="float-right text-muted"> <?= $toko['email']; ?> </span>
                                    </p>
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <div class="d-flex justify-content-between">
                                    <div>
                                        <h3><?= $toko['nama_toko']; ?></h3>
                                        <div class="d-flex align-items-center">
                                            <h5 class="mb-0 mr-2 text-muted"><?= $toko['alamat_toko']; ?></h5>
                                        </div>
                                    </div>
                                    <div>
                                        <h6 class="m-0">Saldo Toko</h6>
                                        <div class="d-flex align-items-center">
                                            <h5 class="mb-0 mr-2 text-muted">
                                                <?= formatNumber($toko['saldo_toko'], "Rp"); ?>
                                            </h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-4 py-2 border-top border-bottom">
                                    <ul class="nav profile-navbar">
                                        <li class="nav-item">
                                            <a class="nav-link active" href="#">
                                                <i class="mdi mdi-newspaper"></i> Produk Baru </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="profile-feed">

                                    <?php if ($latest_produk) : ?>
                                        <?php foreach ($latest_produk as $result) : ?>
                                            <div class="d-flex align-items-start profile-feed-item">
                                                <img src="<?= base_url('assets/pengguna/toko/produk/' . $result['gambar_produk']); ?>" alt="profile" class="img-sm thumbnail-cover" />
                                                <div class="ml-4">
                                                    <h6> <?= $result['nama_produk']; ?> <small class="ml-4 text-muted"><i class="mdi mdi-clock mr-1"></i><?= date('d/m/Y H:i:s', strtotime($result['created_at'])); ?></small>
                                                    </h6>
                                                    <p> <?= $result['deskripsi_produk'] ? substr($result['deskripsi_produk'], 0, 150) . '...' : 'Tidak ada deskripsi.'; ?> </p>
                                                    <p class="small text-muted mt-2 mb-0">
                                                        <span>
                                                            <i class="mdi mdi-star mr-1"></i>4 </span>
                                                        <span class="ml-2">
                                                            <i class="mdi mdi-comment mr-1"></i>11 </span>
                                                        <span class="ml-2">
                                                            <i class="mdi mdi-reply"></i>
                                                        </span>
                                                    </p>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>

                                    <?php else : ?>
                                        <p class="text-center py-3">Kosong</p>
                                    <?php endif; ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- content-wrapper ends -->
    <!-- partial:partials/_footer.html -->
    <footer class="footer">
        <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2019 <a href="https://www.bootstrapdash.com/" target="_blank">BootstrapDash</a>. All rights reserved.</span>
            <span class="text-muted float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart text-danger"></i></span>
        </div>
    </footer>
    <!-- partial -->
</div>
<!-- main-panel ends -->

<!-- Modal -->
<div class="modal fade" id="modalPengajuanLaunching" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg w-75">
        <div class="modal-content">
            <div class="modal-body p-0 position-relative">
                <button type="button" class="position-absolute btn btn-outline-secondary" data-dismiss="modal" aria-label="Close" style="z-index: 1; right: 10px; top: 10px;">
                    <span class="" aria-hidden="true">×</span>
                </button>

                <div class="row no-gutters">
                    <div class="col-lg-6">
                        <img id="gambar_utama" class="thumbnail-cover w-100" style="min-height: 320px; max-height: 320px;">
                        <div class="row no-gutters" id="gambar_produk">
                        </div>
                    </div>
                    <div class="col-lg-6 p-4">
                        <div class="d-flex justify-content-center justify-content-md-start content-content-center content-content-md-start flex-column flex-md-row">
                            <img id="gambar_toko" src="" class="thumbnail-cover align-self-center align-self-md-start" style="height: 40px; width: 40px">

                            <div class="ml-0 ml-md-3 mt-3 mt-md-0 align-self-center align-self-md-start text-center text-md-left">
                                <h6 class="m-0">
                                    <a id="link_toko"><strong id="nama_toko">Nama Toko</strong></a>
                                </h6>
                                <p>Sejak <span id="registered_toko"></span></p>
                            </div>
                        </div>

                        <h5 class="mb-1" id="nama_produk">Nama Produk</h5>
                        <p id="harga_produk">Harga</p>

                        <p class="m-0" id="deskripsi_produk"></p>

                        <div class="mt-3">
                            <button class="btn btn-primary" id="btnApproveLaunching">Approve</button>
                            <button class="btn btn-danger" id="btnTolakLaunching">Tolak</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>