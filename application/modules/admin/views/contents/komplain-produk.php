<!-- partial -->
<div class="main-panel">
    <div class="content-wrapper">

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="border-bottom text-center pb-4">
                                    <img src="<?= base_url('assets/pengguna/toko/produk/' . $data_komplain['gambar_produk']); ?>" class="img-lg mb-3 thumbnail-cover" style="width: 160px;" />
                                    <p><?= $data_komplain['nama_produk']; ?></p>
                                </div>
                                <div class="py-4">
                                    <p class="clearfix">
                                        <span class="float-left"> Nama Pembeli </span>
                                        <span class="float-right text-muted"> <?= $data_komplain['nama_pengguna']; ?> </span>
                                    </p>
                                    <p class="clearfix">
                                        <span class="float-left"> Harga Pembelian </span>
                                        <span class="float-right text-muted"> <?= formatNumber($data_komplain['harga_produk'], "Rp"); ?> </span>
                                    </p>
                                    <p class="clearfix">
                                        <span class="float-left"> Metode Pembayaran </span>
                                        <span class="float-right text-muted"> <?= ucwords(str_replace('_', ' ', $data_komplain['metode_pembayaran'])); ?> </span>
                                    </p>
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <div class="d-flex justify-content-between">
                                    <div>
                                        <h3><?= $data_komplain['alasan']; ?></h3>
                                        <div class="d-flex align-items-center">
                                            <h5 class="mb-0 mr-2 text-muted">
                                                <?= $data_komplain['status_laporan']; ?>
                                                <?php if ($data_komplain['status_laporan'] === 'berlangsung') : ?>
                                                    <sup><i class="fas fa-circle text-success small"></i></sup>
                                                <?php else : ?>
                                                    <sup><i class="fas fa-circle text-danger small"></i></sup>
                                                <?php endif; ?>
                                            </h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-4 py-2 border-top">
                                    <ul class="nav profile-navbar">
                                        <li class="nav-item">
                                            <a class="nav-link active px-0" href="#">
                                                <i class="mdi mdi-comment-processing"></i> Komplain Pembeli </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="profile-feed overflow-auto vh-100">

                                    <?php if ($data_konversasi) : ?>
                                        <?php foreach ($data_konversasi as $result) : ?>
                                            <div class="d-flex align-items-start">
                                                <img src="<?= base_url('assets/pengguna/profil/' . $result['gambar_pengguna']); ?>" alt="profile" class="img-sm rounded-circle thumbnail-cover" />
                                                <div class="ml-4">
                                                    <h6> <?= $result['nama_pengguna']; ?> - <span class="font-weight-normal"><?= $result['pengirim']; ?></span> <small class="ml-4 text-muted"><i class="mdi mdi-clock mr-1"></i><?= date('d/m/Y H:i:s', strtotime($result['created_at'])); ?></small>
                                                    </h6>
                                                    <p> <?= $result['text']; ?> </p>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>

                                    <?php else : ?>
                                        <p class="text-center py-3">Kosong</p>
                                    <?php endif; ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- content-wrapper ends -->
    <!-- partial:partials/_footer.html -->
    <footer class="footer">
        <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2019 <a href="https://www.bootstrapdash.com/" target="_blank">BootstrapDash</a>. All rights reserved.</span>
            <span class="text-muted float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart text-danger"></i></span>
        </div>
    </footer>
    <!-- partial -->
</div>
<!-- main-panel ends -->