<!-- partial -->
<div class="main-panel">
    <div class="content-wrapper">

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="border-bottom text-center pb-4">
                                    <img id="gambar_utama" class="mb-3 thumbnail-cover w-100" style="min-height: 200px; max-height: 200px;" src="<?= base_url('assets/pengguna/toko/produk/' . $detail_pesanan['gambar_produk']); ?>" />
                                    <div class="row no-gutters">

                                        <?php if ($gambar_produk) : ?>
                                            <?php foreach ($gambar_produk as $result) : ?>
                                                <div class="col-3 border border-dark" id="img-selector">
                                                    <img src="<?= base_url('assets/pengguna/toko/produk/' . $result['gambar_produk']); ?>" class="thumbnail-cover w-100" style="min-height: 55px; max-height: 55px;" />
                                                </div>
                                            <?php endforeach; ?>
                                        <?php endif; ?>

                                    </div>
                                </div>
                                <div class="py-4">
                                    <input type="hidden" value="<?=$detail_pesanan['id_produk_post']?>" id="id_produk">
                                    <p class="clearfix">
                                        <span class="float-left"> Nama Pemesan </span>
                                        <span class="float-right text-muted"> <?= $detail_pesanan['nama_pengguna']; ?> </span>
                                    </p>
                                    <p class="clearfix">
                                        <span class="float-left"> Email Pemesan </span>
                                        <span class="float-right text-muted"> <?= $detail_pesanan['email']; ?> </span>
                                    </p>
                                    <p class="clearfix">
                                        <span class="float-left"> No HP/WA Pemesan </span>
                                        <span class="float-right text-muted"> <?= $detail_pesanan['nomor_hp'] ?: '-'; ?> </span>
                                    </p>
                                    <p class="clearfix">
                                        <span class="float-left"> Perusahaan </span>
                                        <span class="float-right text-muted"> <?= $detail_pesanan['nama_perusahaan'] ?: '-'; ?> </span>
                                    </p>
                                    <p class="clearfix">
                                        <span class="float-left"> Nama Produk </span>
                                        <span class="float-right text-muted"> <?= $detail_pesanan['nama_produk']; ?> </span>
                                    </p>
                                    <p class="clearfix">
                                        <span class="float-left"> Harga Pembelian </span>
                                        <span class="float-right text-muted"> <?= formatNumber($detail_pesanan['harga_produk'], "Rp"); ?> </span>
                                    </p>
                                    <p class="clearfix">
                                        <span class="float-left"> Total Harga </span>
                                        <span class="float-right text-muted"> <?= formatNumber($detail_pesanan['total_harga'], "Rp"); ?> </span>                                        
                                    </p>
                                    <p class="clearfix">
                                        <span class="float-left"> Tanggal Pemesanan </span>
                                        <span class="float-right text-muted"> <?= date('d/m/Y H:i:s', strtotime($detail_pesanan['tgl_pesan'])); ?> </span>
                                    </p>
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <div class="d-flex justify-content-between">
                                    <div>
                                        <h5>
                                            <?= ucwords(str_replace('_', ' ', $detail_pesanan['status_pesanan'])); ?>
                                            <?php if ($detail_pesanan['status_pesanan'] === 'menunggu_pembayaran') : ?>
                                                <sup><i class="fas fa-circle text-danger small"></i></sup>
                                            <?php else : ?>
                                                <sup><i class="fas fa-circle text-warning small"></i></sup>
                                            <?php endif; ?>
                                        </h5>
                                        <p class="mb-0 mr-2 text-muted"><?= ucwords(str_replace('_', ' ', $detail_pesanan['metode_pembayaran'])); ?></p>
                                    </div>
                                    <div>
                                        <?php if ($detail_pesanan['is_blocked'] === '1') : ?>
                                            <label class="badge badge-danger">Toko Diblokir!</label>
                                            <p class="mb-0">Pesanan tidak dapat diproses!</p>
                                        <?php endif; ?>

                                    </div>
                                </div>
                                <div class="mt-4 py-2 border-top border-bottom">
                                    <ul class="nav profile-navbar">
                                        <li class="nav-item">
                                            <a class="nav-link active" href="#">
                                                <i class="mdi mdi-credit-card-multiple"></i> Bukti Pembayaran </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="profile-feed">
                                    <?php if ($bukti_pembayaran) : ?>
                                        <div class="col-12 col-lg-6 px-0 py-3">
                                            <img class="img-fluid" src="<?= base_url('assets/pengguna/bukti_pembayaran/' . $bukti_pembayaran['bukti_gambar']); ?>">
                                        </div>
                                    <?php else : ?>
                                        <p class="text-center py-3">Pemesan belum memberikan bukti pembayaran.</p>
                                    <?php endif; ?>

                                    <button class="btn btn-danger my-1" id="btnBatalkanPesanan">Batalkan</button>
                                    <?php if ($detail_pesanan['is_blocked'] === '0') : ?>
                                        <button class="btn btn-primary my-1" id="btnKirimPesanan">Kirim Pesanan</button>
                                    <?php endif; ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- content-wrapper ends -->
    <!-- partial:partials/_footer.html -->
    <footer class="footer">
        <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2019 <a href="https://www.bootstrapdash.com/" target="_blank">BootstrapDash</a>. All rights reserved.</span>
            <span class="text-muted float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart text-danger"></i></span>
        </div>
    </footer>
    <!-- partial -->
</div>
<!-- main-panel ends -->