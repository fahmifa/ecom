<!-- partial -->
<div class="main-panel">
    <div class="content-wrapper">

        <div class="row">
            <div class="col-xl-3 col-sm-6 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-9">
                                <div class="d-flex align-items-center align-self-start">
                                    <h3 class="mb-0">$12.34</h3>
                                    <p class="text-success ml-2 mb-0 font-weight-medium">+3.5%</p>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="icon icon-box-success ">
                                    <span class="mdi mdi-arrow-top-right icon-item"></span>
                                </div>
                            </div>
                        </div>
                        <h6 class="text-muted font-weight-normal">Total Transaksi Hari Ini</h6>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-sm-6 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-9">
                                <div class="d-flex align-items-center align-self-start">
                                    <h3 class="mb-0">$17.34</h3>
                                    <p class="text-success ml-2 mb-0 font-weight-medium">+11%</p>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="icon icon-box-success">
                                    <span class="mdi mdi-arrow-top-right icon-item"></span>
                                </div>
                            </div>
                        </div>
                        <h6 class="text-muted font-weight-normal">Total Transaksi Bulan Ini</h6>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-sm-6 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-9">
                                <div class="d-flex align-items-center align-self-start">
                                    <h3 class="mb-0">$12.34</h3>
                                    <p class="text-danger ml-2 mb-0 font-weight-medium">-2.4%</p>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="icon icon-box-danger">
                                    <span class="mdi mdi-arrow-bottom-left icon-item"></span>
                                </div>
                            </div>
                        </div>
                        <h6 class="text-muted font-weight-normal">Total Transaksi Tahun Ini</h6>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-sm-6 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-9">
                                <div class="d-flex align-items-center align-self-start">
                                    <h3 class="mb-0">$31.53</h3>
                                    <p class="text-success ml-2 mb-0 font-weight-medium">+3.5%</p>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="icon icon-box-success ">
                                    <span class="mdi mdi-arrow-top-right icon-item"></span>
                                </div>
                            </div>
                        </div>
                        <h6 class="text-muted font-weight-normal">Total Keseluruhan</h6>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Pengajuan Launching Produk</h4>
                        </p>
                        <div class="table-responsive mb-2">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center">Produk</th>
                                        <th class="text-center">Nama Toko</th>
                                        <th class="text-center">Nama Produk</th>
                                        <th class="text-center">Harga</th>
                                        <th class="text-center">Tautan File</th>
                                        <th class="text-center">Tanggal</th>
                                        <th class="text-center">#</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php if ($pengajuan_live) : ?>
                                        <?php foreach ($pengajuan_live as $result) : ?>
                                            <tr>
                                                <td>
                                                    <img class="img-fluid" style="border-radius: 0;" src="<?= base_url('assets/pengguna/toko/produk/' . $result['gambar_produk']); ?>" alt="produk">
                                                </td>
                                                <td><?= $result['nama_toko']; ?></td>
                                                <td><?= $result['nama_produk']; ?></td>
                                                <td><?= formatNumber($result['harga_produk'], "Rp"); ?></td>
                                                <td>
                                                    <a href="<?= ($result['link_file']); ?>">Lihat</a>
                                                </td>
                                                <td><?= date('d/m/Y H:i:s', strtotime($result['created_at'])); ?></td>
                                                <td>
                                                    <button class="btn btn-info" id="openModalProduk" targetid="<?= $result['id_produk_post']; ?>">Lihat</button>
                                                    <button class="btn btn-danger" id="btnTolakLaunching" targetid="<?= $result['id_produk_post']; ?>">Tolak</button>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>

                                    <?php else : ?>
                                        <td class="text-center" colspan="7">Kosong</td>
                                    <?php endif; ?>

                                </tbody>
                            </table>
                        </div>

                        <a class="text-center" href="<?= base_url(''); ?>">
                            <p>Lihat Semua</p>
                        </a>

                    </div>
                </div>
            </div>
            <div class="col-lg-6 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Komplain Pembelian</h4>
                        </p>
                        <div class="table-responsive mb-2">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center"> Pembeli </th>
                                        <th class="text-center"> Produk </th>
                                        <th class="text-center"> Nama Produk </th>
                                        <th class="text-center"> Alasan </th>
                                        <th class="text-center"> Status Komplain </th>
                                        <th class="text-center"> Tanggal </th>
                                        <th class="text-center">#</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php if ($komplain_pembeli) : ?>
                                        <?php foreach ($komplain_pembeli as $result) : ?>
                                            <tr>
                                                <td><?= $result['nama_pengguna']; ?></td>
                                                <td>
                                                    <img class="img-fluid" style="border-radius: 0;" src="<?= base_url('assets/pengguna/toko/produk/' . $result['gambar_produk']); ?>" alt="produk">
                                                </td>
                                                <td><?= $result['nama_produk']; ?></td>
                                                <td><?= $result['alasan']; ?></td>
                                                <td><?= ucwords($result['status_laporan']); ?></td>
                                                <td><?= date('d/m/Y H:i:s', strtotime($result['created_at'])); ?></td>
                                                <td>
                                                    <a class="btn btn-info" href="<?= base_url('admin/komplain_produk/' . $result['id_komplain']); ?>">Lihat</a>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>

                                    <?php else : ?>
                                        <td colspan="7">Kosong.</td>
                                    <?php endif; ?>

                                </tbody>
                            </table>
                        </div>

                        <a class="text-center" href="<?= base_url('admin/komplain'); ?>">
                            <p>Lihat Semua</p>
                        </a>

                    </div>
                </div>
            </div>
            <div class="col-lg-6 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Pesanan Baru</h4>
                        </p>
                        <div class="table-responsive mb-2">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center"> Nama Pemesan </th>
                                        <th class="text-center"> Produk </th>
                                        <th class="text-center"> Nama Produk </th>
                                        <th class="text-center"> Harga Pembelian </th>
                                        <th class="text-center"> Total Harga </th>
                                        <th class="text-center"> Metode Pembayaran </th>
                                        <th class="text-center"> Status Pembayaran </th>
                                        <th class="text-center"> Tanggal </th>
                                        <th class="text-center"> # </th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php if ($pesanan_baru) : ?>
                                        <?php foreach ($pesanan_baru as $result) : ?>
                                            <tr>
                                                <td><?= $result['nama_pengguna']; ?></td>
                                                <td>
                                                    <img class="img-fluid" style="border-radius: 0;" src="<?= base_url('assets/pengguna/toko/produk/' . $result['gambar_produk']); ?>" alt="produk">
                                                </td>
                                                <td><?= $result['nama_produk']; ?></td>
                                                <td><?= formatNumber($result['harga_produk'], "Rp"); ?></td>
                                                <td><?= formatNumber($result['total_harga'], "Rp"); ?></td>
                                                <td><?= ucwords(str_replace('_', ' ', $result['metode_pembayaran'])); ?></td>
                                                <td><?= ucwords(str_replace('_', ' ', $result['status_pesanan'])); ?></td>
                                                <td><?= date('d/m/Y H:i:s', strtotime($result['created_at'])); ?></td>
                                                <td>
                                                    <a class="btn btn-info" href="<?= base_url('admin/detail_pesanan_baru/' . $result['id_pesanan']); ?>">Lihat</a>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>

                                    <?php else : ?>
                                        <td colspan="7">Kosong.</td>
                                    <?php endif; ?>

                                </tbody>
                            </table>
                        </div>

                        <a class="text-center" href="<?= base_url('admin/pesanan_baru'); ?>">
                            <p>Lihat Semua</p>
                        </a>

                    </div>
                </div>
            </div>
            <div class="col-lg-6 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Pengajuan Pencairan Dana</h4>
                        </p>
                        <div class="table-responsive mb-2">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center"> Nama Toko </th>
                                        <th class="text-center"> Nominal </th>
                                        <th class="text-center"> Status </th>
                                        <th class="text-center"> Tanggal </th>
                                        <th class="text-center"> # </th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php if ($pengajuan_dana) : ?>
                                        <?php foreach ($pengajuan_dana as $result) : ?>
                                            <tr>
                                                <td><?= $result['nama_toko']; ?></td>
                                                <td><?= formatNumber($result['nominal'], "Rp"); ?></td>
                                                <td><?= $result['status']; ?></td>
                                                <td><?= date('d/m/Y H:i:s', strtotime($result['created_at'])); ?></td>
                                                <td>
                                                    <button class="btn btn-info" id="openModalProduk" targetid="<?= $result['id_pengajuan']; ?>">Lihat</button>
                                                    <button class="btn btn-danger" id="btnTolakLaunching" targetid="<?= $result['id_pengajuan']; ?>">Tolak</button>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>

                                    <?php else : ?>
                                        <td colspan="7">Kosong.</td>
                                    <?php endif; ?>

                                </tbody>
                            </table>
                        </div>

                        <a class="text-center" href="<?= base_url(''); ?>">
                            <p>Lihat Semua</p>
                        </a>

                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- content-wrapper ends -->
    <!-- partial:partials/_footer.html -->
    <footer class="footer">
        <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2019 <a href="https://www.bootstrapdash.com/" target="_blank">BootstrapDash</a>. All rights reserved.</span>
            <span class="text-muted float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart text-danger"></i></span>
        </div>
    </footer>
    <!-- partial -->
</div>
<!-- main-panel ends -->