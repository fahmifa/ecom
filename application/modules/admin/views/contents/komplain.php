<!-- partial -->
<div class="main-panel min-vh-100">
    <div class="content-wrapper">

        <div class="card">
            <div class="card-body">
                <div class="d-flex justify-content-between flex-wrap">
                    <h4 class="card-title">Komplain Pembelian</h4>
                    <!-- <form action="" method="get"> -->
                    <form class="form-inline" method="get" action="">
                        <div class="form-group">
                            <label for="cari">Cari &nbsp;</label>
                            <input id="cari" class="form-control text-white" type="text" name="q" title="Cari nama pembeli, nama produk, alasan komplain, atau status komplain disini..." placeholder="Cari nama pembeli, nama produk, alasan komplain, atau status komplain disini..." value="<?= $this->input->get('q'); ?>">
                        </div>
                    </form>
                </div>

                </p>
                <div class="table-responsive mb-2">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th class="text-center"> Pembeli </th>
                                <th class="text-center"> Produk </th>
                                <th class="text-center"> Nama Produk </th>
                                <th class="text-center"> Alasan </th>
                                <th class="text-center"> Status Komplain </th>
                                <th class="text-center"> Tanggal </th>
                                <th class="text-center">#</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php if ($data_komplain) : ?>
                                <?php foreach ($data_komplain as $result) : ?>
                                    <tr>
                                        <td><?= $result['nama_pengguna']; ?></td>
                                        <td>
                                            <img class="img-fluid" style="border-radius: 0;" src="<?= base_url('assets/pengguna/toko/produk/' . $result['gambar_produk']); ?>" alt="produk">
                                        </td>
                                        <td><?= $result['nama_produk']; ?></td>
                                        <td><?= $result['alasan']; ?></td>
                                        <td><?= ucwords($result['status_laporan']); ?></td>
                                        <td><?= date('d/m/Y H:i:s', strtotime($result['created_at'])); ?></td>
                                        <td class="text-center">
                                            <a class="btn btn-info" href="<?= base_url('admin/komplain_produk/' . $result['id_komplain']); ?>">Lihat</a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>

                            <?php else : ?>
                                <td colspan="7">Kosong.</td>
                            <?php endif; ?>

                        </tbody>
                    </table>
                </div>

                <div class="d-flex justify-content-between">
                    <p class="m-0"> <?= $this->input->get('q') ? "$total_pencarian ditemukan dari pencarian : " . $this->input->get('q') : ''; ?> </p>
                    <?= $this->pagination->create_links(); ?>
                </div>
            </div>
        </div>


    </div>
    <!-- content-wrapper ends -->
    <!-- partial:partials/_footer.html -->
    <footer class="footer">
        <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2019 <a href="https://www.bootstrapdash.com/" target="_blank">BootstrapDash</a>. All rights reserved.</span>
            <span class="text-muted float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart text-danger"></i></span>
        </div>
    </footer>
    <!-- partial -->
</div>
<!-- main-panel ends -->