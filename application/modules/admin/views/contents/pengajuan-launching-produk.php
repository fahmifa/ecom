<!-- partial -->
<div class="main-panel min-vh-100">
    <div class="content-wrapper">

        <div class="card">
            <div class="card-body">
                <div class="d-flex justify-content-between flex-wrap">
                    <h4 class="card-title">Pengajuan Launching Produk</h4>
                    <!-- <form action="" method="get"> -->
                    <form class="form-inline" method="get" action="">
                        <div class="form-group">
                            <label for="cari">Cari &nbsp;</label>
                            <input id="cari" class="form-control text-white" type="text" name="q" title="Cari nama produk atau toko disini..." placeholder="Cari nama produk atau toko disini..." value="<?= $this->input->get('q'); ?>">
                        </div>
                    </form>
                </div>

                </p>
                <div class="table-responsive mb-2">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th class="text-center">Produk</th>
                                <th class="text-center">Nama Toko</th>
                                <th class="text-center">Nama Produk</th>
                                <th class="text-center">Harga</th>
                                <th class="text-center">Tautan File</th>
                                <th class="text-center">Tanggal</th>
                                <th class="text-center">#</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php if ($data_pengajuan) : ?>
                                <?php foreach ($data_pengajuan as $result) : ?>
                                    <tr>
                                        <td class="text-center">
                                            <img class="img-fluid" style="border-radius: 0;" src="<?= base_url('assets/pengguna/toko/produk/' . $result['gambar_produk']); ?>" alt="produk">
                                        </td>
                                        <td><?= $result['nama_toko']; ?></td>
                                        <td><?= $result['nama_produk']; ?></td>
                                        <td><?= formatNumber($result['harga_produk'], "Rp"); ?></td>
                                        <td>
                                            <a href="<?= ($result['link_file']); ?>">Lihat</a>
                                        </td>
                                        <td><?= date('d/m/Y H:i:s', strtotime($result['created_at'])); ?></td>
                                        <td class="text-center">
                                            <button class="btn btn-info" id="openModalProduk" targetid="<?= $result['id_produk_post']; ?>">Lihat</button>
                                            <button class="btn btn-danger" id="btnTolakLaunching" targetid="<?= $result['id_produk_post']; ?>">Tolak</button>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>

                            <?php else : ?>
                                <td class="text-center" colspan="7">Kosong</td>
                            <?php endif; ?>

                        </tbody>
                    </table>
                </div>

                <div class="d-flex justify-content-between">
                    <p class="m-0"> <?= $this->input->get('q') ? "$total_pencarian ditemukan dari pencarian : " . $this->input->get('q') : ''; ?> </p>
                    <?= $this->pagination->create_links(); ?>
                </div>
            </div>
        </div>


    </div>
    <!-- content-wrapper ends -->
    <!-- partial:partials/_footer.html -->
    <footer class="footer">
        <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2019 <a href="https://www.bootstrapdash.com/" target="_blank">BootstrapDash</a>. All rights reserved.</span>
            <span class="text-muted float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart text-danger"></i></span>
        </div>
    </footer>
    <!-- partial -->
</div>
<!-- main-panel ends -->