</div>
<!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->

<!-- Global modal -->
<div class="modal fade" id="modalUbahPassword" tabindex="-1" aria-labelledby="modalUbahPassword" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="titleModal">Change Password</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="formubahpassword">
                    <div class="form-group">
                        <label for="oldpassword">Old Password</label>
                        <input id="oldpassword" class="form-control text-white" type="password" name="oldpassword">
                        <small id="oldpassworderror" class="text-danger"></small>
                    </div>
                    <div class="form-group">
                        <label for="newpassword">New Password</label>
                        <input id="newpassword" class="form-control text-white" type="password" name="newpassword">
                        <small id="newpassworderror" class="text-danger"></small>
                    </div>
                    <div class="form-group">
                        <label for="newpassword2">Retype New Password</label>
                        <input id="newpassword2" class="form-control text-white" type="password" name="newpassword2">
                        <small id="newpassword2error" class="text-danger"></small>
                    </div>

                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="eye">
                        <label class="custom-control-label" for="eye">Show Password</label>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submitubahpassword">Save</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalPengajuanLaunching" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg w-75">
        <div class="modal-content">
            <div class="modal-body p-0 position-relative">
                <button type="button" class="position-absolute btn btn-outline-secondary" data-dismiss="modal" aria-label="Close" style="z-index: 1; right: 10px; top: 10px;">
                    <span class="" aria-hidden="true">×</span>
                </button>

                <div class="row no-gutters">
                    <div class="col-lg-6">
                        <img id="gambar_utama" class="thumbnail-cover w-100" style="min-height: 320px; max-height: 320px;">
                        <div class="row no-gutters" id="gambar_produk">
                        </div>
                    </div>
                    <div class="col-lg-6 p-4">
                        <div class="d-flex justify-content-center justify-content-md-start content-content-center content-content-md-start flex-column flex-md-row">
                            <img id="gambar_toko" src="" class="thumbnail-cover align-self-center align-self-md-start" style="height: 40px; width: 40px">

                            <div class="ml-0 ml-md-3 mt-3 mt-md-0 align-self-center align-self-md-start text-center text-md-left">
                                <h6 class="m-0">
                                    <a id="link_toko"><strong id="nama_toko">Nama Toko</strong></a>
                                </h6>
                                <p>Sejak <span id="registered_toko"></span></p>
                            </div>
                        </div>

                        <h5 class="mb-1" id="nama_produk">Nama Produk</h5>
                        <p id="harga_produk">Harga</p>

                        <p class="m-0" id="deskripsi_produk"></p>

                        <div class="mt-3">
                            <button class="btn btn-primary" id="btnApproveLaunching">Approve</button>
                            <button class="btn btn-danger" id="btnTolakLaunching">Tolak</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- plugins:js -->
<script src="<?= base_url('assets/assets-admin/js/vendor.bundle.base.js'); ?>"></script>
<!-- endinject -->
<!-- Plugin js for this page -->
<script src="//cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<!-- End plugin js for this page -->
<!-- inject:js -->
<script src="<?= base_url('assets/assets-admin/js/off-canvas.js'); ?>"></script>
<script src="<?= base_url('assets/assets-admin/js/hoverable-collapse.js'); ?>"></script>
<script src="<?= base_url('assets/assets-admin/js/misc.js'); ?>"></script>
<script src="<?= base_url('assets/assets-admin/js/settings.js'); ?>"></script>
<script src="<?= base_url('assets/assets-admin/js/todolist.js'); ?>"></script>
<!-- endinject -->
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script> -->
<script src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easy-loading/1.3.0/jquery.loading.js"></script>
<script src="<?= base_url('assets/assets-admin/js/sweetalert.js'); ?>"></script>
<script src="<?= base_url('assets/js/jquery-rate-picker.js'); ?>"></script>
<script src="<?= base_url('assets/js/global.js'); ?>"></script>
<!-- End custom js for this page -->
<script>
    let baseUrl = `<?= base_url(); ?>`,
        api_endpoint = baseUrl + 'admin/api',
        pathAssets = baseUrl + 'assets',
        pathGambarProduk = pathAssets + '/pengguna/toko/produk',
        pathGambarToko = pathAssets + '/pengguna/toko/profil',
        pathGambarUser = pathAssets + '/pengguna/profil';

    $(document).ready(function() {

        $(document).on('click', 'button#openModalProduk', function() {
            let id_produk = $(this).attr('targetid');
            getProduk(id_produk);
        });

        function getProduk(id_produk) {
            loadingStart();
            $.ajax({
                url: `${ api_endpoint }/produkModal`,
                method: "GET",
                data: {
                    id_produk
                }
            }).done((res) => {
                if (res.data) {
                    let modalParent = $('div#modalPengajuanLaunching');
                    let gambar_produk = '';
                    let gambar_utama = res.gambar_produk.length > 0 ? res.gambar_produk[0].gambar_produk : null;

                    res.gambar_produk.forEach((data) => {
                        gambar_produk +=
                            `<div class="col-3 border border-dark" id="img-selector">
                            <img src="<?= base_url('assets/pengguna/toko/produk/'); ?>${data.gambar_produk}" class="thumbnail-cover w-100" style="min-height: 70px; max-height: 70px;">
                            </div>`;
                    });

                    modalParent.find('img#gambar_utama').attr('src', pathGambarProduk + '/' + gambar_utama);
                    modalParent.find('#gambar_produk').html(gambar_produk);
                    modalParent.find('#gambar_toko').attr('src', pathGambarToko + '/' + res.data.gambar_toko);
                    modalParent.find('#link_toko').attr('href', baseUrl + 'admin/toko/' + res.data.id_toko);
                    modalParent.find('#nama_toko').text(res.data.nama_toko);
                    modalParent.find('#registered_toko').text(res.data.registered_toko);
                    modalParent.find('#nama_produk').text(res.data.nama_produk);
                    modalParent.find('#harga_produk').text("Rp" + formatNumber(res.data.harga_produk));
                    modalParent.find('#deskripsi_produk').text(res.data.deskripsi_produk);
                    modalParent.find('#btnApproveLaunching').attr('targetid', res.data.id_produk_post);
                    modalParent.find('#btnTolakLaunching').attr('targetid', res.data.id_produk_post);
                    modalParent.modal('show');
                }
            }).fail((res) => {
                // console.log(res);
            }).always(() => {
                loadingStop();
            });
        }

        $(document).on('click', '#img-selector', function() {
            let selector = $(this).children().attr('src');
            let gambar_utama = $('#gambar_utama');
            gambar_utama.attr('src', selector);
        });

        $(document).on('click', 'button#btnApproveLaunching', function() {
            let id_produk = $(this).attr('targetid')

            swal({
                title: 'Apakah anda yakin?',
                buttons: {
                    cancel: {
                        text: "Cancel",
                        value: null,
                        visible: true,
                        className: "btn btn-danger",
                        closeModal: true,
                    },
                    confirm: {
                        text: "OK",
                        value: true,
                        visible: true,
                        className: "btn btn-primary",
                        closeModal: true
                    }
                }
            }).then((res) => {
                if (res) {
                    approvePengajuanLaunching(id_produk)
                }
            });
        });

        function approvePengajuanLaunching(id_produk) {
            loadingStart();
            $.ajax({
                url: api_endpoint + '/approvePengajuanLaunching',
                method: "POST",
                data: {
                    id_produk
                },
            }).done((res) => {
                if (res.kode) {
                    location.reload();
                }
            }).fail(
                swalErrorServer
            ).always(
                loadingStop
            );
        }

        $(document).on('click', 'button#btnTolakLaunching', function() {
            let id_produk = $(this).attr('targetid')

            swal({
                title: 'Apakah anda yakin?',
                buttons: {
                    cancel: {
                        text: "Cancel",
                        value: null,
                        visible: true,
                        className: "btn btn-danger",
                        closeModal: true,
                    },
                    confirm: {
                        text: "OK",
                        value: true,
                        visible: true,
                        className: "btn btn-primary",
                        closeModal: true
                    }
                }
            }).then((res) => {
                if (res) {
                    tolakPengajuanLaunching(id_produk)
                }
            });
        });

        function tolakPengajuanLaunching(id_produk) {
            loadingStart();
            $.ajax({
                url: api_endpoint + '/tolakPengajuanLaunching',
                method: "POST",
                data: {
                    id_produk
                },
            }).done((res) => {
                if (res.kode) {
                    location.reload();
                }
            }).fail(
                swalErrorServer
            ).always(
                loadingStop
            );
        }

        function swalErrorServer() {
            swal(
                'Oops, Sepertinya ada kesalahan pada server!',
                '',
                'error'
            );
        }

        $('button#submitubahpassword').click(function() {
            let data = $('div#modalUbahPassword').find('form').serialize();
            $('small#oldpassworderror').html('');
            $('small#newpassworderror').html('');
            $('small#newpassword2error').html('');

            ubahpassword(data, $(this));
        });

        function ubahpassword(data, elm) {
            elm.prop('disabled', true);
            elm.html(`<i class="fas fa-spin fa-spinner m-0"></i>`);

            $.ajax({
                url: `<?= base_url('api/apiubahpassword'); ?>`,
                method: "POST",
                data: data
            }).done((res) => {
                let oldpassworderror = $('small#oldpassworderror');
                let newpassworderror = $('small#newpassworderror');
                let newpassword2error = $('small#newpassword2error');

                if (res.kode === '0') {
                    oldpassworderror.html(res.oldpassworderror);
                    newpassworderror.html(res.newpassworderror);
                    newpassword2error.html(res.newpassword2error);
                } else if (res.kode === '00') {
                    oldpassworderror.html(res.oldpassworderror);
                } else if (res.kode === '000') {
                    newpassworderror.html(res.newpassworderror);
                } else if (res.kode === '0000') {
                    swal({
                        icon: 'error',
                        title: 'INFO!',
                        text: 'Oops, server error!',
                    });
                } else if (res.kode === '1') {
                    $('div#modalUbahPassword').modal('hide');
                    oldpassworderror.html('');
                    newpassworderror.html('');
                    newpassword2error.html('');
                    oldpassworderror.prev().val('');
                    newpassworderror.prev().val('');
                    newpassword2error.prev().val('');
                    swal({
                        icon: 'success',
                        title: 'INFO!',
                        text: 'Change password successfuly!',
                    });
                }
            }).fail((res) => {
                $('div#modalUbahPassword').modal('hide');
                swal({
                    icon: 'error',
                    title: 'INFO!',
                    text: 'Oops, server error!',
                });
            }).always(() => {
                elm.prop('disabled', false);
                elm.html('Save');
            });
        }

        $('div#modalUbahPassword').on('hidden.bs.modal', function() {
            let oldpassworderror = $('small#oldpassworderror');
            let newpassworderror = $('small#newpassworderror');
            let newpassword2error = $('small#newpassword2error');
            oldpassworderror.html('');
            newpassworderror.html('');
            newpassword2error.html('');
            oldpassworderror.prev().val('');
            newpassworderror.prev().val('');
            newpassword2error.prev().val('');
        });

        $('input#eye').click(function() {
            let oldpassworderror = $('input#oldpassword');
            let newpassworderror = $('input#newpassword');
            let newpassword2error = $('input#newpassword2');

            if ($(this).is(':checked')) {
                oldpassworderror.attr('type', 'text');
                newpassworderror.attr('type', 'text');
                newpassword2error.attr('type', 'text');
            } else {
                oldpassworderror.attr('type', 'password');
                newpassworderror.attr('type', 'password');
                newpassword2error.attr('type', 'password');
            }
        });
    });
</script>