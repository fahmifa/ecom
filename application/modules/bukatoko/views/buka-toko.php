<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Buka Toko</title>
    <link rel="icon" href="https://getbootstrap.com/docs/4.5/assets/img/favicons/favicon-32x32.png" type="image/png">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url('assets/assets-login/css/owl.carousel.min.css'); ?>">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= base_url('assets/assets-login/css/bootstrap.min.css'); ?>">
    <!-- Style -->
    <!-- <link rel="stylesheet" href="<?= base_url('assets/assets-login/css/style.css'); ?>"> -->
</head>

<body>

    <div class="p-0 d-flex align-items-center" style="min-height: 100vh;">
        <div class="container d-flex justify-content-center">
            <div class="col-12 col-md-6">
                <?= $this->session->flashdata('msg'); ?>
                <div class="card">
                    <div class="card-body py-4">
                        <form action="" method="post">
                            <div class="form-group">
                                <label for="namatoko">Nama Toko</label>
                                <input type="text" class="form-control" id="namatoko" name="namatoko" required placeholder="Nama Toko" value="<?= set_value('namatoko'); ?>">
                                <?= form_error('namatoko'); ?>
                            </div>
                            <div class="form-group">
                                <label for="alamattoko">Alamat Toko</label>
                                <textarea type="text" class="form-control" id="alamattoko" name="alamattoko" required placeholder="Alamat Toko"><?= set_value('alamattoko'); ?></textarea>
                                <?= form_error('alamattoko'); ?>
                            </div>
                            <div class="form-group">
                                <label for="deskripsitoko">Deskripsi Toko</label>
                                <textarea type="text" class="form-control" id="deskripsitoko" name="deskripsitoko" placeholder="Deskripsi Toko"><?= set_value('deskripsitoko'); ?></textarea>
                                <?= form_error('deskripsitoko'); ?>
                            </div>

                            <button type="submit" class="btn btn-success w-100 mt-3" style="background: #38D39F; height: 45px;">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script src="<?= base_url('assets/assets-login/js/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?= base_url('assets/assets-login/js/popper.min.js'); ?>"></script>
    <script src="<?= base_url('assets/assets-login/js/bootstrap.min.js'); ?>"></script>
    <script src="<?= base_url('assets/assets-login/js/main.js'); ?>"></script>
</body>

</html>