<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bukatoko extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_bukatoko', 'bukatoko');

		if (!$this->session->email) {
			redirect(base_url('auth/login'));
		} else {
			$getTokoWhereId = $this->bukatoko->getTokoWhereId(userdata('id_pengguna'));

			if ($getTokoWhereId->num_rows() > 0) {
				redirect(base_url('tokoku'));
			}
		}
	}

	// view
	public function index()
	{
		$rules = [
			'required'		=> 'Mohon masukkan Nama Toko terlebih dahulu!',
			'max_length'	=> 'Oops Nama Toko terlalu panjang! Panjang maximum 64 karakter!',
			'is_unique'		=> 'Maaf Nama Toko sudah digunakan!'
		];
		$this->form_validation->set_rules('namatoko', 'namaToko', 'trim|required|max_length[64]', $rules);
		$this->form_validation->set_rules('alamattoko', 'alamatToko', 'trim|required', ['required' => 'Mohon masukkan Alamat Toko terlebih dahulu!']);
		$this->form_validation->set_rules('deskripsitoko', 'deskripsiToko', 'trim');
		$this->form_validation->set_error_delimiters('<small class="text-danger">', '</small>');

		if ($this->form_validation->run() === FALSE) {
			$this->load->view('buka-toko');
		} else {
			$namatoko		= $this->input->post('namatoko', true);
			$alamattoko		= $this->input->post('alamattoko', true);
			$deskripsitoko	= $this->input->post('deskripsitoko', true);
			$this->_bukatoko($namatoko, $alamattoko, $deskripsitoko);
		}
	}
	// view

	// function
	private function _bukatoko($namatoko, $alamattoko, $deskripsitoko)
	{
		$data = [
			'id_toko'			=> rand(10000, 99999) . rand(10000, 99999),
			'id_pengguna'		=> userdata('id_pengguna'),
			'nama_toko'			=> $namatoko,
			'alamat_toko'		=> $alamattoko,
			'deskripsi_toko'	=> $deskripsitoko,
		];
		$return = $this->bukatoko->postToko($data);

		if ($return) {
			redirect(base_url('tokoku'));
		} else {
			$this->session->set_flashdata('msg', $this->bootstrapalert('Oops! Sepertinya ada kesalahan server.', 'alert-warning'));
			redirect(base_url('bukatoko'));
		}
	}
	// function

	// helper
	private function bootstrapalert($msg, $class)
	{
		$template = "<div class='alert $class' role='alert'>$msg</div>";

		return $template;
	}
	// helper
}
