<?php

defined('BASEPATH') or exit('No direct script access allowed');

class M_bukatoko extends CI_Model
{
    public function getTokoWhereId($idUser)
    {
        return $this->db->get_where('tbl_pengguna_toko', ['id_pengguna' => $idUser]);
    }

    public function postToko($data)
    {
        return $this->db->insert('tbl_pengguna_toko', $data);
    }
}
