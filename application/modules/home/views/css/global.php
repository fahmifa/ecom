<style>
    .modal-produk-thumbnail {
        max-height: 400px;
        max-width: 100%;
        min-height: 400px;
        min-width: 100%;
        object-fit: cover;
        object-position: center;
        box-shadow: 0 .5rem 1rem rgba(0, 0, 0, .15) !important;
    }

    .produk-selector {
        width: 100%;
        max-height: 80px;
        object-fit: cover;
        object-position: center;
    }

    .swal2-styled.swal2-confirm {
        font-size: inherit;
    }

    .page-item.active .page-link {
        background-color: #29BB8A !important;
        border-color: #29BB8A !important;
    }

    .list-group-item.active {
        background-color: #29BB8A !important;
        border-color: #29BB8A !important;
    }
</style>