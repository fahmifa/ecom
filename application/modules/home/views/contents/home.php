<!-- Slider Area -->
<section class="hero-slider" style="height: auto;">
    <!-- Single Slider -->
    <div class="single-slider" style="height: inherit;">
        <div class="container py-3">
            <div class="row">

                <div class="col-12 col-md-6 align-self-center">
                    <div class="hero-text px-0 m-0">
                        <h1><span>UP TO 50% OFF </span>Lorem Ipsum!</h1>
                        <p>Maboriosam in a nesciung eget magnae <br> dapibus disting tloctio in the find it pereri <br> odiy maboriosm.</p>
                    </div>

                    <form method="GET" action="<?= base_url('home/search'); ?>">
                        <div class="input-group mb-3">
                            <input type="text" name="q" class="form-control px-3 border-right-0" placeholder="Icon pack for web development...">
                            <div class="input-group-append">
                                <button class="btn" type="submit">Cari</button>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="col-12 col-md-6 align-self-center text-center">
                    <img class="img-fluid" src="<?= base_url('assets/pengguna/toko/produk/buku.png'); ?>" alt="product">
                </div>
            </div>
        </div>
    </div>
    <!--/ End Single Slider -->
</section>
<!--/ End Slider Area -->

<!-- Start Small Banner  -->
<!-- <section class="small-banner section">
    <div class="container-fluid">
        <div class="row">
            
            <div class="col-lg-4 col-md-6 col-12">
                <div class="single-banner">
                    <img src="http://via.placeholder.com/600x370" alt="#">
                    <div class="content">
                        <p>Man's Collectons</p>
                        <h3>Summer travel <br> collection</h3>
                        <a href="#">Discover Now</a>
                    </div>
                </div>
            </div>
      
            <div class="col-lg-4 col-md-6 col-12">
                <div class="single-banner">
                    <img src="http://via.placeholder.com/600x370" alt="#">
                    <div class="content">
                        <p>Bag Collectons</p>
                        <h3>Awesome Bag <br> 2020</h3>
                        <a href="#">Shop Now</a>
                    </div>
                </div>
            </div>
         
            <div class="col-lg-4 col-12">
                <div class="single-banner tab-height">
                    <img src="http://via.placeholder.com/600x370" alt="#">
                    <div class="content">
                        <p>Flash Sale</p>
                        <h3>Mid Season <br> Up to <span>40%</span> Off</h3>
                        <a href="#">Discover Now</a>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</section> -->
<!-- End Small Banner -->

<!-- Start Product Area -->
<div class="product-area section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-title">
                    <h2>Produk</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="product-info">
                    <div class="nav-main">
                        <!-- Tab Nav -->
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <?php for ($i = 0; $i < count($kategori); $i++) { ?>
                                <li class="nav-item"><a class="nav-link <?= ($i == 0) ? 'active' : null ?>" data-toggle="tab" href="#<?= $kategori[$i]->id_kategori ?>" role="tab"><?= $kategori[$i]->nama_kategori ?></a></li>
                            <?php } ?>
                        </ul>
                        <!--/ End Tab Nav -->
                    </div>
                    <div class="tab-content" id="myTabContent">
                        <!-- Start Single Tab -->
                        <?php for ($i = 0; $i < count($produk); $i++) {
                            if (!empty($produk[$i])) {
                        ?>
                                <div class="tab-pane fade <?= ($i == 0) ? 'show active' : null ?>" id="<?= $produk[$i][0]->id_kategori ?>" role="tabpanel">
                                    <div class="tab-single">
                                        <div class="row">
                                            <?php for ($x = 0; $x < count($produk[$i]); $x++) { ?>
                                                <div class="col-xl-3 col-lg-4 col-md-4 col-12">
                                                    <div class="single-product">
                                                        <div class="product-img">
                                                            <?php
                                                            $gam = gambar($produk[$i][$x]->id_produk_post);
                                                            if ($gam) {
                                                                $img = $gam[0]->gambar_produk;
                                                            } else {
                                                                $img = 'sample.jpg';
                                                            }
                                                            ?>
                                                            <a target="_blank" href="<?= base_url('produk/' . url_title($produk[$i][$x]->id_produk_post)) ?>">
                                                                <img class="img-home" src="<?= base_url('assets/pengguna/toko/produk/' . $img) ?>" alt="#">
                                                            </a>


                                                            <div class="button-head">
                                                                <div class="product-action">

                                                                    <a title="Lihat Produk" href="#" id="openModalProduk" data-toggle="modal" targetid="<?= $produk[$i][$x]->id_produk_post; ?>">
                                                                        <i class="ti-eye"></i>
                                                                    </a>

                                                                    <?php if (in_array($produk[$i][$x]->id_produk_post, $produk_favorit)) : ?>
                                                                        <a title="Hapus Favorit" href="#" data-toggle="modal" id="hapusFavorit" targetid="<?= $produk[$i][$x]->id_produk_post; ?>">
                                                                            <i class="fa fa-heart text-success"></i>
                                                                        </a>
                                                                    <?php else : ?>
                                                                        <a title="Favoritkan" href="#" data-toggle="modal" id="tambahFavorit" targetid="<?= $produk[$i][$x]->id_produk_post; ?>">
                                                                            <i class="fa fa-heart-o"></i>
                                                                        </a>
                                                                    <?php endif; ?>

                                                                </div>
                                                                <div class="product-action-2">
                                                                    <!-- <a title="Add to cart" href="#">Add to cart</a> -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="product-content">
                                                            <div class="row mb-3">
                                                                <div class="col">
                                                                    <?php
                                                                    $r = rating($produk[$i][$x]->id_produk_post);
                                                                    $n = ($r != 0) ? $r : 0;
                                                                    $m = 5 - $n;
                                                                    ?>
                                                                    <ul class="rating-stars">
                                                                        <li class="stars-active">
                                                                            <?php for ($a = 0; $a < $n; $a++) {  ?>
                                                                                <i class="fa fa-star active"></i>
                                                                            <?php } ?>
                                                                            <?php for ($a = 0; $a < $m; $a++) {  ?>
                                                                                <i class="fa fa-star"></i>
                                                                            <?php } ?>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="col">
                                                                    <p class="label-rating text-muted"><?= $r ?> Rating</p>
                                                                </div>
                                                            </div>
                                                            <h3><a href="<?= base_url('produk/' . url_title($produk[$i][$x]->id_produk_post)) ?>"><?= $produk[$i][$x]->nama_produk ?></a></h3>
                                                            <div class="product-price">
                                                                <span><?= formatNumber($produk[$i][$x]->harga_produk, "Rp "); ?></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                        <?php }
                        } ?>
                        <!--/ End Single Tab -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Product Area -->



<!-- Start Midium Banner  -->

<!-- <section class="midium-banner">
    <div class="container">
        <div class="row">
            
            <div class="col-lg-6 col-md-6 col-12">
                <div class="single-banner">
                    <img src="http://via.placeholder.com/600x370" alt="#">
                    <div class="content">
                        <p>Man's Collectons</p>
                        <h3>Man's items <br>Up to<span> 50%</span></h3>
                        <a href="#">Shop Now</a>
                    </div>
                </div>
            </div>
          
            <div class="col-lg-6 col-md-6 col-12">
                <div class="single-banner">
                    <img src="http://via.placeholder.com/600x370" alt="#">
                    <div class="content">
                        <p>shoes women</p>
                        <h3>mid season <br> up to <span>70%</span></h3>
                        <a href="#" class="btn">Shop Now</a>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</section> -->

<!-- End Midium Banner -->

<!-- Start Most Popular -->

<div class="product-area most-popular section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-title">
                    <h2>Terlaris</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="owl-carousel popular-slider">

                    <?php foreach ($max as $row) : ?>
                        <div class="single-product">
                            <div class="product-img">
                                <a target="_blank" href="<?= base_url('produk/' . url_title($row->id_produk_post)) ?>">
                                    <?php $gam = gambar($row->id_produk_post); ?>
                                    <img class="img-home" src="<?= base_url('assets/pengguna/toko/produk/' . $gam[0]->gambar_produk) ?>" alt="#">
                                </a>
                                <div class="button-head">
                                    <div class="product-action">
                                        <a title="Lihat Produk" href="#" id="openModalProduk" data-toggle="modal" targetid="<?= $row->id_produk_post; ?>">
                                            <i class="ti-eye"></i>
                                        </a>

                                        <?php if (in_array($row->id_produk_post, $produk_favorit)) : ?>
                                            <a title="Hapus Favorit" href="#" data-toggle="modal" id="hapusFavorit" targetid="<?= $row->id_produk_post; ?>">
                                                <i class="fa fa-heart text-success"></i>
                                                <!-- <span>Favoritkan</span> -->
                                            </a>
                                        <?php else : ?>
                                            <a title="Favoritkan" href="#" data-toggle="modal" id="tambahFavorit" targetid="<?= $row->id_produk_post; ?>">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="product-content">
                                <div class="row mb-3">
                                    <div class="col">
                                        <?php
                                        $r = rating($row->id_produk_post);
                                        $n = ($r != 0) ? $r : 0;
                                        $m = 5 - $n;
                                        ?>
                                        <ul class="rating-stars">
                                            <li class="stars-active">
                                                <?php for ($a = 0; $a < $n; $a++) {  ?>
                                                    <i class="fa fa-star active"></i>
                                                <?php } ?>
                                                <?php for ($a = 0; $a < $m; $a++) {  ?>
                                                    <i class="fa fa-star"></i>
                                                <?php } ?>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col">
                                        <p class="label-rating text-muted"><?= $r ?> Rating</p>
                                    </div>
                                </div>
                                <h3><a href="<?= base_url('produk/' . $row->id_produk_post) ?>"><?= $row->nama_produk ?></a></h3>
                                <div class="product-price">
                                    <span><?= formatNumber($row->harga_produk, "Rp "); ?></span>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>

                </div>
            </div>
        </div>
    </div>
</div>



<div class="product-area most-popular section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-title">
                    <h2>Best Seller</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="owl-carousel popular-slider">

                    <?php foreach ($fav as $rw) : ?>
                        <div class="single-product">
                            <div class="product-img">
                                <a target="_blank" href="<?= base_url('produk/' . url_title($rw->id_produk_post)) ?>">
                                    
                                         <?php
                                        $gam = gambar($rw->id_produk_post);
                                        if ($gam) {
                                            $img = $gam[0]->gambar_produk;
                                        } else {
                                            $img = 'sample.jpg';
                                        }
                                        ?>
                                        
                                    <img class="img-home" src="<?= base_url('assets/pengguna/toko/produk/'.$img) ?>" alt="#">
                                </a>
                                <div class="button-head">
                                    <div class="product-action">
                                        <a title="Lihat Produk" href="#" id="openModalProduk" data-toggle="modal" targetid="<?= $rw->id_produk_post; ?>">
                                            <i class="ti-eye"></i>
                                        </a>

                                        <?php if (in_array($row->id_produk_post, $produk_favorit)) : ?>
                                            <a title="Hapus Favorit" href="#" data-toggle="modal" id="hapusFavorit" targetid="<?= $rw->id_produk_post; ?>">
                                                <i class="fa fa-heart text-success"></i>
                                            </a>
                                        <?php else : ?>
                                            <a title="Favoritkan" href="#" data-toggle="modal" id="tambahFavorit" targetid="<?= $rw->id_produk_post; ?>">
                                                <i class="fa fa-heart-o"></i>
                                            </a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="product-content">
                                <div class="row mb-3">
                                    <div class="col">
                                        <?php
                                        $r = rating($rw->id_produk_post);
                                        $n = ($r != 0) ? $r : 0;
                                        $m = 5 - $n;
                                        ?>
                                        <ul class="rating-stars">
                                            <li class="stars-active">
                                                <?php for ($a = 0; $a < $n; $a++) {  ?>
                                                    <i class="fa fa-star active"></i>
                                                <?php } ?>
                                                <?php for ($a = 0; $a < $m; $a++) {  ?>
                                                    <i class="fa fa-star"></i>
                                                <?php } ?>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col">
                                        <p class="label-rating text-muted"><?= $r ?> Rating</p>
                                    </div>
                                </div>
                                <h3><a href="<?= base_url('produk/' . $rw->id_produk_post) ?>"><?= $rw->nama_produk ?></a></h3>
                                <div class="product-price">
                                    <span><?= formatNumber($rw->harga_produk, "Rp "); ?></span>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- End Most Popular Area -->

<!-- Start Shop Home List  -->

<!-- <section class="shop-home-list section">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-12">
                <div class="row">
                    <div class="col-12">
                        <div class="shop-section-title">
                            <h1>On sale</h1>
                        </div>
                    </div>
                </div>

                <div class="single-list">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="list-image overlay">
                                <img src="http://via.placeholder.com/115x140" alt="#">
                                <a href="#" class="buy"><i class="fa fa-shopping-bag"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12 no-padding">
                            <div class="content">
                                <h4 class="title"><a href="#">Licity jelly leg flat Sandals</a></h4>
                                <p class="price with-discount">$59</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="single-list">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="list-image overlay">
                                <img src="http://via.placeholder.com/115x140" alt="#">
                                <a href="#" class="buy"><i class="fa fa-shopping-bag"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12 no-padding">
                            <div class="content">
                                <h5 class="title"><a href="#">Licity jelly leg flat Sandals</a></h5>
                                <p class="price with-discount">$44</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="single-list">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="list-image overlay">
                                <img src="http://via.placeholder.com/115x140" alt="#">
                                <a href="#" class="buy"><i class="fa fa-shopping-bag"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12 no-padding">
                            <div class="content">
                                <h5 class="title"><a href="#">Licity jelly leg flat Sandals</a></h5>
                                <p class="price with-discount">$89</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-lg-4 col-md-6 col-12">
                <div class="row">
                    <div class="col-12">
                        <div class="shop-section-title">
                            <h1>Best Seller</h1>
                        </div>
                    </div>
                </div>

                <div class="single-list">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="list-image overlay">
                                <img src="http://via.placeholder.com/115x140" alt="#">
                                <a href="#" class="buy"><i class="fa fa-shopping-bag"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12 no-padding">
                            <div class="content">
                                <h5 class="title"><a href="#">Licity jelly leg flat Sandals</a></h5>
                                <p class="price with-discount">$65</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="single-list">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="list-image overlay">
                                <img src="http://via.placeholder.com/115x140" alt="#">
                                <a href="#" class="buy"><i class="fa fa-shopping-bag"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12 no-padding">
                            <div class="content">
                                <h5 class="title"><a href="#">Licity jelly leg flat Sandals</a></h5>
                                <p class="price with-discount">$33</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="single-list">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="list-image overlay">
                                <img src="http://via.placeholder.com/115x140" alt="#">
                                <a href="#" class="buy"><i class="fa fa-shopping-bag"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12 no-padding">
                            <div class="content">
                                <h5 class="title"><a href="#">Licity jelly leg flat Sandals</a></h5>
                                <p class="price with-discount">$77</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-lg-4 col-md-6 col-12">
                <div class="row">
                    <div class="col-12">
                        <div class="shop-section-title">
                            <h1>Top viewed</h1>
                        </div>
                    </div>
                </div>
                <div class="single-list">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="list-image overlay">
                                <img src="http://via.placeholder.com/115x140" alt="#">
                                <a href="#" class="buy"><i class="fa fa-shopping-bag"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12 no-padding">
                            <div class="content">
                                <h5 class="title"><a href="#">Licity jelly leg flat Sandals</a></h5>
                                <p class="price with-discount">$22</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="single-list">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="list-image overlay">
                                <img src="http://via.placeholder.com/115x140" alt="#">
                                <a href="#" class="buy"><i class="fa fa-shopping-bag"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12 no-padding">
                            <div class="content">
                                <h5 class="title"><a href="#">Licity jelly leg flat Sandals</a></h5>
                                <p class="price with-discount">$35</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="single-list">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="list-image overlay">
                                <img src="http://via.placeholder.com/115x140" alt="#">
                                <a href="#" class="buy"><i class="fa fa-shopping-bag"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12 no-padding">
                            <div class="content">
                                <h5 class="title"><a href="#">Licity jelly leg flat Sandals</a></h5>
                                <p class="price with-discount">$99</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section> -->

<!-- End Shop Home List  -->

<!-- Start Cowndown Area -->

<!-- <section class="cown-down">
    <div class="section-inner ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6 col-12 padding-right">
                    <div class="image">
                        <img src="http://via.placeholder.com/750x590" alt="#">
                    </div>
                </div>
                <div class="col-lg-6 col-12 padding-left">
                    <div class="content">
                        <div class="heading-block">
                            <p class="small-title">Deal of day</p>
                            <h3 class="title">Beatutyful dress for women</h3>
                            <p class="text">Suspendisse massa leo, vestibulum cursus nulla sit amet, frungilla placerat lorem. Cars fermentum, sapien. </p>
                            <h1 class="price">$1200 <s>$1890</s></h1>
                            <div class="coming-time">
                                <div class="clearfix" data-countdown="2021/02/30"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> -->

<!-- /End Cowndown Area -->

<!-- Start Shop Blog  -->

<!-- <section class="shop-blog section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-title">
                    <h2>From Our Blog</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6 col-12">

                <div class="shop-single-blog">
                    <img src="http://via.placeholder.com/370x300" alt="#">
                    <div class="content">
                        <p class="date">22 July , 2020. Monday</p>
                        <a href="#" class="title">Sed adipiscing ornare.</a>
                        <a href="#" class="more-btn">Continue Reading</a>
                    </div>
                </div>

            </div>
            <div class="col-lg-4 col-md-6 col-12">

                <div class="shop-single-blog">
                    <img src="http://via.placeholder.com/370x300" alt="#">
                    <div class="content">
                        <p class="date">22 July, 2020. Monday</p>
                        <a href="#" class="title">Man’s Fashion Winter Sale</a>
                        <a href="#" class="more-btn">Continue Reading</a>
                    </div>
                </div>

            </div>
            <div class="col-lg-4 col-md-6 col-12">

                <div class="shop-single-blog">
                    <img src="http://via.placeholder.com/370x300" alt="#">
                    <div class="content">
                        <p class="date">22 July, 2020. Monday</p>
                        <a href="#" class="title">Women Fashion Festive</a>
                        <a href="#" class="more-btn">Continue Reading</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section> -->

<!-- End Shop Blog  -->

<!-- Start Shop Services Area -->

<!-- <section class="shop-services section home">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-12">

                <div class="single-service">
                    <i class="ti-rocket"></i>
                    <h4>Free shiping</h4>
                    <p>Orders over $100</p>
                </div>

            </div>
            <div class="col-lg-3 col-md-6 col-12">

                <div class="single-service">
                    <i class="ti-reload"></i>
                    <h4>Free Return</h4>
                    <p>Within 30 days returns</p>
                </div>

            </div>
            <div class="col-lg-3 col-md-6 col-12">

                <div class="single-service">
                    <i class="ti-lock"></i>
                    <h4>Sucure Payment</h4>
                    <p>100% secure payment</p>
                </div>

            </div>
            <div class="col-lg-3 col-md-6 col-12">

                <div class="single-service">
                    <i class="ti-tag"></i>
                    <h4>Best Peice</h4>
                    <p>Guaranteed price</p>
                </div>

            </div>
        </div>
    </div>
</section> -->

<!-- End Shop Services Area -->

<!-- Start Shop Newsletter  -->

<!-- <section class="shop-newsletter section">
    <div class="container">
        <div class="inner-top">
            <div class="row">
                <div class="col-lg-8 offset-lg-2 col-12">

                    <div class="inner">
                        <h4>Newsletter</h4>
                        <p> Subscribe to our newsletter and get <span>10%</span> off your first purchase</p>
                        <form action="mail/mail.php" method="get" target="_blank" class="newsletter-inner">
                            <input name="EMAIL" placeholder="Your email address" required="" type="email">
                            <button class="btn">Subscribe</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section> -->

<!-- End Shop Newsletter -->