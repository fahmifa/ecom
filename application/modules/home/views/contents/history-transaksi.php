<!-- Breadcrumbs -->
<div class="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="bread-inner">
                    <ul class="bread-list">
                        <li><a href="<?= base_url(); ?>">Home<i class="ti-arrow-right"></i></a></li>
                        <li class="active"><a href="#">History Transaksi</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Breadcrumbs -->

<div class="row no-gutters flex-column-reverse flex-lg-row">
    <div class="col-12 col-lg-9">
        <!-- Shopping Cart -->
        <div class="shopping-cart section py-0">
            <div class="container">
                <div class="row">
                    <div class="col-12 bg-white">
                        <div class="table-responsive">
                            <!-- Shopping Summery -->
                            <table class="table shopping-summery">
                                <thead>
                                    <tr class="main-hading">
                                        <th>PRODUK</th>
                                        <th>NAMA</th>
                                        <th class="text-center">HARGA</th>
                                        <th class="text-center">STATUS</th>
                                        <th class="text-center">TANGGAL PESANAN</th>
                                        <?php if ($this->uri->segment(3) === 'pending') : ?>
                                            <th></th>
                                        <?php endif; ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if ($history) : ?>
                                        <?php foreach ($history as $result) : ?>
                                            <tr>
                                                <td class="image" data-title="PRODUK"><img style="object-fit: cover;" src="<?= base_url('assets/pengguna/toko/produk/' . $result['gambar_produk']); ?>" alt="#"></td>
                                                <td class="product-des" data-title="NAMA">
                                                    <p class="product-name"><a href="#"><?= $result['nama_produk']; ?></a></p>
                                                </td>
                                                <td class="price" data-title="HARGA"><span><?= formatNumber($result['harga_produk'], "Rp"); ?></span></td>
                                                <td class="price" data-title="STATUS"><span><?= ucwords(str_replace('_', ' ', $result['status_pesanan'])); ?></span></td>
                                                <td class="total-amount" data-title="TANGGAL PESANAN"><span><?= date('d/m/Y H:i:s', strtotime($result['created_at'])); ?></span></td>
                                                <?php if ($this->uri->segment(3) === 'pending') : ?>
                                                    <td class="action">
                                                        <div class="d-flex justify-content-center align-items-center">
                                                            <button class="btn-sm btn-danger bg-danger mx-1" title="Batalkan Pesanan" id="batalkan_pesanan" style="line-height: 0" target="<?= $result['id_pesanan']; ?>"><i class="ti-close remove-icon text-white"></i></button>
                                                            <a class="btn-sm btn-primary bg-success mx-1" style="line-height: 1.35;" title="Upload Bukti Pembayaran" href="<?= base_url('home/formbuktipembayaran/' . $result['id_pesanan']); ?>"><i class="fa fa-upload remove-icon text-white"></i></a>
                                                        </div>
                                                    </td>
                                                <?php endif; ?>
                                            </tr>
                                        <?php endforeach; ?>
                                    <?php else : ?>
                                        <tr>
                                            <td colspan="6" class="text-center">History transaksi kosong</td>
                                        </tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                            <!--/ End Shopping Summery -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/ End Shopping Cart -->
    </div>

    <div class="col-12 col-lg-3 my-3 my-lg-0 px-3 px-lg-0">
        <div class="main-sidebar card mb-3 mt-0 p-4">
            <h6 class="card-title">Cari nama produk</h6>

            <div class="single-widget search mt-2">
                <div class="form">
                    <form action="" method="get">
                        <input type="text" name="q" placeholder="Cari nama produk disini" value="<?= $this->input->get('q'); ?>" />
                        <button type="submit" class="button"><i class="fa fa-search"></i></button>
                    </form>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-body pb-0">
                <h6 class="card-title">Saring berdasarkan :</h6>

                <div class="list-group" style="margin: 0 -20px;">
                    <a href="<?= base_url('home/historytransaksi/all'); ?>" class="list-group-item list-group-item-action <?= $this->uri->segment(3) === 'all' ? 'active' : ''; ?>" style="border: none; border-radius: 0;">Semua</a>
                    <a href="<?= base_url('home/historytransaksi/pending'); ?>" class="list-group-item list-group-item-action <?= $this->uri->segment(3) === 'pending' ? 'active' : ''; ?>" style="border: none;">Pending</a>
                    <a href="<?= base_url('home/historytransaksi/selesai'); ?>" class="list-group-item list-group-item-action <?= $this->uri->segment(3) === 'selesai' ? 'active' : ''; ?>" style="border: none;">Selesai</a>
                    <a href="<?= base_url('home/historytransaksi/dibatalkan'); ?>" class="list-group-item list-group-item-action <?= $this->uri->segment(3) === 'dibatalkan' ? 'active' : ''; ?>" style="border: none;">Dibatalkan</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Paging -->
<?= $this->pagination->create_links(); ?>