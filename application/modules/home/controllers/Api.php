<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Api extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_api', 'api');
        if (!userdata('id_pengguna')) {
            exit('Unauthorized!');
        }
    }

    public function batalkanpesanan()
    {
        $id_pesanan = $this->input->post('id_pesanan');
        $return = $this->api->batalkanPesanan(userdata('id_pengguna'), $id_pesanan);
        $output = ['kode' => $return];
        $this->output($output);
    }

    public function tambahfavorit()
    {
        $id_produk = $this->input->post('id_produk');
        $return = $this->api->tambahFavorit(userdata('id_pengguna'), $id_produk);
        $output = ['kode' => $return];
        $this->output($output);
    }

    public function hapusfavorit()
    {
        $id_produk = $this->input->post('id_produk');
        $return = $this->api->hapusFavorit(userdata('id_pengguna'), $id_produk);
        $output = ['kode' => $return];
        $this->output($output);
    }

    private function output($output)
    {
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($output));
    }
}
