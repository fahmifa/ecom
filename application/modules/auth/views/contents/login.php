</head>

<body>

    <div class="content p-0 d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-md-6 order-md-2">
                    <img src="<?= base_url('assets/assets-login/images/undraw_file_sync_ot38.svg'); ?>" alt="Image" class="img-fluid">
                </div>
                <div class="col-md-6 contents">
                    <div class="row justify-content-center">
                        <div class="col-md-8">
                            <div class="mb-4">
                                <h3>Log In ke <a href="<?= base_url(); ?>" class="text-body" style="text-decoration: none;"><strong>AweShop</strong></a></h3>
                                <p class="mb-4">Lorem ipsum dolor sit amet elit. Sapiente sit aut eos consectetur adipisicing.</p>
                            </div>

                            <?= $this->session->flashdata('msg'); ?>

                            <form action="" method="post">
                                <div class="form-group first">
                                    <label for="email">Email</label>
                                    <input type="text" class="form-control" id="email" name="email" value="<?= set_value('email'); ?>" required>
                                    <?= form_error('email'); ?>
                                </div>

                                <div class="form-group last mb-4">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control" id="password" name="password" required>
                                    <?= form_error('password'); ?>
                                </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-block btn-primary text-white">Log In</button>
                                    <div class="d-flex justify-content-between">
                                        <a href="<?= base_url('auth/daftar'); ?>" class="forgot-pass d-block">Daftar</a>
                                        <a href="<?= base_url('auth/lupapassword'); ?>" class="forgot-pass d-block">Lupa Password</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>


    <script src="<?= base_url('assets/assets-login/js/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?= base_url('assets/assets-login/js/popper.min.js'); ?>"></script>
    <script src="<?= base_url('assets/assets-login/js/bootstrap.min.js'); ?>"></script>
    <script src="<?= base_url('assets/assets-login/js/main.js'); ?>"></script>