</head>

<body>

    <div class="content p-0 d-flex align-items-center">
        <div class="container d-flex justify-content-center">
            <div class="card contents">
                <div class="card-body py-4">
                    <form action="" method="post">
                        <div class="form-group">
                            <label for="nama">Nama Lengkap</label>
                            <input type="text" class="form-control" id="nama" name="nama" required>
                            <?= form_error('nama'); ?>
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" minlength="6" class="form-control" id="password" name="password" required>
                            <?= form_error('password'); ?>
                        </div>

                        <button class="btn btn-success w-100 mt-3" type="submit" style="background: #38D39F; height: 45px;">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <script src="<?= base_url('assets/assets-login/js/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?= base_url('assets/assets-login/js/popper.min.js'); ?>"></script>
    <script src="<?= base_url('assets/assets-login/js/bootstrap.min.js'); ?>"></script>
    <script src="<?= base_url('assets/assets-login/js/main.js'); ?>"></script>