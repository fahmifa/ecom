</head>

<body>

    <div class="content p-0 d-flex align-items-center">
        <div class="container d-flex justify-content-center">
            <div class="card contents">
                <div class="card-body text-center">
                    <div>
                        <img class="img-fluid" width="30" src="<?= base_url('assets/mail.svg'); ?>" alt="">
                        <strong class="d-block mt-3">Masukkan Kode Verifikasi</strong>
                        <p class="text-dark">Kode verifikasi telah dikirim melalui e-mail ke <?= $this->input->get('email'); ?></p>
                    </div>

                    <form action="" method="post">
                        <div class="form-group first">
                            <input type="number" class="form-control w-50 text-center mx-auto" id="otp" name="otp">
                        </div>

                        <button class="btn btn-success w-75 mb-2" type="button" style="background: #38D39F; height: 45px;">Submit</button>
                    </form>

                    <small>
                        Tidak menerima kode? <span id="kirimUlangOTP">Kirim ulang</span>
                    </small>
                    <small>Mohon tunggu dalam 27 detik untuk kirim ulang</small>
                </div>
            </div>
        </div>
    </div>


    <script src="<?= base_url('assets/assets-login/js/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?= base_url('assets/assets-login/js/popper.min.js'); ?>"></script>
    <script src="<?= base_url('assets/assets-login/js/bootstrap.min.js'); ?>"></script>
    <script src="<?= base_url('assets/assets-login/js/main.js'); ?>"></script>