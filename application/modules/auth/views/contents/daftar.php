</head>

<body>

    <div class="content p-0 d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-md-6 mb-3 mb-md-0">
                    <img src="<?= base_url('assets/assets-login/images/undraw_file_sync_ot38.svg'); ?>" alt="Image" class="img-fluid" style="transform: scaleX(-1);">
                </div>
                <div class="col-md-6 contents">
                    <div class="row justify-content-center">
                        <div class="col-md-8">
                            <div class="mb-4">
                                <h3>Daftar Sekarang!</h3>
                                <p class="mb-4">Lorem ipsum dolor sit amet elit. Sapiente sit aut eos consectetur adipisicing.</p>
                            </div>

                            <?= $this->session->flashdata('msg'); ?>

                            <form action="" method="post">
                                <div class="form-group first">
                                    <label for="email">Email</label>
                                    <input type="text" class="form-control" id="email" name="email" value="<?= set_value('email'); ?>">
                                    <?= form_error('email'); ?>
                                </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-block btn-primary text-white">Daftar</button>
                                    <a href="<?= base_url('auth/login'); ?>" class="forgot-pass d-block">
                                        Saya sudah memiliki akun.
                                    </a>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>


    <script src="<?= base_url('assets/assets-login/js/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?= base_url('assets/assets-login/js/popper.min.js'); ?>"></script>
    <script src="<?= base_url('assets/assets-login/js/bootstrap.min.js'); ?>"></script>
    <script src="<?= base_url('assets/assets-login/js/main.js'); ?>"></script>