<script>
    $(document).ready(function() {
        let baseUrl = `<?= base_url(''); ?>`,
            email_session = `<?= $this->session->email_session; ?>`,
            otp = '';

        $('button').click(function() {
            otp = $('input').val();
            validasiOTP(otp);
        });

        function validasiOTP(otp) {
            let data = {
                otp: otp,
                email: email_session
            };

            $.ajax({
                url: `${ baseUrl }pengguna/apivalidasiotp`,
                method: "POST",
                data: data
            }).done((res) => {
                if (res.kode === '200') {
                    window.location.replace(`${ baseUrl }pengguna/setupakun`);
                }
            }).fail((res) => {
                console.log(res);
            }).always((res) => {
                console.log(res);
            });
        }

    });
</script>