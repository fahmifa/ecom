<?php

defined('BASEPATH') or exit('No direct script access allowed');

class M_auth extends CI_Model
{

    // Daftar ===========================================
    public function postOTP($otp_session, $email_session)
    {
        $expired = time() + (60 * 5);
        $data = ['otp' => $otp_session, 'email' => $email_session, 'expired' => $expired];
        $this->db->insert('tbl_pengguna_otp', $data);
    }

    public function getOTPWhere($otp_session, $email_session, $is_validated = 0)
    {
        $where = ['otp' => $otp_session, 'email' => $email_session, 'is_validated' => $is_validated];
        return $this->db->get_where('tbl_pengguna_otp', $where);
    }

    public function setOTPToValidated($otp_session, $email_session)
    {
        $this->db->set('is_validated', 1);
        $this->db->where(['otp' => $otp_session, 'email' => $email_session]);
        $this->db->update('tbl_pengguna_otp');

        return $this->db->affected_rows();
    }

    public function postAkunPengguna($data)
    {
        return $this->db->insert('tbl_pengguna', $data);
    }
    // Daftar ===========================================


    // Login ============================================
    public function getWhereEmail($email_session)
    {
        return $this->db->get_where('tbl_pengguna', ['email' => $email_session]);
    }
    // Login ============================================
}
