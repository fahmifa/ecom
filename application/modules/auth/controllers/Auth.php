<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_auth', 'auth');
		if ($this->session->email) {
			redirect(base_url());
		}
	}

	// daftar
	public function daftar()
	{
		$this->form_validation->set_rules('email', 'Email', 'trim|required|is_unique[tbl_pengguna.email]', ['required' => 'Mohon masukkan alamat email terlebih dahulu!', 'is_unique' => 'Maaf, email yang anda masukkan sudah terdaftar.']);
		$this->form_validation->set_error_delimiters('<small class="text-danger">', '</small>');

		if ($this->form_validation->run() === FALSE) {
			$data['title'] = 'Daftar';
			$this->load->view('template/header', $data);
			$this->load->view('contents/daftar');
			$this->load->view('template/footer');
		} else {
			$email = $this->input->post('email');

			$this->_sendOTP($email);
		}
	}

	private function _sendOTP($emailUser)
	{
		$emailServer = "xxx"; // change it to yours
		$passwordServer = "xxx"; // change it to yours

		$config = [
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_user' => $emailServer,
			'smtp_pass' => $passwordServer,
			'smtp_port' => 465,
			'mailtype' => 'html',
			'charset' => 'utf-8',
			'wordwrap' => TRUE,
			'newline' => "\r\n"
		];

		$this->load->library('email', $config);
		$this->email->initialize($config);

		$otp = rand(100000, 999999);
		$message = "OTP anda adalah <strong style='color: #38D39F;'>$otp</strong>";
		$this->email->from($emailServer);
		$this->email->to($emailUser);
		$this->email->subject('OTP');
		$this->email->message($message);
		if ($this->email->send()) {
			$this->auth->postOTP($otp, $emailUser);
			$set_session = [
				'otp_session' => $otp,
				'email_session' => htmlspecialchars($emailUser)
			];
			$this->session->set_userdata($set_session);
			redirect(base_url("auth/verifikasiotp"));
		} else {
			exit('Ada kesalahan server.');
			// echo $this->email->print_debugger();
		}
	}

	public function verifikasiotp()
	{
		$otp_session = $this->session->otp_session;
		$email_session = $this->session->email_session;
		$emailOTP = $this->auth->getOTPWhere($otp_session, $email_session);

		if ($emailOTP->num_rows() > 0) {
			$data['title'] = 'Verifikasi OTP';
			$this->load->view('template/header', $data);
			$this->load->view('css/verifikasi-otp'); // load CSS
			$this->load->view('contents/verifikasi-otp');
			$this->load->view('template/footer');
			$this->load->view('javascript/verifikasi-otp'); // load JS
		} else {
			redirect(base_url('auth/daftar'));
		}
	}

	public function apivalidasiotp()
	{
		$otp = $this->input->post('otp');
		$email = $this->session->email_session;
		$getOTPWhere = $this->auth->getOTPWhere($otp, $email);
		$output = [];

		if ($getOTPWhere->num_rows() > 0) {
			$expOTP = $getOTPWhere->row_array()['expired'];
			$waktuSekarang = time();

			if ($waktuSekarang > $expOTP) {
				$output = [
					'kode'	=> '01',
					'msg'	=> 'OTP yang anda masukkan telah kadaluarsa!'
				];
			} else {
				$setup = $this->auth->setOTPToValidated($otp, $email);
				if ($setup) {
					$output = [
						'kode'	=> '200',
						'msg'	=> 'ok',
						'email'	=> $email
					];
				} else {
					$this->session->set_flashdata('msg', $this->bootstrapalert('Maaf, sepertinya ada kesalahan server.', 'alert-danger'));
					redirect(base_url('auth/daftar'));
				}
			}
		} else {
			$output = [
				'kode'	=> '02',
				'msg'	=> 'OTP yang anda masukkan tidak ditemukan!'
			];
		}
		$this->output($output);
	}

	public function setupakun()
	{
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|max_length[128]', ['required' => 'Mohon masukkan Nama Lengkap terlebih dahulu!', 'is_unique' => 'Email yang anda masukkan sudah terdaftar!', 'max_length' => 'Silahkan masukkan Nama Lengkap dengan benar!']);
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]', ['required' => 'Mohon masukkan Password terlebih dahulu!', 'is_unique' => 'Email yang anda masukkan sudah terdaftar!', 'min_length' => 'Mohon masukkan password setidaknya 6 karakter!']);
		$this->form_validation->set_error_delimiters('<small class="text-danger">', '</small>');
		$otp_session = $this->session->otp_session;
		$email_session = $this->session->email_session;
		$emailOTP = $this->auth->getOTPWhere($otp_session, $email_session, 1);

		if ($emailOTP->num_rows() > 0) {
			if ($this->form_validation->run() === FALSE) {
				$data['title'] = 'Set up Akun';
				$this->load->view('template/header', $data);
				$this->load->view('contents/setupakun');
				$this->load->view('template/footer');
			} else {
				$nama = $this->input->post('nama');
				$password = $this->input->post('password');

				$this->_setupakun($nama, $password);
			}
		} else {
			redirect(base_url('auth/daftar'));
		}
	}

	private function _setupakun($namaPengguna, $password)
	{
		$this->_validasisession();
		$data = [
			'email' 		=> $this->session->email_session,
			'nama_pengguna'	=> htmlspecialchars($namaPengguna),
			'password'		=> password_hash($password, PASSWORD_DEFAULT)
		];

		$return_query = $this->auth->postAkunPengguna($data);

		$this->session->unset_userdata(['otp_session', 'email_session']);
		if ($return_query) {
			$this->session->set_flashdata('msg', $this->bootstrapalert('Daftar berhasil, silahkan login untuk melanjutkan.', 'alert-success'));
			redirect(base_url('auth/login'));
		} else {
			$this->session->set_flashdata('msg', $this->bootstrapalert('Sepertinya ada kesalahan sinkronisasi server, silahkan coba lagi.', 'alert-danger'));
			redirect(base_url('auth/daftar'));
		}
	}

	private function _validasisession()
	{
		$otp_session = $this->session->otp_session;
		$email_session = $this->session->email_session;

		$emailOTP = $this->auth->getOTPWhere($otp_session, $email_session, 1);

		if ($emailOTP->num_rows() < 1) {
			redirect(base_url('auth/daftar'));
		}
	}
	// daftar

	// login
	public function login()
	{
		$this->form_validation->set_rules('email', 'Email', 'trim|required', ['required' => 'Mohon isi kolom ini terlebih dahulu!']);
		$this->form_validation->set_rules('password', 'Password', 'trim|required', ['required' => 'Mohon isi kolom ini terlebih dahulu!']);
		$this->form_validation->set_error_delimiters('<small class="text-danger">', '</small>');

		if ($this->form_validation->run() === FALSE) {
			$data['title'] = 'Log In';
			$this->load->view('template/header', $data);
			$this->load->view('contents/login');
			$this->load->view('template/footer');
		} else {
			$email = $this->input->post('email');
			$password = $this->input->post('password');

			$this->loginValidation($email, $password);
		}
	}

	private function loginValidation($email, $password)
	{
		$getWhereEmail = $this->auth->getWhereEmail($email);

		if ($getWhereEmail->num_rows() > 0) {
			$passwordUser = $getWhereEmail->row_array()["password"];
			$user = $getWhereEmail->row_array()["id_pengguna"];
			if (password_verify($password, $passwordUser)) {
				$sess_data['email'] = $email;
				$sess_data['id'] = $user;
				$this->session->set_userdata($sess_data);
				redirect(base_url());
			} else {
				$this->session->set_flashdata('msg', $this->bootstrapalert('Password yang anda masukkan salah!', 'alert-danger'));
				redirect(base_url('auth/login'));
			}
		} else {
			$this->session->set_flashdata('msg', $this->bootstrapalert('Email pengguna tidak ditemukan!', 'alert-danger'));
			redirect(base_url('auth/login'));
		}
	}

	public function lupapassword()
	{
	}
	// login

	// helper
	private function output($data)
	{
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($data));
	}

	private function bootstrapalert($msg, $class)
	{
		$template = "<div class='alert $class' role='alert'>$msg</div>";

		return $template;
	}
	// helper
}
