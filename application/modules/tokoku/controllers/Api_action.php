<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Api_action extends CI_Controller
{
	private $_getTokoWhereId;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_tokoku', 'tokoku');
		$this->load->model('m_api_action', 'action');
		$this->_getTokoWhereId = $this->tokoku->getTokoWhereId(userdata('id_pengguna'));

		if ($this->_getTokoWhereId->num_rows() < 1) {
			redirect(base_url());
		}
	}

	// api
	public function batalkanpesanan()
	{
		$id_pesanan = $this->input->post('id_pesanan');
		$query = $this->action->batalkanpesanan($id_pesanan);
		$output = ['kode' => $query];
		$this->output($output);
	}

	public function prosespesanan()
	{
		$id_pesanan = $this->input->post('id_pesanan');
		$query = $this->action->prosespesanan($id_pesanan);
		$output = ['kode' => $query];
		$this->output($output);
	}

	public function ajukanpencairandana()
	{
		$id_pesanan = $this->input->post('id_pesanan');
		$query = $this->action->ajukanpencairandana($id_pesanan);
		$output = ['kode' => $query];
		$this->output($output);
	}
	// api

	// helper
	private function output($data)
	{
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($data));
	}
	// helper
}
