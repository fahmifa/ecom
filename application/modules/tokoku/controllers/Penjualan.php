<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Penjualan extends CI_Controller
{
	private $_getTokoWhereId;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_tokoku', 'tokoku');
		$this->load->model('m_penjualan', 'penjualan');
		$this->_getTokoWhereId = $this->tokoku->getTokoWhereId(userdata('id_pengguna'));

		if ($this->_getTokoWhereId->num_rows() < 1) {
			redirect(base_url());
		}
	}

	// view
	public function pesananbaru()
	{
		$data['title'] = 'Pesanan Baru';
		$this->load->view('template/header', $data);
		$this->load->view('css/global');
		$this->load->view('css/penjualan/pesanan-baru');
		$this->load->view('template/sidebar-topbar');
		$this->load->view('contents/penjualan/pesanan-baru');
		$this->load->view('javascript/penjualan/pesanan-baru');
		$this->load->view('template/footer');
	}

	public function pesanandiproses()
	{
		$data['title'] = 'Pesanan Diproses';
		$this->load->view('template/header', $data);
		$this->load->view('css/global');
		$this->load->view('css/penjualan/pesanan-diproses');
		$this->load->view('template/sidebar-topbar');
		$this->load->view('contents/penjualan/pesanan-diproses');
		$this->load->view('javascript/penjualan/pesanan-diproses');
		$this->load->view('template/footer');
	}

	public function pesananselesai()
	{
		$data['title'] = 'Pesanan Selesai';
		$this->load->view('template/header', $data);
		$this->load->view('css/global');
		$this->load->view('css/penjualan/pesanan-selesai');
		$this->load->view('template/sidebar-topbar');
		$this->load->view('contents/penjualan/pesanan-selesai');
		$this->load->view('javascript/penjualan/pesanan-selesai');
		$this->load->view('template/footer');
	}

	public function pesanandibatalkan()
	{
		$data['title'] = 'Pesanan Dibatalkan';
		$this->load->view('template/header', $data);
		$this->load->view('css/global');
		$this->load->view('css/penjualan/pesanan-dibatalkan');
		$this->load->view('template/sidebar-topbar');
		$this->load->view('contents/penjualan/pesanan-dibatalkan');
		$this->load->view('javascript/penjualan/pesanan-dibatalkan');
		$this->load->view('template/footer');
	}

	public function semuapesanan()
	{
		$data['title'] = 'Semua Pesanan';
		$this->load->view('template/header', $data);
		$this->load->view('css/global');
		$this->load->view('css/penjualan/semua-pesanan');
		$this->load->view('template/sidebar-topbar');
		$this->load->view('contents/penjualan/semua-pesanan');
		$this->load->view('javascript/penjualan/semua-pesanan');
		$this->load->view('template/footer');
	}

	public function lihatpesanan($id_pesanan)
	{
		$data['data_pesanan'] = $this->penjualan->selectPesananWhere($id_pesanan);
		// print_r($data['data_pesanan']);
		// die();
		if ($data['data_pesanan']) {
			$data['title'] = 'Lihat Pesanan';
			$this->load->view('template/header', $data);
			$this->load->view('css/index');
			$this->load->view('css/penjualan/lihat-pesanan');
			$this->load->view('template/sidebar-topbar');
			$this->load->view('contents/penjualan/lihat-pesanan');
			$this->load->view('javascript/penjualan/lihat-pesanan');
			$this->load->view('template/footer');
		} else {
			exit('Access not allowed!');
		}
	}
	// view

	// api
	public function apilistpesananbaru()
	{
		$data = [];

		// Fetch member's records
		$dataPesanan = $this->penjualan->dataTablePesanan($_POST);

		$i = $_POST['start'];
		foreach ($dataPesanan as $pesanan) {
			$i++;
			$total_harga = $pesanan['jumlah_pesanan'] * $pesanan['harga_produk'];
			$metode_pembayaran = str_replace("_", " ", $pesanan['metode_pembayaran']);
			$status_pembayaran = $pesanan['status_pesanan'] === "menunggu_pembayaran" ? '<span class="badge badge-primary">Menunggu Pembayaran' : '<span class="badge badge-warning">Menunggu Verifikasi Pembayaran';
			$tanggal_pesanan = date("d/m/Y H:i:s", strtotime($pesanan['created_at']));
			$data[] = [$i, $pesanan['nama_pembeli'], $pesanan['nama_produk'], ucfirst($pesanan['kondisi']), formatNumber($pesanan['jumlah_pesanan']), formatNumber($total_harga, "Rp"), ucwords($metode_pembayaran), $status_pembayaran, $tanggal_pesanan, $pesanan['id_pesanan']];
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->penjualan->countAll(),
			"recordsFiltered" => $this->penjualan->countFiltered($_POST),
			"data" => $data,
		);

		// Output to JSON format
		$this->output($output);
	}

	public function apilistpesanandiproses()
	{
		$data = [];
		$section = 'pesanan_diproses';

		// Fetch member's records
		$dataPesanan = $this->penjualan->dataTablePesanan($_POST, $section);

		$i = $_POST['start'];
		foreach ($dataPesanan as $pesanan) {
			$i++;
			$total_harga = $pesanan['jumlah_pesanan'] * $pesanan['harga_produk'];
			$metode_pembayaran = str_replace("_", " ", $pesanan['metode_pembayaran']);
			$status_pembayaran = '<span class="badge badge-warning">Sedang Diproses';
			$tanggal_pesanan = date("d/m/Y H:i:s", strtotime($pesanan['created_at']));
			$data[] = [$i, $pesanan['nama_pembeli'], $pesanan['nama_produk'], ucfirst($pesanan['kondisi']), formatNumber($pesanan['jumlah_pesanan']), formatNumber($total_harga, "Rp"), ucwords($metode_pembayaran), $status_pembayaran, $tanggal_pesanan, $pesanan['id_pesanan']];
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->penjualan->countAll($section),
			"recordsFiltered" => $this->penjualan->countFiltered($_POST, $section),
			"data" => $data,
		);

		// Output to JSON format
		$this->output($output);
	}

	public function apilistpesananselesai()
	{
		$data = [];
		$section = 'pesanan_selesai';

		// Fetch member's records
		$dataPesanan = $this->penjualan->dataTablePesanan($_POST, $section);

		$i = $_POST['start'];
		foreach ($dataPesanan as $pesanan) {
			$i++;
			$total_harga = $pesanan['jumlah_pesanan'] * $pesanan['harga_produk'];
			$metode_pembayaran = str_replace("_", " ", $pesanan['metode_pembayaran']);
			$status_pembayaran = $pesanan['status_pesanan'] === 'pesanan_selesai' ? '<span class="badge badge-success">Pesanan Selesai' : ($pesanan['status_pesanan'] === 'dana_diterima_penjual' ? '<span class="badge badge-success">Dana Diterima Penjual' : '<span class="badge badge-primary">Menunggu Pencairan Dana');
			$tanggal_pesanan = date("d/m/Y H:i:s", strtotime($pesanan['created_at']));
			$data[] = [$i, $pesanan['nama_pembeli'], $pesanan['nama_produk'], ucfirst($pesanan['kondisi']), formatNumber($pesanan['jumlah_pesanan']), formatNumber($total_harga, "Rp"), ucwords($metode_pembayaran), $status_pembayaran, $tanggal_pesanan, $pesanan['id_pesanan'], $pesanan['status_pesanan']];
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->penjualan->countAll($section),
			"recordsFiltered" => $this->penjualan->countFiltered($_POST, $section),
			"data" => $data,
		);

		// Output to JSON format
		$this->output($output);
	}

	public function apilistpesanandibatalkan()
	{
		$data = [];
		$section = 'dibatalkan';

		// Fetch member's records
		$dataPesanan = $this->penjualan->dataTablePesanan($_POST, $section);

		$i = $_POST['start'];
		foreach ($dataPesanan as $pesanan) {
			$i++;
			$total_harga = $pesanan['jumlah_pesanan'] * $pesanan['harga_produk'];
			$metode_pembayaran = str_replace("_", " ", $pesanan['metode_pembayaran']);
			$status_pembayaran = '<span class="badge badge-danger">Pesanan Dibatalkan oleh ' . ucfirst($pesanan['dibatalkan_oleh']);
			$tanggal_pesanan = date("d/m/Y H:i:s", strtotime($pesanan['created_at']));
			$data[] = [$i, $pesanan['nama_pembeli'], $pesanan['nama_produk'], ucfirst($pesanan['kondisi']), formatNumber($pesanan['jumlah_pesanan']), formatNumber($total_harga, "Rp"), ucwords($metode_pembayaran), $status_pembayaran, $tanggal_pesanan, $pesanan['id_pesanan']];
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->penjualan->countAll($section),
			"recordsFiltered" => $this->penjualan->countFiltered($_POST, $section),
			"data" => $data,
		);

		// Output to JSON format
		$this->output($output);
	}

	public function apilistsemuapesanan()
	{
		$data = [];
		$section = 'semua';

		// Fetch member's records
		$dataPesanan = $this->penjualan->dataTablePesanan($_POST, $section);

		$i = $_POST['start'];
		foreach ($dataPesanan as $pesanan) {
			$i++;
			$status_pesanan = $pesanan['status_pesanan'];
			$status = '';

			if ($status_pesanan === 'menunggu_pembayaran') {
				$status = '<span class="badge badge-primary">Menunggu Pembayaran';
			} elseif ($status_pesanan === 'sedang_diproses') {
				$status = '<span class="badge badge-warning">Sedang Diproses';
			} elseif ($status_pesanan === 'pesanan_selesai') {
				$status = '<span class="badge badge-success">Pesanan Selesai';
			} elseif ($status_pesanan === 'dibatalkan') {
				$status = '<span class="badge badge-danger">Pesanan Dibatalkan oleh ' . ucfirst($pesanan['dibatalkan_oleh']);
			} elseif ($status_pesanan === 'menunggu_pencairan_dana') {
				$status = '<span class="badge badge-primary">Menunggu Pencairan Dana';
			} elseif ($status_pesanan === 'menunggu_verifikasi') {
				$status = '<span class="badge badge-warning">Menunggu Verifikasi Pembayaran';
			} elseif ($status_pesanan === 'dana_diterima_penjual') {
				$status = '<span class="badge badge-success">Dana Diterima Penjual';
			}

			$total_harga = $pesanan['jumlah_pesanan'] * $pesanan['harga_produk'];
			$metode_pembayaran = str_replace("_", " ", $pesanan['metode_pembayaran']);
			$tanggal_pesanan = date("d/m/Y H:i:s", strtotime($pesanan['created_at']));
			$data[] = [$i, $pesanan['nama_pembeli'], $pesanan['nama_produk'], ucfirst($pesanan['kondisi']), formatNumber($pesanan['jumlah_pesanan']), formatNumber($total_harga, "Rp"), ucwords($metode_pembayaran), $status, $tanggal_pesanan, $pesanan['id_pesanan'], $pesanan['status_pesanan']];
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->penjualan->countAll($section),
			"recordsFiltered" => $this->penjualan->countFiltered($_POST, $section),
			"data" => $data,
		);

		// Output to JSON format
		$this->output($output);
	}
	// api

	// helper
	private function output($data)
	{
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($data));
	}

	private function bootstrapalert($msg, $class)
	{
		$template = "<div class='alert $class' role='alert'>$msg</div>";

		return $template;
	}
	// helper
}
