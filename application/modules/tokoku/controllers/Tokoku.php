<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tokoku extends CI_Controller
{
	private $_getTokoWhereId;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_tokoku', 'tokoku');
		$this->_getTokoWhereId = $this->tokoku->getTokoWhereId(userdata('id_pengguna'));

		if ($this->_getTokoWhereId->num_rows() < 1) {
			redirect(base_url());
		}
	}

	// view
	public function index()
	{
		$id_toko = $this->_getTokoWhereId->row_array()['id_toko'];
		$data['title'] = 'Dashboard';
		$data['totalPesananBaru'] = $this->tokoku->totalPesananBaru($id_toko);
		$data['totalPesananDiproses'] = $this->tokoku->totalPesananDiproses($id_toko);
		$data['totalPesananSelesai'] = $this->tokoku->totalPesananSelesai($id_toko);
		$data['estimasiTotalPendapatan'] = $this->tokoku->estimasiTotalPendapatan($id_toko);
		$this->load->view('template/header', $data);
		$this->load->view('css/index');
		$this->load->view('template/sidebar-topbar');
		$this->load->view('contents/index');
		$this->load->view('template/footer');
	}

	public function profil()
	{
		$rules = [
			'required'		=> 'Mohon masukkan Nama Toko terlebih dahulu!',
			'max_length'	=> 'Oops Nama Toko terlalu panjang! Panjang maximum 64 karakter!',
			'is_unique'		=> 'Maaf Nama Toko sudah digunakan!'
		];
		$this->form_validation->set_rules('namatoko', 'namaToko', 'trim|required|max_length[64]', $rules);
		$this->form_validation->set_rules('alamattoko', 'alamatToko', 'trim|required', ['required' => 'Mohon masukkan Alamat Toko terlebih dahulu!']);
		$this->form_validation->set_rules('deskripsitoko', 'deskripsiToko', 'trim');
		$this->form_validation->set_error_delimiters('<small class="text-danger">', '</small>');

		if ($this->form_validation->run() === FALSE) {
			$data['profil_toko'] = $this->_getTokoWhereId->row_array();
			$data['title'] = 'Profil Toko';
			$this->load->view('template/header', $data);
			$this->load->view('css/index');
			$this->load->view('css/profil-toko');
			$this->load->view('template/sidebar-topbar');
			$this->load->view('contents/profil-toko');
			$this->load->view('javascript/profil-toko');
			$this->load->view('template/footer');
		} else {
			$gambartoko		= $_FILES['gambar_toko']['name'];
			$namatoko		= $this->input->post('namatoko', true);
			$alamattoko		= $this->input->post('alamattoko', true);
			$deskripsitoko	= $this->input->post('deskripsitoko', true);
			$this->_edittoko($gambartoko, $namatoko, $alamattoko, $deskripsitoko);
		}
	}

	private function _edittoko($gambartoko, $namatoko, $alamattoko, $deskripsitoko)
	{
		$data = [
			'nama_toko'			=> $namatoko,
			'alamat_toko'		=> $alamattoko,
			'deskripsi_toko'	=> $deskripsitoko,
		];
		$this->tokoku->editToko(userdata('id_pengguna'), $data);

		if ($gambartoko) {
			$config['upload_path']		= './assets/pengguna/profil/';
			$config['allowed_types']	= 'jpg|jpeg|png';
			$config['file_name']		= "profil_" . userdata('id_pengguna');
			$config['max_height']		= '100';
			$config['max_width']		= '100';

			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			if ($this->upload->do_upload('gambar_toko')) {
				if (userdata('gambar_pengguna') !== 'user_default.svg') {
					unlink(FCPATH . '/assets/pengguna/profil/' . userdata('gambar_pengguna'));
				}

				$namagambar = $this->upload->data("file_name");
				$this->tokoku->editGambarProfil($namagambar, userdata('id_pengguna'));
			} else {
				$this->session->set_flashdata('msg', $this->bootstrapalert($this->upload->display_errors('', ''), 'alert-danger'));
			}
		}

		redirect(base_url('tokoku/profil'));
	}

	public function produklists()
	{
		$data['title'] = 'Produk';
		$this->load->view('template/header', $data);
		$this->load->view('css/produk-lists');
		$this->load->view('css/global');
		$this->load->view('template/sidebar-topbar');
		$this->load->view('contents/produk-lists');
		$this->load->view('javascript/produk-lists');
		$this->load->view('template/footer');
	}

	public function tambahproduk()
	{
		$this->form_validation->set_rules('nama', 'nama', 'trim|required', ['required' => 'Mohon masukkan Nama Produk terlebih dahulu!']);
		$this->form_validation->set_rules('harga', 'Harga', 'trim|required', ['required' => 'Mohon masukkan Harga Produk terlebih dahulu!']);
		$this->form_validation->set_rules('stok', 'Stok', 'trim|required', ['required' => 'Mohon masukkan Stok Produk terlebih dahulu!']);
		$this->form_validation->set_error_delimiters('<small class="text-danger">', '</small>');

		if ($this->form_validation->run() === FALSE) {
			$data['title'] = 'Tambah Produk';
			$data['kategori'] = $this->tokoku->kategoriSelect2();
			$this->load->view('template/header', $data);
			$this->load->view('css/index');
			$this->load->view('css/tambah-produk');
			$this->load->view('template/sidebar-topbar');
			$this->load->view('contents/tambah-produk');
			$this->load->view('javascript/tambah-produk');
			$this->load->view('template/footer');
		} else {
			$gambarproduk = $_FILES['gambar'];
			$dataPostProduk = [
				'id_kategori'		=> $this->input->post('idkategori', true),
				'id_toko'			=> $this->_getTokoWhereId->row_array()['id_toko'],
				'nama_produk'		=> $this->input->post('nama', true),
				'deskripsi_produk'	=> $this->input->post('deskripsi', true),
				'harga_produk'		=> getNumber($this->input->post('harga')),
				'stok'				=> getNumber($this->input->post('stok')),
				'kondisi'			=> $this->input->post('kondisi', true),
			];

			$this->_postProduk($gambarproduk, $dataPostProduk);
		}
	}

	public function updateproduk($id_produk)
	{
		$this->form_validation->set_rules('nama', 'nama', 'trim|required', ['required' => 'Mohon masukkan Nama Produk terlebih dahulu!']);
		$this->form_validation->set_rules('harga', 'Harga', 'trim|required', ['required' => 'Mohon masukkan Harga Produk terlebih dahulu!']);
		$this->form_validation->set_rules('stok', 'Stok', 'trim|required', ['required' => 'Mohon masukkan Stok Produk terlebih dahulu!']);
		$this->form_validation->set_error_delimiters('<small class="text-danger">', '</small>');

		$getTokoWhere = $this->tokoku->getTokoWhere(userdata('id_pengguna'));
		$id_toko = $getTokoWhere->row_array()['id_toko'];

		$data['title'] = 'Update Produk';
		$data['kategori'] = $this->tokoku->kategoriSelect2();
		$data['produk'] = $this->tokoku->selectProdukWhere($id_produk, $id_toko);
		$data['gambar_produk'] = $this->tokoku->selectGambarProdukWhere($id_produk);

		if ($data['produk']) {
			if ($this->form_validation->run() === FALSE) {
				$this->load->view('template/header', $data);
				$this->load->view('css/index');
				$this->load->view('css/update-produk');
				$this->load->view('template/sidebar-topbar');
				$this->load->view('contents/update-produk');
				$this->load->view('javascript/update-produk');
				$this->load->view('template/footer');
			} else {
				$gambarproduk = $_FILES['gambar'];
				$dataPostProduk = [
					'id_kategori'		=> $this->input->post('idkategori', true),
					'nama_produk'		=> $this->input->post('nama', true),
					'deskripsi_produk'	=> $this->input->post('deskripsi', true),
					'harga_produk'		=> getNumber($this->input->post('harga')),
					'stok'				=> getNumber($this->input->post('stok')),
					'kondisi'			=> $this->input->post('kondisi', true),
					'status_post'		=> $this->input->post('status', true),
				];

				$this->_updateProduk($gambarproduk, $data['gambar_produk'], $id_produk, $dataPostProduk);
			}
		} else {
			exit('Access not allowed!');
		}
	}
	// view

	public function verifikasiproduklists()
	{
		$data['title'] = 'List Pengajuan Launching Produk';
		$this->load->view('template/header', $data);
		$this->load->view('css/verifikasi-produk-lists');
		$this->load->view('css/global');
		$this->load->view('template/sidebar-topbar');
		$this->load->view('contents/verifikasi-produk-lists');
		$this->load->view('javascript/verifikasi-produk-lists');
		$this->load->view('template/footer');
	}

	// api
	public function apidatatableproduk()
	{
		$data = array();

		// Fetch member's records
		$dataProduk = $this->tokoku->dataTableProduk($_POST);

		$i = $_POST['start'];
		foreach ($dataProduk as $produk) {
			$i++;
			$data[] = array($i, $produk['nama_produk'], formatNumber($produk['harga_produk'], "Rp"), formatNumber($produk['stok']), formatNumber($produk['terjual']), ucfirst($produk['kondisi']), ucfirst($produk['status_post']), $produk['id_produk_post']);
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->tokoku->countAll(),
			"recordsFiltered" => $this->tokoku->countFiltered($_POST),
			"data" => $data,
		);

		// Output to JSON format
		$this->output($output);
	}

	public function apidatatableverifikasiproduk()
	{
		$data = [];

		// Fetch member's records
		$dataProduk = $this->tokoku->dataTableProduk($_POST, false);

		$i = $_POST['start'];
		foreach ($dataProduk as $produk) {
			$i++;
			$status_live = $produk['status_live'] === 'verifikasi' ? '<span class="badge badge-warning">Menunggu Verifikasi' : '<span class="badge badge-info">Menunggu Disunting';
			$data[] = [$i, $produk['nama_produk'], formatNumber($produk['harga_produk'], "Rp"), formatNumber($produk['stok']), ucfirst($produk['kondisi']), $status_live, $produk['id_produk_post']];
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->tokoku->countAll(false),
			"recordsFiltered" => $this->tokoku->countFiltered($_POST, false),
			"data" => $data,
		);

		// Output to JSON format
		$this->output($output);
	}

	public function apihapusproduk()
	{
		$id_produk_post = $this->input->post('id_produk');
		$getTokoWhere = $this->tokoku->getTokoWhere(userdata('id_pengguna'));
		$id_toko = $getTokoWhere->row_array()['id_toko'];
		$return = $this->tokoku->deleteProdukPost($id_produk_post, $id_toko);
		$output = [
			'kode' => "$return",
		];
		$this->output($output);
	}

	public function apideletegambar()
	{
		$id_produk = $this->input->post('id_produk_post');
		$id_gambar = $this->input->post('id_produk_gambar');
		$output = [];
		$data = [
			'id_produk_post'	=> $id_produk,
			'id_produk_gambar'	=> $id_gambar
		];

		$getGambarProdukWhere = $this->tokoku->getGambarProdukWhere($data);
		$deleteGambarProduk = $this->tokoku->deleteGambarProduk($data);
		if ($deleteGambarProduk) {
			unlink(FCPATH . '/assets/pengguna/toko/produk/' . $getGambarProdukWhere[0]['gambar_produk']);
			$output = [
				'kode'	=> '1',
				'msg'	=> 'Hapus Gambar Produk berhasil!'
			];
		} else {
			$output = [
				'kode'	=> '0',
				'msg'	=> 'Hapus Gambar Produk gagal!'
			];
		}
		$this->output($output);
	}
	// api

	// function
	private function _postProduk($gambarproduk, $dataPostProduk)
	{
		if ($gambarproduk['name'][0] !== '') {
			$id_produk_post = rand(10000, 99999) . rand(10000, 99999);
			$dataPostProduk['id_produk_post'] = $id_produk_post;

			$this->tokoku->postProduk($dataPostProduk);
			$this->_postGambar($id_produk_post, $gambarproduk);

			$this->session->set_flashdata('msg', $this->bootstrapalert('Produk berhasil ditambahkan dan akan segera ditampilkan setelah berhasil diverifikasi oleh admin.', 'alert-success'));
			redirect(base_url('tokoku/tambahproduk'));
		} else {
			$this->session->set_flashdata('msg', $this->bootstrapalert('Mohon pilih Gambar Utama Produk terlebih dahulu!', 'alert-danger'));
			redirect(base_url('tokoku/tambahproduk'));
		}
	}

	private function _postGambar($id_produk_post, $gambarproduk)
	{
		$config['upload_path']		= './assets/pengguna/toko/produk/';
		$config['allowed_types']	= 'jpg|jpeg|png';

		$this->load->library('upload', $config);

		foreach ($gambarproduk['name'] as $index => $value) {
			$id_produk_gambar				= rand(10000, 99999) . rand(10000, 99999);
			$_FILES['gambar']['name'] 		= $gambarproduk['name'][$index];
			$_FILES['gambar']['type'] 		= $gambarproduk['type'][$index];
			$_FILES['gambar']['tmp_name']	= $gambarproduk['tmp_name'][$index];
			$_FILES['gambar']['error']		= $gambarproduk['error'][$index];
			$_FILES['gambar']['size']		= $gambarproduk['size'][$index];

			$config['file_name'] = "produk_" . rand();

			$this->upload->initialize($config);

			if ($index === 0) {
				if (!$this->upload->do_upload('gambar')) {
					$getTokoWhere = $this->tokoku->getTokoWhere(userdata('id_pengguna'));
					$id_toko = $getTokoWhere->row_array()['id_toko'];

					$this->tokoku->deleteProdukPost($id_produk_post, $id_toko);
					$this->session->set_flashdata('msg', $this->bootstrapalert('Ada kesalahan saat upload Gambar Utama : <br> - ' . $this->upload->display_errors('<small>', '</small>'), 'alert-danger'));
					redirect(base_url('tokoku/tambahproduk'));
				} else {
					$namagambar = $this->upload->data("file_name");
					$this->tokoku->postGambarProduk($id_produk_gambar, $id_produk_post, $namagambar, 1);
				}
			} else {
				if ($this->upload->do_upload('gambar')) {
					$namagambar = $this->upload->data("file_name");
					$this->tokoku->postGambarProduk($id_produk_gambar, $id_produk_post, $namagambar);
				}
			}
		}
	}

	private function _updateProduk($gambarproduk, $dbgambarproduk, $id_produk, $dataPostProduk)
	{
		$updateProduk = $this->tokoku->updateProduk($id_produk, $dataPostProduk);
		$this->_updateGambar($gambarproduk, $dbgambarproduk, $id_produk);
	}

	private function _updateGambar($gambarproduk, $dbgambarproduk, $id_produk)
	{
		$config['upload_path']		= './assets/pengguna/toko/produk/';
		$config['allowed_types']	= 'jpg|jpeg|png';

		$this->load->library('upload', $config);

		$list_notice_upload = '';
		foreach ($gambarproduk['name'] as $index => $value) {
			$urutan_gambar					= $index === 0 ? '1 (Utama)' : ($index + 1);
			$is_thumbnail 					= $index === 0 ? 1 : 0;
			$_FILES['gambar']['name'] 		= $gambarproduk['name'][$index];
			$_FILES['gambar']['type'] 		= $gambarproduk['type'][$index];
			$_FILES['gambar']['tmp_name']	= $gambarproduk['tmp_name'][$index];
			$_FILES['gambar']['error']		= $gambarproduk['error'][$index];
			$_FILES['gambar']['size']		= $gambarproduk['size'][$index];

			$config['file_name'] = "produk_" . rand();

			$this->upload->initialize($config);

			if ($gambarproduk['name'][$index] !== '' && array_key_exists($index, $dbgambarproduk)) {
				if (!$this->upload->do_upload('gambar')) {
					$list_notice_upload .= '<p class="m-0"><i class="fas fa-exclamation-circle text-warning"></i>&nbsp; Gambar ke ' . $urutan_gambar . ' tidak diupdate</p>';
				} else {
					$namagambar = $this->upload->data("file_name");
					$old_file = $dbgambarproduk[$index]['gambar_produk'];
					$id_gambar = $dbgambarproduk[$index]['id_produk_gambar'];
					unlink(FCPATH . '/assets/pengguna/toko/produk/' . $old_file);
					$this->tokoku->updateGambarProduk($id_produk, $id_gambar, $namagambar, $is_thumbnail);
					$list_notice_upload .= '<p class="m-0"><i class="fas fa-check-circle text-success"></i>&nbsp; Gambar ke ' . $urutan_gambar . ' telah diupdate</p>';
				}
			} else {
				if ($this->upload->do_upload('gambar')) {
					$namagambar = $this->upload->data("file_name");
					$id_produk_gambar = rand(10000, 99999) . rand(10000, 99999);
					$list_notice_upload .= '<p class="m-0"><i class="fas fa-check-circle text-success"></i>&nbsp; Gambar ke ' . $urutan_gambar . ' telah diupdate</p>';
					$this->tokoku->postGambarProduk($id_produk_gambar, $id_produk, $namagambar, $is_thumbnail);
				} else {
					$list_notice_upload .= '<p class="m-0"><i class="fas fa-exclamation-circle text-warning"></i>&nbsp; Gambar ke ' . $urutan_gambar . ' tidak diupdate</p>';
				}
			}
		}

		$this->session->set_flashdata(
			'msg',
			$this->bootstrapalert('Produk telah berhasil diupdate, produk akan ditampikan kembali setelah berhasil diverifikasi oleh admin.', 'alert-success mb-2') . $this->bootstrapalert("<p class='mb-1'>Keterangan upload gambar</span> : $list_notice_upload", 'alert-success')
		);
		redirect(base_url("tokoku/updateproduk/$id_produk"));
	}
	// function

	// helper
	private function output($data)
	{
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($data));
	}

	private function bootstrapalert($msg, $class)
	{
		$template = "<div class='alert $class' role='alert'>$msg</div>";

		return $template;
	}
	// helper
}
