<?php

defined('BASEPATH') or exit('No direct script access allowed');

class M_tokoku extends CI_Model
{
    public function __construct()
    {
        $this->table = 'tbl_produk_post';
        $this->column_order = array(null, 'nama_produk', 'harga_produk', 'stok', 'terjual', 'kondisi', 'status_post');
        $this->column_search = array('nama_produk', 'harga_produk', 'stok', 'terjual', 'kondisi', 'status_post');
        $this->order = array('nama_produk' => 'asc');
    }

    // Datatable produk settings start
    public function dataTableProduk($postData, $isLive = true)
    {
        $this->_get_datatables_query($postData);
        if ($postData['length'] != -1) {
            $this->db->limit($postData['length'], $postData['start']);
        }
        if ($isLive) $this->db->where('status_live', 'live');
        else $this->db->where_in('status_live', ['verifikasi', 'menunggu_disunting']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function countAll($isLive = true)
    {
        $this->db->from($this->table);
        if ($isLive) $this->db->where('status_live', 'live');
        else $this->db->where_in('status_live', ['verifikasi', 'menunggu_disunting']);
        return $this->db->count_all_results();
    }

    public function countFiltered($postData, $isLive = true)
    {
        $this->_get_datatables_query($postData);
        if ($isLive) $this->db->where('status_live', 'live');
        else $this->db->where_in('status_live', ['verifikasi', 'menunggu_disunting']);
        $query = $this->db->get();
        return $query->num_rows();
    }

    private function _get_datatables_query($postData)
    {

        $this->db->from($this->table);

        $i = 0;
        foreach ($this->column_search as $item) {
            if ($postData['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $postData['search']['value']);
                } elseif ($i === 1 || 2 || 3) {
                    $this->db->or_like($item, preg_replace("/[^\d]/", "", $postData['search']['value']));
                } else {
                    $this->db->or_like($item, $postData['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($postData['order'])) {
            $this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    // Datatable produk settings end

    public function getTokoWhereId($idUser)
    {
        return $this->db->get_where('tbl_pengguna_toko', ['id_pengguna' => $idUser]);
    }

    public function kategoriSelect2()
    {
        $result = $this->db->query("SELECT `id_kategori` AS id, `nama_kategori` AS text FROM tbl_produk_kategori WHERE id_kategori != '12345' ORDER BY `nama_kategori` ASC");

        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return null;
        }
    }

    public function postGambarProduk($id_produk_gambar, $id_produk_post, $namagambar, $is_thumbnail = 0)
    {
        $data = [
            'id_produk_gambar'  => $id_produk_gambar,
            'id_produk_post'    => $id_produk_post,
            'gambar_produk'     => $namagambar,
            'is_thumbnail'      => $is_thumbnail
        ];
        $this->db->insert('tbl_produk_gambar', $data);
    }

    public function deleteProdukPost($id_produk_post, $id_toko)
    {
        $where = [
            'id_produk_post' => $id_produk_post,
            'id_toko' => $id_toko
        ];
        $this->db->delete('tbl_produk_post', $where);
        return $this->db->affected_rows();
    }

    public function postProduk($data)
    {
        $this->db->insert('tbl_produk_post', $data);
    }

    public function getTokoWhere($id_pengguna)
    {
        return $this->db->get_where('tbl_pengguna_toko', ['id_pengguna' => $id_pengguna]);
    }

    public function selectProdukWhere($id_produk, $id_toko)
    {
        $this->db->select('
            tbl_produk_post.nama_produk,
            tbl_produk_post.id_kategori,
            tbl_produk_post.kondisi,
            tbl_produk_post.deskripsi_produk,
            tbl_produk_post.harga_produk,
            tbl_produk_post.stok,
            tbl_produk_post.status_post,
        ')
            ->from('tbl_produk_post')
            ->limit(1)
            ->where('tbl_produk_post.id_produk_post', $id_produk)
            ->where('tbl_produk_post.id_toko', $id_toko);

        return $this->db->get()->result_array();
    }

    public function selectGambarProdukWhere($id_produk)
    {
        $this->db->select('id_produk_gambar, id_produk_post, gambar_produk')
            ->from('tbl_produk_gambar')
            ->where('id_produk_post', $id_produk)
            ->order_by('is_thumbnail', 'desc')
            ->order_by('created_at', 'asc');

        return $this->db->get()->result_array();
    }

    public function updateProduk($id_produk, $data)
    {
        $this->db->set($data);
        $this->db->where('id_produk_post', $id_produk);
        $this->db->update('tbl_produk_post');

        return $this->db->affected_rows();
    }

    public function updateGambarProduk($id_produk, $id_gambar, $namagambar, $is_thumbnail)
    {
        $where = [
            'id_produk_post'    => $id_produk,
            'id_produk_gambar'  => $id_gambar
        ];
        $set = [
            'gambar_produk' => $namagambar,
            'is_thumbnail'  => $is_thumbnail,
        ];
        $this->db->set($set);
        $this->db->where($where);
        $this->db->update('tbl_produk_gambar');
    }

    public function deleteGambarProduk($data)
    {
        $this->db->delete('tbl_produk_gambar', $data);
        return $this->db->affected_rows();
    }

    public function getGambarProdukWhere($obj)
    {
        $result = $this->db->get_where('tbl_produk_gambar', $obj);

        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return 1;
        }
    }

    public function totalPesananBaru($id_toko)
    {
        $joinProdukPost = "(SELECT * FROM tbl_produk_post WHERE id_toko = $id_toko) tbl_produk_post";
        $this->db->from('tbl_pengguna_pesanan');
        $this->db->join($joinProdukPost, 'tbl_produk_post.id_produk_post = tbl_pengguna_pesanan.id_produk_post');
        $this->db->where_in('status_pesanan', ["menunggu_pembayaran", "menunggu_verifikasi"]);
        return $this->db->count_all_results();
    }

    public function totalPesananDiproses($id_toko)
    {
        $joinProdukPost = "(SELECT * FROM tbl_produk_post WHERE id_toko = $id_toko) tbl_produk_post";
        $this->db->from('tbl_pengguna_pesanan');
        $this->db->join($joinProdukPost, 'tbl_produk_post.id_produk_post = tbl_pengguna_pesanan.id_produk_post');
        $this->db->where('tbl_pengguna_pesanan.status_pesanan', 'sedang_diproses');
        return $this->db->count_all_results();
    }

    public function totalPesananSelesai($id_toko)
    {
        $joinProdukPost = "(SELECT * FROM tbl_produk_post WHERE id_toko = $id_toko) tbl_produk_post";
        $this->db->from('tbl_pengguna_pesanan');
        $this->db->join($joinProdukPost, 'tbl_produk_post.id_produk_post = tbl_pengguna_pesanan.id_produk_post');
        $this->db->where_in('tbl_pengguna_pesanan.status_pesanan', ['pesanan_selesai', 'menunggu_pencairan_dana', 'dana_diterima_penjual']);
        return $this->db->count_all_results();
    }

    public function estimasiTotalPendapatan($id_toko)
    {
        $joinProdukPost = "(SELECT * FROM tbl_produk_post WHERE id_toko = $id_toko) tbl_produk_post";
        $this->db->select("IFNULL(SUM(total_harga), 0) AS total_pendapatan");
        $this->db->from('tbl_pengguna_pesanan');
        $this->db->join($joinProdukPost, 'tbl_produk_post.id_produk_post = tbl_pengguna_pesanan.id_produk_post');
        $this->db->where_in('tbl_pengguna_pesanan.status_pesanan', ['pesanan_selesai', 'menunggu_pencairan_dana', 'dana_diterima_penjual']);
        return $this->db->get()->result_array()[0]['total_pendapatan'];
    }

    public function editToko($id_pengguna, $data)
    {
        $this->db->set($data);
        $this->db->where('id_pengguna', $id_pengguna);
        $this->db->update('tbl_pengguna_toko');
    }

    public function editGambarProfil($gambar, $id_pengguna)
    {
        $this->db->set('gambar_pengguna', $gambar);
        $this->db->where('id_pengguna', $id_pengguna);
        $this->db->update('tbl_pengguna');
    }
}
