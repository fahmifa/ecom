<?php

defined('BASEPATH') or exit('No direct script access allowed');

class M_api_action extends CI_Model
{
    public function batalkanpesanan($id_pesanan)
    {
        $set = ['status_pesanan' => 'dibatalkan', 'dibatalkan_oleh' => 'toko'];
        $this->db->set($set);
        $this->db->where($this->where($id_pesanan));
        $this->db->update('tbl_pengguna_pesanan');
        return $this->db->affected_rows();
    }

    public function prosespesanan($id_pesanan)
    {
        $set = ['status_pesanan' => 'sedang_diproses'];
        $this->db->set($set);
        $this->db->where($this->where($id_pesanan));
        $this->db->update('tbl_pengguna_pesanan');
        return $this->db->affected_rows();
    }

    public function ajukanpencairandana($id_pesanan)
    {
        $set = ['status_pesanan' => 'menunggu_pencairan_dana'];
        $this->db->set($set);
        $this->db->where($this->where($id_pesanan));
        $this->db->update('tbl_pengguna_pesanan');
        return $this->db->affected_rows();
    }

    public function where($id_pesanan)
    {
        $where = ['id_pesanan' => $id_pesanan];
        return $where;
    }
}
