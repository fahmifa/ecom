<?php

defined('BASEPATH') or exit('No direct script access allowed');

class M_penjualan extends CI_Model
{
    public function __construct()
    {
        $this->table = 'tbl_pengguna_pesanan';
        $this->column_order = array(null, 'nama_pengguna', 'nama_produk', 'kondisi', 'jumlah_pesanan', null, 'metode_pembayaran', 'status_pesanan', 'created_at', 'id_pesanan');
        $this->column_search = array('nama_pengguna', 'nama_produk');
        $this->order = array('created_at' => 'desc');
    }

    // Datatable produk settings start
    public function dataTablePesanan($postData, $section = 'pesanan_baru')
    {
        $this->_get_datatables_query($postData, $section);
        if ($postData['length'] != -1) {
            $this->db->limit($postData['length'], $postData['start']);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function countAll($section = 'pesanan_baru')
    {
        $this->_querypesanan();

        if ($section === 'pesanan_baru') {
            $this->db->where_in('tbl_pengguna_pesanan.status_pesanan', ["menunggu_pembayaran", "menunggu_verifikasi"]);
        } elseif ($section === 'pesanan_diproses') {
            $this->db->where('tbl_pengguna_pesanan.status_pesanan', 'sedang_diproses');
        } elseif ($section === 'pesanan_selesai') {
            $this->db->where_in('tbl_pengguna_pesanan.status_pesanan', ['pesanan_selesai', 'menunggu_pencairan_dana', 'dana_diterima_penjual']);
        } elseif ($section === 'dibatalkan') {
            $this->db->where('tbl_pengguna_pesanan.status_pesanan', 'dibatalkan');
        }
        return $this->db->count_all_results();
    }

    public function countFiltered($postData, $section = 'pesanan_baru')
    {
        $this->_get_datatables_query($postData, $section);
        $query = $this->db->get();
        return $query->num_rows();
    }

    private function _get_datatables_query($postData, $section)
    {
        $this->_querypesanan();

        if ($section === 'pesanan_baru') {
            $this->db->where_in('tbl_pengguna_pesanan.status_pesanan', ["menunggu_pembayaran", "menunggu_verifikasi"]);
            $this->column_search[] = 'status_pesanan';
        } elseif ($section === 'pesanan_diproses') {
            $this->db->where('tbl_pengguna_pesanan.status_pesanan', 'sedang_diproses');
        } elseif ($section === 'pesanan_selesai') {
            $this->db->where_in('tbl_pengguna_pesanan.status_pesanan', ['pesanan_selesai', 'menunggu_pencairan_dana', 'dana_diterima_penjual']);
            $this->column_search[] = 'status_pesanan';
        } elseif ($section === 'dibatalkan') {
            $this->db->where('tbl_pengguna_pesanan.status_pesanan', 'dibatalkan');
        } elseif ($section === 'semua') {
            $this->column_search[] = 'status_pesanan';
        }

        $i = 0;
        foreach ($this->column_search as $item) {
            if ($postData['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $postData['search']['value']);
                } elseif ($i === 2) {
                    $this->db->or_like($item, str_replace(" ", "_", strtolower(trim($postData['search']['value']))));
                } else {
                    $this->db->or_like($item, $postData['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($postData['order'])) {
            $this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    // Datatable produk settings end

    private function _getTokoWhereSession()
    {
        return $this->db->get_where('tbl_pengguna_toko', ['id_pengguna' => userdata('id_pengguna')])->row_array();
    }

    public function selectPesananWhere($id_pesanan)
    {
        $this->_querypesanan();
        $this->db->select("tbl_produk_gambar.gambar_produk, tbl_produk_post.deskripsi_produk, tbl_pengguna.alamat, tbl_pengguna.email, IFNULL(tbl_pengguna.nomor_hp, '-') AS nomor_hp, tbl_produk_post.stok, tbl_produk_post.terjual, tbl_produk_post.status_post");
        $this->db->join('tbl_produk_gambar', "tbl_produk_gambar.id_produk_post = tbl_produk_post.id_produk_post");
        $this->db->where('tbl_pengguna_pesanan.id_pesanan', $id_pesanan);
        $this->db->limit(1);

        $result = $this->db->get();

        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    private function _querypesanan()
    {
        $id_toko = $this->_getTokoWhereSession()['id_toko'];
        $joinProdukPost = "(SELECT * FROM tbl_produk_post WHERE id_toko = $id_toko) tbl_produk_post";
        $this->db->select("tbl_pengguna_pesanan.*, tbl_pengguna.nama_pengguna AS nama_pembeli, tbl_produk_post.nama_produk, tbl_produk_post.kondisi, tbl_produk_post.harga_produk");
        $this->db->from($this->table);
        $this->db->join($joinProdukPost, 'tbl_produk_post.id_produk_post = tbl_pengguna_pesanan.id_produk_post', 'FULL');
        $this->db->join('tbl_pengguna', 'tbl_pengguna.id_pengguna = tbl_pengguna_pesanan.id_pengguna');
        $this->db->where('tbl_produk_post.status_live', 'live');
    }
}
