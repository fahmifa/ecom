<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easy-loading/1.3.0/jquery.loading.js" integrity="sha512-l9jYjbia7nXf4ZpR3dFSAjOOygUAytRrqmT32a5cBZjVpIUdFgBzIPQPPhJ6gh/NwaIerUEsn3vkEVQzQExGag==" crossorigin="anonymous"></script>
<script src="<?= base_url('assets/js/global.js'); ?>"></script>
<script>
    $(document).ready(function() {

        $('button#submitubahpassword').click(function() {
            let data = $('form#formubahpassword').serialize();
            $('small#oldpassworderror').html('');
            $('small#newpassworderror').html('');
            $('small#newpassword2error').html('');
            $('#modalubahpassword').css('z-index', '1');
            ubahpassword(data);
        });

        function ubahpassword(data) {
            $('body').loading();
            $.ajax({
                url: `<?= base_url('pengguna/apiubahpassword'); ?>`,
                method: "POST",
                data: data
            }).done((res) => {
                console.log(res);
                let oldpassworderror = $('small#oldpassworderror');
                let newpassworderror = $('small#newpassworderror');
                let newpassword2error = $('small#newpassword2error');
                if (res.kode === '0') {
                    oldpassworderror.html(res.oldpassworderror);
                    newpassworderror.html(res.newpassworderror);
                    newpassword2error.html(res.newpassword2error);
                } else if (res.kode === '00') {
                    oldpassworderror.html(res.oldpassworderror);
                } else if (res.kode === '000') {
                    newpassworderror.html(res.newpassworderror);
                } else if (res.kode === '0000') {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Maaf sepertinya ada kesalahan pada server.'
                    });
                } else if (res.kode === '1') {
                    $('#modalubahpassword').modal('hide');
                    oldpassworderror.html('');
                    newpassworderror.html('');
                    newpassword2error.html('');
                    oldpassworderror.prev().val('');
                    newpassworderror.prev().val('');
                    newpassword2error.prev().val('');
                    Swal.fire({
                        icon: 'success',
                        title: 'INFO',
                        text: 'Password berhasil diperbarui.'
                    });
                }
            }).fail((res) => {
                $('#modalubahpassword').modal('hide');
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Maaf sepertinya ada kesalahan pada server.'
                });
            }).always(() => {
                $('body').loading('stop');
                $('#modalubahpassword').css('z-index', '1072');
            });
        }

        $('#modalubahpassword').on('hidden.bs.modal', function() {
            let oldpassworderror = $('small#oldpassworderror');
            let newpassworderror = $('small#newpassworderror');
            let newpassword2error = $('small#newpassword2error');
            oldpassworderror.html('');
            newpassworderror.html('');
            newpassword2error.html('');
            oldpassworderror.prev().val('');
            newpassworderror.prev().val('');
            newpassword2error.prev().val('');
        });

        $('input#eye').click(function() {
            let oldpassworderror = $('input#oldpassword');
            let newpassworderror = $('input#newpassword');
            let newpassword2error = $('input#newpassword2');

            if ($(this).is(':checked')) {
                oldpassworderror.attr('type', 'text');
                newpassworderror.attr('type', 'text');
                newpassword2error.attr('type', 'text');
            } else {
                oldpassworderror.attr('type', 'password');
                newpassworderror.attr('type', 'password');
                newpassword2error.attr('type', 'password');
            }
        });

    });
</script>
</body>

</html>