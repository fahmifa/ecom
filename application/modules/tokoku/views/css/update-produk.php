<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" />
<link rel="stylesheet" href="<?= base_url('assets/css/jquery-loading.css'); ?>" />
<style>
    .custom-pemilih-gambar {
        display: none;
    }

    .pemilih-gambar img {
        cursor: pointer;
    }

    .img-preview {
        min-height: 150px;
        max-height: 150px;
        object-fit: contain;
        object-position: center;
    }

    .select2-selection__rendered {
        line-height: 38px !important;
    }

    .select2-container .select2-selection--single {
        height: 38px !important;
    }

    .select2-selection__arrow {
        height: 38px !important;
    }
</style>