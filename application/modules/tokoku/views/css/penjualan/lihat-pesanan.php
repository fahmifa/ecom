<style>
    .img-preview {
        min-height: 250px;
        max-height: 250px;
        object-fit: contain;
        object-position: center;
    }

    .deskripsi {
        overflow: hidden;
        display: -webkit-box;
        -webkit-line-clamp: 5;
        -webkit-box-orient: vertical;
    }
</style>