<style>
    .primary {
        background: #008757 !important;
        background-color: #008757 !important;
    }

    .scroll-to-top {
        z-index: 3;
    }

    .page-item .page-link {
        color: #1cc88a;
    }

    .page-item.active .page-link {
        background-color: #1cc88a !important;
        border-color: #1cc88a !important;
    }

    .select2-selection__rendered {
        line-height: 38px !important;
    }

    .select2-container .select2-selection--single {
        height: 38px !important;
    }

    .select2-selection__arrow {
        height: 38px !important;
    }
</style>