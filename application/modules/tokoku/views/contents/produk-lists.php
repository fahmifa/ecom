<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">List Produk</h1>

    <?= $this->session->flashdata('msg'); ?>

    <!-- DataTable Produk -->
    <div class="card shadow mb-2">
        <div class="card-body">
            <table id="tableproduk" class="table" style="width: 100%;">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Produk</th>
                        <th>Harga Produk</th>
                        <th>Stok</th>
                        <th>Terjual</th>
                        <th>Kondisi Produk</th>
                        <th>Status Produk</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Bootstrap core JavaScript-->
<script src="<?= base_url('assets/assets-sb-admin/vendor/jquery/jquery.min.js') ?>"></script>
<script src="<?= base_url('assets/assets-sb-admin/vendor/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>

<!-- Core plugin JavaScript-->
<script src="<?= base_url('assets/assets-sb-admin/vendor/jquery-easing/jquery.easing.min.js') ?>"></script>

<!-- Custom scripts for all pages-->
<script src="<?= base_url('assets/assets-sb-admin/js/sb-admin-2.min.js') ?>"></script>