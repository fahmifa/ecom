<?php
$placeholderkategori = 'Sepatu Sneakers Pria Tokostore Kanvas Hitam Seri C28B
- Model simple
- Nyaman Digunakan
- Tersedia warna hitam
- Sole PVC (injection shoes) yang nyaman dan awet untuk digunakan sehari - hari

Bahan:
Upper: Semi Leather (kulit tidak pecah-pecah)
Sole: Premium Rubber Sole

Ukuran
39 : 25,5 cm
40 : 26 cm
41 : 26.5 cm
42 : 27 cm
43 : 27.5 - 28 cm

Edisi terbatas dari Tokostore dengan model baru dan trendy untukmu. Didesain untuk bisa dipakai dalam berbagai acara. Sangat nyaman saat dipakai sehingga dapat menunjang penampilan dan kepercayaan dirimu. Beli sekarang sebelum kehabisan!';
?>

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Tambah Produk</h1>

    <?= $this->session->flashdata('msg'); ?>

    <form method="post" action="" enctype="multipart/form-data">

        <!-- Upload Gambar -->
        <div class="card shadow mb-2">
            <div class="card-body">
                <h5 class="d-inline-block mr-2">Upload Produk</h5><span class="badge badge-light">Wajib</span>
                <p class="small m-0">
                    Format gambar .jpg .jpeg .png dan ukuran maximum 500kb/gambar.
                </p>

                <div class="row">
                    <div class="col-6 col-sm-4 col-md-3 mt-3 text-center">
                        <input type="file" accept=".jpg, .jpeg, .png" class="custom-pemilih-gambar" name="gambar[]" id="gambar1">
                        <div class="pemilih-gambar">
                            <div class="text-center p-2">
                                <img id="gambar1" class="img-fluid img-preview" src="https://via.placeholder.com/480x480.png?text=Gambar%20Produk%20" alt="gambar1">
                            </div>
                        </div>
                        <p>Gambar 1 (Utama)</p>
                    </div>
                    <div class="col-6 col-sm-4 col-md-3 mt-3 text-center">
                        <input type="file" accept=".jpg, .jpeg, .png" class="custom-pemilih-gambar" name="gambar[]" id="gambar2">
                        <div class="pemilih-gambar">
                            <div class="text-center p-2">
                                <img id="gambar2" class="img-fluid img-preview" src="https://via.placeholder.com/480x480.png?text=Gambar%20Produk%20" alt="gambar2">
                            </div>
                        </div>
                        <p>Gambar 2</p>
                    </div>
                    <div class="col-6 col-sm-4 col-md-3 mt-3 text-center">
                        <input type="file" accept=".jpg, .jpeg, .png" class="custom-pemilih-gambar" name="gambar[]" id="gambar3">
                        <div class="pemilih-gambar">
                            <div class="text-center p-2">
                                <img id="gambar3" class="img-fluid img-preview" src="https://via.placeholder.com/480x480.png?text=Gambar%20Produk%20" alt="gambar3">
                            </div>
                        </div>
                        <p>Gambar 3</p>
                    </div>
                    <div class="col-6 col-sm-4 col-md-3 mt-3 text-center">
                        <input type="file" accept=".jpg, .jpeg, .png" class="custom-pemilih-gambar" name="gambar[]" id="gambar4">
                        <div class="pemilih-gambar">
                            <div class="text-center p-2">
                                <img id="gambar4" class="img-fluid img-preview" src="https://via.placeholder.com/480x480.png?text=Gambar%20Produk%20" alt="gambar4">
                            </div>
                        </div>
                        <p>Gambar 4</p>
                    </div>
                </div>

            </div>
        </div>

        <!-- Informasi Produk -->
        <div class="card shadow mb-2">
            <div class="card-body">
                <h5 class="d-inline-block mr-2">Informasi Produk</h5><span class="badge badge-light">Wajib</span>

                <div class="form-group">
                    <label for="nama">Nama Produk</label>
                    <input id="nama" class="form-control" minlength="6" type="text" name="nama" placeholder="Contoh: Sepatu Pria (Jenis/Kategori Produk) + Tokostore (Merek) + Kanvas Hitam (Keterangan)" required value="<?= set_value('nama'); ?>">
                    <?= form_error('nama'); ?>
                </div>

                <div class="form-group">
                    <label for="idkategori">Kategori</label>
                    <select id="idkategori" class="form-control" name="idkategori" required>
                        <option></option>
                    </select>
                    <?= form_error('kategori'); ?>
                </div>
            </div>
        </div>

        <!-- Detail Produk -->
        <div class="card shadow mb-2">
            <div class="card-body">
                <h5 class="d-inline-block mr-2">Detail Produk</h5>

                <div class="form-group">
                    <label for="kondisi" class="m-0">Kondisi</label> <br>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="kondisi1" name="kondisi" class="custom-control-input" value="baru">
                        <label class="custom-control-label" for="kondisi1">Baru</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="kondisi2" name="kondisi" class="custom-control-input" value="bekas">
                        <label class="custom-control-label" for="kondisi2">Bekas</label>
                    </div>
                </div>

                <div class="form-group">
                    <label for="deskripsi">Deskripsi Produk</label>
                    <textarea id="deskripsi" class="form-control" name="deskripsi" rows="5" placeholder="<?= $placeholderkategori; ?>"></textarea>
                </div>
            </div>
        </div>

        <!-- Harga Produk -->
        <div class="card shadow mb-2">
            <div class="card-body">
                <h5 class="d-inline-block mr-2">Harga</h5><span class="badge badge-light">Wajib</span>

                <div class="form-group">
                    <label for="harga">Harga Satuan</label>
                    <input id="harga" class="form-control" type="text" name="harga" placeholder="Masukkan Harga" required value="<?= set_value('harga'); ?>">
                    <?= form_error('harga'); ?>
                </div>
            </div>
        </div>

        <!-- Pengelolaan Produk -->
        <div class="card shadow mb-2">
            <div class="card-body">
                <h5 class="d-inline-block mr-2">Pengelolaan Produk</h5><span class="badge badge-light">Wajib</span>

                <div class="form-group">
                    <label for="stok">Stok Produk</label>
                    <input id="stok" class="form-control" type="text" name="stok" placeholder="Masukkan jumlah stok" required value="<?= set_value('stok'); ?>">
                    <?= form_error('stok'); ?>
                </div>
            </div>
        </div>

        <div class="row no-gutters">
            <div class="col-12 col-md-3 col-lg-2">
                <a href="<?= base_url('tokoku'); ?>" class="btn btn-outline-secondary w-100 mt-1 mb-0 mb-md-4">Batal & Kembali</a>
            </div>
            <div class="col-12 col-md-3 col-lg-2 m-0 ml-sm-2">
                <button type="button" id="postProduk" class="btn btn-success w-100 mt-1 mb-4">Simpan</button>
            </div>
        </div>

    </form>


</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Bootstrap core JavaScript-->
<script src="<?= base_url('assets/assets-sb-admin/vendor/jquery/jquery.min.js') ?>"></script>
<script src="<?= base_url('assets/assets-sb-admin/vendor/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>

<!-- Core plugin JavaScript-->
<script src="<?= base_url('assets/assets-sb-admin/vendor/jquery-easing/jquery.easing.min.js') ?>"></script>

<!-- Custom scripts for all pages-->
<script src="<?= base_url('assets/assets-sb-admin/js/sb-admin-2.min.js') ?>"></script>

<!-- Page level plugins -->
<script src="<?= base_url('assets/assets-sb-admin/vendor/chart.js/Chart.min.js') ?>"></script>

<!-- Page level custom scripts -->
<!-- <script src="<?= base_url('assets/assets-sb-admin/js/demo/chart-area-demo.js') ?>"></script>
        <script src="<?= base_url('assets/assets-sb-admin/js/demo/chart-pie-demo.js') ?>"></script> -->