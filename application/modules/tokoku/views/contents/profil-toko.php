<!-- Begin Page Content -->
<div class="container-fluid">

    <?= $this->session->flashdata('msg'); ?>

    <form action="" method="POST" enctype="multipart/form-data">

        <!-- Profil Toko -->
        <div class="card shadow mb-2">
            <div class="card-body">
                <div class="text-center">
                    <input id="file-gambar" class="d-none" accept=".jpg, .jpeg, .png" type="file" name="gambar_toko">
                    <img id="gambar-toko" class="img-fluid rounded-circle border border-success p-2" src="<?= base_url('assets/pengguna/profil/' . userdata('gambar_pengguna')); ?>" alt="Gambar Profil Toko">
                    <button type="button" id="btn-pemilih-gambar" class="d-block mx-auto btn btn-success btn-sm mt-2 px-3"><i class="fas fa-camera fa-fw"></i></button>

                    <small>*Maksimal dimensi gambar 100x100 pixel dalam format setidaknya .jpg, .jpeg, atau .png. Apabila tidak mengikuti aturan akan tertolak secara otomatis oleh sistem.</small>
                </div>

                <div class="form-group">
                    <label for="namatoko">Nama Toko :</label>
                    <input id="namatoko" class="form-control" type="text" name="namatoko" required value="<?= $profil_toko['nama_toko']; ?>">
                    <?= form_error('namatoko'); ?>
                </div>

                <div class="form-group">
                    <label for="alamattoko">Alamat Toko :</label>
                    <textarea id="alamattoko" class="form-control" rows="3" name="alamattoko" required><?= $profil_toko['alamat_toko']; ?></textarea>
                    <?= form_error('alamattoko'); ?>
                </div>

                <div class="form-group">
                    <label for="deskripsitoko">Deksripsi Toko :</label>
                    <textarea id="deskripsitoko" class="form-control" rows="3" name="deskripsitoko"><?= $profil_toko['deskripsi_toko']; ?></textarea>
                    <?= form_error('deskripsitoko'); ?>
                </div>

                <div class="form-group">
                    <label>Dibuat Pada :</label>
                    <input class="form-control" type="text" disabled value="<?= date("d/m/Y H:i:s", strtotime($profil_toko['created_at'])); ?>">
                </div>
            </div>
        </div>

        <div class="row no-gutters">
            <div class="col-12 col-md-3 col-lg-2">
                <a href="<?= base_url('tokoku'); ?>" class="btn btn-outline-secondary w-100 mt-1 mb-0 mb-md-4">Kembali</a>
            </div>
            <div class="col-12 col-md-3 col-lg-2 m-0 ml-sm-2">
                <button type="submit" class="btn btn-success w-100 mt-1 mb-4">Simpan</button>
            </div>
        </div>

    </form>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Bootstrap core JavaScript-->
<script src="<?= base_url('assets/assets-sb-admin/vendor/jquery/jquery.min.js') ?>"></script>
<script src="<?= base_url('assets/assets-sb-admin/vendor/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>

<!-- Core plugin JavaScript-->
<script src="<?= base_url('assets/assets-sb-admin/vendor/jquery-easing/jquery.easing.min.js') ?>"></script>

<!-- Custom scripts for all pages-->
<script src="<?= base_url('assets/assets-sb-admin/js/sb-admin-2.min.js') ?>"></script>