<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">List Pesanan Baru</h1>

    <!-- Upload Gambar -->
    <div class="card shadow mb-2">
        <div class="card-body pt-0">
            <!-- Menu Bar -->
            <div class="overflow-auto mb-3" style="margin: 0 -20px;">
                <div class="d-flex border-bottom">
                    <a href="<?= base_url('tokoku/penjualan/semuapesanan'); ?>" class="card-link text-body py-3">
                        <div class="col-auto text-center">Semua Pesanan</div>
                    </a>
                    <a href="<?= base_url('tokoku/penjualan/pesananbaru'); ?>" class="card-link text-success border-bottom-success py-3">
                        <div class="col-auto text-center">Pesanan Baru</div>
                    </a>
                    <a href="<?= base_url('tokoku/penjualan/pesanandiproses'); ?>" class="card-link text-body py-3">
                        <div class="col-auto text-center">Pesanan Diproses</div>
                    </a>
                    <a href="<?= base_url('tokoku/penjualan/pesananselesai'); ?>" class="card-link text-body py-3">
                        <div class="col-auto text-center">Pesanan Selesai</div>
                    </a>
                    <a href="<?= base_url('tokoku/penjualan/pesanandibatalkan'); ?>" class="card-link text-body py-3">
                        <div class="col-auto text-center">Pesanan Dibatalkan</div>
                    </a>
                </div>
            </div>
            <!-- End Menu Bar -->

            <!-- Table Pesanan -->
            <table id="tablepesananbaru" class="table" style="width: 100%;">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Pembeli</th>
                        <th>Nama Produk</th>
                        <th>Kondisi Produk</th>
                        <th>Jumlah</th>
                        <th>Total Harga</th>
                        <th>Metode Pembayaran</th>
                        <th>Status Pesanan</th>
                        <th>Tanggal Pesanan</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <!-- End Table Pesanan -->

        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Bootstrap core JavaScript-->
<script src="<?= base_url('assets/assets-sb-admin/vendor/jquery/jquery.min.js') ?>"></script>
<script src="<?= base_url('assets/assets-sb-admin/vendor/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>

<!-- Core plugin JavaScript-->
<script src="<?= base_url('assets/assets-sb-admin/vendor/jquery-easing/jquery.easing.min.js') ?>"></script>

<!-- Custom scripts for all pages-->
<script src="<?= base_url('assets/assets-sb-admin/js/sb-admin-2.min.js') ?>"></script>

<!-- Page level plugins -->
<script src="<?= base_url('assets/assets-sb-admin/vendor/chart.js/Chart.min.js') ?>"></script>

<!-- Page level custom scripts -->
<!-- <script src="<?= base_url('assets/assets-sb-admin/js/demo/chart-area-demo.js') ?>"></script>
        <script src="<?= base_url('assets/assets-sb-admin/js/demo/chart-pie-demo.js') ?>"></script> -->