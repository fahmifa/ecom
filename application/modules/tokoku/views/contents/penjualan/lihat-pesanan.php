<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Lihat Pesanan</h1>

    <!-- Data Produk -->
    <div class="card shadow mb-2">
        <div class="card-body">
            <div class="card p-3">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="<?= base_url("assets/pengguna/toko/produk/" . $data_pesanan[0]['gambar_produk']); ?>" class="card-img img-preview" alt="Gambar Produk">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body h-100 p-0 p-sm-3">
                            <div class="row align-content-between no-gutters h-100">
                                <div class="col-12">
                                    <h5 class="card-title"><?= $data_pesanan[0]['nama_produk']; ?></h5>
                                    <p class="card-text deskripsi"><?= $data_pesanan[0]['deskripsi_produk']; ?></p>
                                    <div class="row">
                                        <div class="col-auto"><small>Terjual: <?= $data_pesanan[0]['terjual']; ?></small></div>
                                        <div class="col-auto"><small>Stok: <?= $data_pesanan[0]['stok']; ?></small></div>
                                    </div>
                                </div>
                                <div class="col-12 text-center text-sm-left">
                                    <a href="<?= base_url(); ?>" target="blank" class="btn btn-outline-success">Lihat Produk</a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Data Pembeli -->
    <div class="card shadow mb-2">
        <div class="card-body">
            <h5 class="d-inline-block mr-2">Data Pembeli</h5>

            <div class="form-group">
                <label>Nama Pembeli</label>
                <input class="form-control" value="<?= $data_pesanan[0]['nama_pembeli']; ?>" readonly>
            </div>

            <div class="form-group">
                <label>Email Pembeli</label>
                <input class="form-control" value="<?= $data_pesanan[0]['email']; ?>" readonly>
            </div>

            <div class="form-group">
                <label>Email Pembeli</label>
                <input class="form-control" value="<?= $data_pesanan[0]['nomor_hp']; ?>" readonly>
            </div>

            <div class="form-group">
                <label>Alamat Pembeli</label>
                <textarea class="form-control" rows="3" readonly><?= $data_pesanan[0]['alamat']; ?></textarea>
            </div>
        </div>
    </div>

    <!-- Data Pesanan -->
    <div class="card shadow mb-2">
        <div class="card-body">
            <h5 class="d-inline-block mr-2">Data Pesanan</h5>

            <div class="form-group">
                <label>Jumlah Pesanan</label>
                <input class="form-control" value="<?= $data_pesanan[0]['jumlah_pesanan']; ?>" readonly>
            </div>

            <div class="form-group">
                <label>Total Harga</label>
                <input class="form-control" value="<?= formatNumber(($data_pesanan[0]['jumlah_pesanan'] * $data_pesanan[0]['harga_produk']), "Rp"); ?>" readonly>
            </div>

            <div class="form-group">
                <label>Metode Pembayaran</label>
                <input class="form-control" value="<?= ucwords(str_replace("_", " ", $data_pesanan[0]['metode_pembayaran'])); ?>" readonly>
            </div>

            <div class="form-group">
                <label>Catatan Pesanan</label>
                <textarea class="form-control" rows="3" readonly><?= $data_pesanan[0]['catatan_pesanan']; ?></textarea>
            </div>

            <div class="form-group">
                <label>Status Pesanan</label>
                <input class="form-control" value="<?= ucwords(str_replace("_", " ", $data_pesanan[0]['status_pesanan'])); ?>" readonly>
            </div>
        </div>
    </div>

    <div class="row no-gutters">
        <div class="col-12 col-md-3 col-lg-2">
            <button type="button" onclick="history.back()" class="btn btn-outline-secondary w-100 mt-1 mb-0 mb-md-4">Kembali</button>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Bootstrap core JavaScript-->
<script src="<?= base_url('assets/assets-sb-admin/vendor/jquery/jquery.min.js') ?>"></script>
<script src="<?= base_url('assets/assets-sb-admin/vendor/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>

<!-- Core plugin JavaScript-->
<script src="<?= base_url('assets/assets-sb-admin/vendor/jquery-easing/jquery.easing.min.js') ?>"></script>

<!-- Custom scripts for all pages-->
<script src="<?= base_url('assets/assets-sb-admin/js/sb-admin-2.min.js') ?>"></script>

<!-- Page level plugins -->
<script src="<?= base_url('assets/assets-sb-admin/vendor/chart.js/Chart.min.js') ?>"></script>