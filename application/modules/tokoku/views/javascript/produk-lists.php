<script src="<?= base_url('assets/js/global.js'); ?>"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {

        let initTableProduk = $('table#tableproduk').DataTable({
            scrollX: true,
            "processing": true,
            "serverSide": true,
            "order": [
                ["1", 'asc']
            ],
            "ajax": {
                "url": "<?php echo base_url('tokoku/apidatatableproduk'); ?>",
                "type": "POST"
            },
            "columnDefs": [{
                    "targets": [0],
                    "orderable": false
                },
                {
                    "targets": [7],
                    "orderable": false,
                    "render": function(id_produk) {
                        let link = `<?= base_url('tokoku'); ?>`;
                        let elm = `
                            <div class="d-flex flex-nowrap">
                                <a href="${ link }/produk/${ id_produk }" class="btn btn-outline-secondary btn-sm mx-1" title="Lihat Produk"><i class="fas fa-eye"></i></a>
                                <a href="${ link }/updateproduk/${ id_produk }" class="btn btn-success btn-sm mx-1" title="Update Produk"><i class="fas fa-pen"></i></a>
                            </div>
                        `;
                        return elm;
                    }
                },
            ]
        });

        $('#tableproduk_filter input').unbind();
        $('#tableproduk_filter input').bind('keyup', function(e) {
            if (e.keyCode == 13) {
                initTableProduk.search($(this).val()).draw();
            }
        });

    });
</script>