<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easy-loading/1.3.0/jquery.loading.js" integrity="sha512-l9jYjbia7nXf4ZpR3dFSAjOOygUAytRrqmT32a5cBZjVpIUdFgBzIPQPPhJ6gh/NwaIerUEsn3vkEVQzQExGag==" crossorigin="anonymous"></script>
<script src="<?= base_url('assets/js/global.js'); ?>"></script>
<script>
    $(document).ready(function() {
        let baseUrl = "<?= base_url('tokoku'); ?>";

        let initTablePesanan = $('table#tablepesananselesai').DataTable({
            scrollX: true,
            "processing": true,
            "serverSide": true,
            "order": [
                ["8", 'desc']
            ],
            "ajax": {
                "url": `${ baseUrl }/penjualan/apilistpesananselesai`,
                "type": "POST"
            },
            "columnDefs": [{
                    "targets": [0],
                    "orderable": false
                },
                {
                    "targets": [5],
                    "orderable": false
                },
                {
                    "targets": [9],
                    "orderable": false,
                    "render": function(id_pesanan, type, data) {
                        let status_pesanan = data[10];
                        let elm = `
                            <div class="d-flex flex-nowrap">
                                <a href="${ baseUrl }/penjualan/lihatpesanan/${ id_pesanan }" class="btn btn-success btn-sm mx-1" title="Lihat Pesanan"><i class="fas fa-eye fa-fw"></i></a>
                        `;

                        if (status_pesanan === 'pesanan_selesai') {
                            elm += `<button type="button" class="btn btn-success btn-sm mx-1" id="ajukan_pencairan_dana" title="Ajukan Pencairan Dana"><i class="fas fa-dollar-sign fa-fw"></i></button>`;
                        }

                        return elm;
                    }
                },
            ]
        });

        $('#tablepesananselesai_filter').html($('#tablepesananselesai_filter input'));
        $('#tablepesananselesai_filter input').unbind();
        $('#tablepesananselesai_filter input').attr('placeholder', "Cari Nama Produk, Pembeli, atau Status Pesanan disini").addClass('w-100 m-0');
        $('#tablepesananselesai_filter input').bind('keyup', function(e) {
            if (e.keyCode == 13) {
                initTablePesanan.search($(this).val()).draw();
            }
        });

        $(document).on('click', 'button#ajukan_pencairan_dana', function() {
            let id_pesanan = initTablePesanan.row($(this).closest('td')).data()[9];

            Swal.fire({
                title: 'Ajukan Pencairan Dana?',
                text: "Dana akan dicairkan 1 minggu setelah admin menyetujui.",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#17A673',
                cancelButtonText: 'Tutup',
                confirmButtonText: 'Ajukan Sekarang!',
                reverseButtons: true,
                customClass: {
                    confirmButton: 'btn btn-success',
                }
            }).then((result) => {
                if (result.isConfirmed) {
                    AjukanPencairanDana(id_pesanan);
                }
            });
        });

        function AjukanPencairanDana(id_pesanan) {
            loadingStart();
            $.ajax({
                url: `${ baseUrl }/api_action/ajukanpencairandana`,
                method: "POST",
                data: {
                    id_pesanan
                },
            }).done((res) => {
                if (res.kode) {
                    location.reload();
                } else {
                    sweetalert('error', 'Oops...', 'Pengajuan Pencairan Dana gagal, silahkan muat ulang halaman dan coba lagi!');
                }
            }).fail(() => {
                sweetalert('error', 'Oops...', 'Ada kesalahan pada server!');
            }).always(() => {
                loadingStop();
            });
        }

    });
</script>