<script src="<?= base_url('assets/js/global.js'); ?>"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {

        let initTablePesanan = $('table#tablepesanandibatalkan').DataTable({
            scrollX: true,
            "processing": true,
            "serverSide": true,
            "order": [
                ["8", 'desc']
            ],
            "ajax": {
                "url": "<?php echo base_url('tokoku/penjualan/apilistpesanandibatalkan'); ?>",
                "type": "POST"
            },
            "columnDefs": [{
                    "targets": [0],
                    "orderable": false
                },
                {
                    "targets": [5],
                    "orderable": false
                },
                {
                    "targets": [9],
                    "orderable": false,
                    "render": function(id_pesanan) {
                        let link = `<?= base_url('tokoku'); ?>`;
                        let elm = `
                            <div class="d-flex flex-nowrap">
                                <a href="${ link }/penjualan/lihatpesanan/${ id_pesanan }" class="btn btn-success btn-sm mx-1" title="Lihat Pesanan"><i class="fas fa-eye fa-fw"></i></a>
                            </div>
                        `;
                        return elm;
                    }
                },
            ]
        });

        $('#tablepesanandibatalkan_filter').html($('#tablepesanandibatalkan_filter input'));
        $('#tablepesanandibatalkan_filter input').unbind();
        $('#tablepesanandibatalkan_filter input').attr('placeholder', "Cari Nama Produk, Pembeli disini").addClass('w-100 m-0');
        $('#tablepesanandibatalkan_filter input').bind('keyup', function(e) {
            if (e.keyCode == 13) {
                initTablePesanan.search($(this).val()).draw();
            }
        });

    });
</script>