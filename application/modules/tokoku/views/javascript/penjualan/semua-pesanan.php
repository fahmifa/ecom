<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easy-loading/1.3.0/jquery.loading.js" integrity="sha512-l9jYjbia7nXf4ZpR3dFSAjOOygUAytRrqmT32a5cBZjVpIUdFgBzIPQPPhJ6gh/NwaIerUEsn3vkEVQzQExGag==" crossorigin="anonymous"></script>
<script src="<?= base_url('assets/js/global.js'); ?>"></script>
<script>
    $(document).ready(function() {
        let baseUrl = "<?= base_url('tokoku'); ?>";

        let initTablePesanan = $('table#tablesemuapesanan').DataTable({
            scrollX: true,
            "processing": true,
            "serverSide": true,
            "order": [
                ["8", 'desc']
            ],
            "ajax": {
                "url": `${ baseUrl }/penjualan/apilistsemuapesanan`,
                "type": "POST"
            },
            "columnDefs": [{
                    "targets": [0],
                    "orderable": false
                },
                {
                    "targets": [5],
                    "orderable": false
                },
                {
                    "targets": [9],
                    "orderable": false,
                    "render": function(id_pesanan, type, data) {
                        let status_pesanan = data[10];
                        let status_spesifik = [
                            ['menunggu_pembayaran', 'sedang_diproses', 'menunggu_verifikasi'],
                            ['menunggu_pembayaran', 'menunggu_verifikasi']
                        ];
                        let elm = `<div class="d-flex flex-nowrap">`
                        if (status_spesifik[0].includes(status_pesanan)) elm += `<button type="button" id="batalkan-pesanan" class="btn btn-outline-danger btn-sm mx-1" title="Batalkan Pesanan"><i class="fas fa-times fa-fw"></i></button>`;
                        elm += `<a href="${ baseUrl }/penjualan/lihatpesanan/${ id_pesanan }" class="btn btn-success btn-sm mx-1" title="Lihat Pesanan"><i class="fas fa-eye fa-fw"></i></a>`;
                        if (status_spesifik[1].includes(status_pesanan)) elm += `<button type="button" id="prosespesanan" class="btn btn-warning btn-sm mx-1" title="Proses Pesanan"><i class="fas fa-box fa-fw"></i></button>`;
                        if (status_pesanan === 'pesanan_selesai') elm += `<button type="button" class="btn btn-success btn-sm mx-1" id="ajukan_pencairan_dana" title="Ajukan Pencairan Dana"><i class="fas fa-dollar-sign fa-fw"></i></button>`;
                        elm += `</div>`;

                        return elm;
                    }
                },
            ]
        });

        $('#tablesemuapesanan_filter').html($('#tablesemuapesanan_filter input'));
        $('#tablesemuapesanan_filter input').unbind();
        $('#tablesemuapesanan_filter input').attr('placeholder', "Cari Nama Produk, Pembeli, atau Status Pesanan disini").addClass('w-100 m-0');
        $('#tablesemuapesanan_filter input').bind('keyup', function(e) {
            if (e.keyCode == 13) {
                initTablePesanan.search($(this).val()).draw();
            }
        });

        $(document).on('click', 'button#batalkan-pesanan', function() {
            let id_pesanan = initTablePesanan.row($(this).closest('td')).data()[9];

            Swal.fire({
                title: 'Apakah Anda yakin?',
                text: "Keputusan tidak dapat dikembalikan!",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonText: 'Tutup',
                confirmButtonText: 'Batalkan Pesanan!',
                reverseButtons: true,
                customClass: {
                    confirmButton: 'btn btn-danger',
                }
            }).then((result) => {
                if (result.isConfirmed) {
                    batalkanPesanan(id_pesanan);
                }
            });
        });

        function batalkanPesanan(id_pesanan) {
            loadingStart();
            $.ajax({
                url: `${baseUrl}/api_action/batalkanpesanan`,
                method: "POST",
                data: {
                    id_pesanan
                },
            }).done((res) => {
                if (res.kode) {
                    location.reload();
                } else {
                    sweetalert('error', 'Oops...', 'Membatalkan Pesanan gagal, silahkan muat ulang halaman dan coba lagi!');
                }
            }).fail(() => {
                sweetalert('error', 'Oops...', 'Ada kesalahan pada server!');
            }).always(() => {
                loadingStop();
            });
        }

        $(document).on('click', 'button#ajukan_pencairan_dana', function() {
            let id_pesanan = initTablePesanan.row($(this).closest('td')).data()[9];

            Swal.fire({
                title: 'Ajukan Pencairan Dana?',
                text: "Dana akan dicairkan 1 minggu setelah admin menyetujui.",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#17A673',
                cancelButtonText: 'Tutup',
                confirmButtonText: 'Ajukan Sekarang!',
                reverseButtons: true,
                customClass: {
                    confirmButton: 'btn btn-success',
                }
            }).then((result) => {
                if (result.isConfirmed) {
                    AjukanPencairanDana(id_pesanan);
                }
            });
        });

        function AjukanPencairanDana(id_pesanan) {
            loadingStart();
            $.ajax({
                url: `${ baseUrl }/api_action/ajukanpencairandana`,
                method: "POST",
                data: {
                    id_pesanan
                },
            }).done((res) => {
                if (res.kode) {
                    location.reload();
                } else {
                    sweetalert('error', 'Oops...', 'Pengajuan Pencairan Dana gagal, silahkan muat ulang halaman dan coba lagi!');
                }
            }).fail(() => {
                sweetalert('error', 'Oops...', 'Ada kesalahan pada server!');
            }).always(() => {
                loadingStop();
            });
        }

        $(document).on('click', 'button#prosespesanan', function() {
            let id_pesanan = initTablePesanan.row($(this).closest('td')).data()[9];
            Swal.fire({
                title: 'Apakah Anda yakin?',
                text: "Status Pesanan akan di ubah ke Proses dan akan diberitahukan ke pembeli.",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#17A673',
                cancelButtonText: 'Tutup',
                confirmButtonText: 'Lanjutkan!',
                reverseButtons: true,
                customClass: {
                    confirmButton: 'btn btn-success',
                }
            }).then((result) => {
                if (result.isConfirmed) {
                    prosesPesanan(id_pesanan);
                }
            });
        });

        function prosesPesanan(id_pesanan) {
            loadingStart();
            $.ajax({
                url: `${ baseUrl }/api_action/prosespesanan`,
                method: "POST",
                data: {
                    id_pesanan
                },
            }).done((res) => {
                if (res.kode) {
                    location.reload();
                } else {
                    sweetalert('error', 'Oops...', 'Status Pesanan gagal diubah ke Proses, silahkan muat ulang halaman dan coba lagi!');
                }
            }).fail(() => {
                sweetalert('error', 'Oops...', 'Ada kesalahan pada server!');
            }).always(() => {
                loadingStop();
            });
        }

    });
</script>