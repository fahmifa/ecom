<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easy-loading/1.3.0/jquery.loading.js" integrity="sha512-l9jYjbia7nXf4ZpR3dFSAjOOygUAytRrqmT32a5cBZjVpIUdFgBzIPQPPhJ6gh/NwaIerUEsn3vkEVQzQExGag==" crossorigin="anonymous"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="<?= base_url('assets/js/global.js'); ?>"></script>
<script>
    $(document).ready(function() {
        let baseUrl = "<?= base_url('tokoku'); ?>";

        let initTablePesanan = $('table#tablepesananbaru').DataTable({
            scrollX: true,
            "processing": true,
            "serverSide": true,
            "order": [
                ["8", 'desc']
            ],
            "ajax": {
                "url": `${baseUrl}/penjualan/apilistpesananbaru`,
                "type": "POST"
            },
            "columnDefs": [{
                    "targets": [0],
                    "orderable": false
                },
                {
                    "targets": [5],
                    "orderable": false
                },
                {
                    "targets": [9],
                    "orderable": false,
                    "render": function(id_pesanan) {
                        let elm = `
                            <div class="d-flex flex-nowrap">
                                <button type="button" id="batalkan-pesanan" class="btn btn-outline-danger btn-sm mx-1" title="Batalkan Pesanan"><i class="fas fa-times fa-fw"></i></button>
                                <a href="${ baseUrl }/penjualan/lihatpesanan/${ id_pesanan }" class="btn btn-success btn-sm mx-1" title="Lihat Pesanan"><i class="fas fa-eye fa-fw"></i></a>
                                <button type="button" id="prosespesanan" class="btn btn-warning btn-sm mx-1" title="Proses Pesanan"><i class="fas fa-box fa-fw"></i></button>
                            </div>
                        `;
                        return elm;
                    }
                },
            ]
        });

        $('#tablepesananbaru_filter').html($('#tablepesananbaru_filter input'));
        $('#tablepesananbaru_filter input').unbind();
        $('#tablepesananbaru_filter input').attr('placeholder', "Cari Nama Produk, Pembeli, atau Status Pesanan disini").addClass('w-100');
        $('#tablepesananbaru_filter input').bind('keyup', function(e) {
            if (e.keyCode == 13) {
                initTablePesanan.search($(this).val()).draw();
            }
        });

        $(document).on('click', 'button#batalkan-pesanan', function() {
            let id_pesanan = initTablePesanan.row($(this).closest('td')).data()[9];
            Swal.fire({
                title: 'Apakah Anda yakin?',
                text: "Keputusan tidak dapat dikembalikan!",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonText: 'Tutup',
                confirmButtonText: 'Batalkan Pesanan!',
                reverseButtons: true,
                customClass: {
                    confirmButton: 'btn btn-danger',
                }
            }).then((result) => {
                if (result.isConfirmed) {
                    batalkanPesanan(id_pesanan);
                }
            });
        });

        function batalkanPesanan(id_pesanan) {
            loadingStart();
            $.ajax({
                url: `${baseUrl}/api_action/batalkanpesanan`,
                method: "POST",
                data: {
                    id_pesanan
                },
            }).done((res) => {
                if (res.kode) {
                    location.reload();
                } else {
                    sweetalert('error', 'Oops...', 'Membatalkan Pesanan gagal, silahkan muat ulang halaman dan coba lagi!');
                }
            }).fail(() => {
                sweetalert('error', 'Oops...', 'Ada kesalahan pada server!');
            }).always(() => {
                loadingStop();
            });
        }

        $(document).on('click', 'button#prosespesanan', function() {
            let id_pesanan = initTablePesanan.row($(this).closest('td')).data()[9];
            Swal.fire({
                title: 'Apakah Anda yakin?',
                text: "Status Pesanan akan di ubah ke Proses dan akan diberitahukan ke pembeli.",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#17A673',
                cancelButtonText: 'Tutup',
                confirmButtonText: 'Lanjutkan!',
                reverseButtons: true,
                customClass: {
                    confirmButton: 'btn btn-success',
                }
            }).then((result) => {
                if (result.isConfirmed) {
                    prosesPesanan(id_pesanan);
                }
            });
        });

        function prosesPesanan(id_pesanan) {
            loadingStart();
            $.ajax({
                url: `${ baseUrl }/api_action/prosespesanan`,
                method: "POST",
                data: {
                    id_pesanan
                },
            }).done((res) => {
                if (res.kode) {
                    location.reload();
                } else {
                    sweetalert('error', 'Oops...', 'Status Pesanan gagal diubah ke Proses, silahkan muat ulang halaman dan coba lagi!');
                }
            }).fail(() => {
                sweetalert('error', 'Oops...', 'Ada kesalahan pada server!');
            }).always(() => {
                loadingStop();
            });
        }

    });
</script>