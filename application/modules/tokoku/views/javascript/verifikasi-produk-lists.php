<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easy-loading/1.3.0/jquery.loading.js" integrity="sha512-l9jYjbia7nXf4ZpR3dFSAjOOygUAytRrqmT32a5cBZjVpIUdFgBzIPQPPhJ6gh/NwaIerUEsn3vkEVQzQExGag==" crossorigin="anonymous"></script>
<script src="<?= base_url('assets/js/global.js'); ?>"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script>
    $(document).ready(function() {

        let initTableProduk = $('table#tableproduk').DataTable({
            scrollX: true,
            "processing": true,
            "serverSide": true,
            "order": ["1", 'asc'],
            "ajax": {
                "url": "<?php echo base_url('tokoku/apidatatableverifikasiproduk'); ?>",
                "type": "POST"
            },
            "columnDefs": [{
                    "targets": [0],
                    "orderable": false
                },
                {
                    "targets": [6],
                    "orderable": false,
                    "render": function(id_produk) {
                        let link = `<?= base_url('tokoku/updateproduk/'); ?>`;
                        let elm = `
                            <div class="d-flex flex-nowrap">
                                <button type="button" id="hapuspengajuanproduk" class="btn btn-outline-danger btn-sm mx-1" title="Batalkan Pengajuan"><i class="fas fa-trash"></i></button>
                                <a href="${ link + id_produk }" class="btn btn-success btn-sm mx-1" title="Update Produk"><i class="fas fa-pen"></i></a>
                            </div>
                        `;
                        return elm;
                    }
                },
            ]
        });

        $('#tableproduk_filter input').unbind();
        $('#tableproduk_filter input').bind('keyup', function(e) {
            if (e.keyCode == 13) {
                initTableProduk.search($(this).val()).draw();
            }
        });

        $(document).on('click', 'button#hapuspengajuanproduk', function() {
            let id_produk = initTableProduk.row($(this).closest('td')).data()['6'];

            Swal.fire({
                title: 'Apakah Anda yakin?',
                text: "Data yang sudah dibatalkan akan dihapus dan tidak dapat dipulihkan!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonText: 'Tutup',
                confirmButtonText: 'Hapus!',
                reverseButtons: true,
                customClass: {
                    confirmButton: 'btn btn-danger',
                }
            }).then((result) => {
                if (result.isConfirmed) {
                    deletePengajuanProduk(id_produk);
                }
            });
        });

        function deletePengajuanProduk(id_produk) {
            loadingStart();
            $.ajax({
                url: `<?= base_url('tokoku/apihapusproduk'); ?>`,
                method: "POST",
                data: {
                    id_produk
                }
            }).done((res) => {
                if (res.kode === '0') {
                    sweetalert('error', 'Oops...', 'Hapus Pengajuan Produk gagal, silahkan muat ulang halaman dan coba lagi!');
                } else {
                    location.reload()
                }
            }).fail(() => {
                sweetalert('error', 'Oops...', 'Ada kesalahan pada server!');
            }).always(() => {
                loadingStop();
            });
        }

    });
</script>