<script>
    $(document).ready(function() {
        let imgPlaceholder = `<?= base_url('assets/pengguna/profil/' . userdata('gambar_pengguna')); ?>`;

        $('button#btn-pemilih-gambar').click(function() {
            $('input#file-gambar').trigger('click');
        });

        $('input#file-gambar').change(function() {
            readURL(this, $(this));
        });

        function readURL(input, elm) {
            if (input.files.length > 0) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    elm.next().attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]); // convert to base64 string
            } else {
                elm.next().attr('src', imgPlaceholder);
            }
        }

    });
</script>