<script src="<?= base_url('assets/js/global.js'); ?>"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>
<script>
    $(document).ready(function() {
        let imgPlaceholder = `https://via.placeholder.com/480x480.png?text=Gambar%20Produk%20`,
            dataKategoriSelect2 = <?= json_encode($kategori); ?>;

        dataKategoriSelect2.push({
            id: 12345,
            text: "Lainnya"
        });

        $('.pemilih-gambar').click(function() {
            $(this).prev().trigger('click');
        });

        $('input.custom-pemilih-gambar').change(function() {
            readURL(this, $(this));
        });

        function readURL(input, elm) {
            if (input.files.length > 0) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    elm.next().find('img').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]); // convert to base64 string
            } else {
                elm.next().find('img').attr('src', imgPlaceholder);
            }
        }

        $('select#idkategori').select2({
            placeholder: 'Pilih Kategori Produk',
            data: dataKategoriSelect2,
            width: '100%',
            cache: true
        });

        $('input#harga').keyup(function() {
            $(this).val(`Rp${formatNumber(this.value)}`);
        });

        $('input#stok').keyup(function() {
            $(this).val(formatNumber(this.value));
        });

        $('button#postProduk').click(function() {
            let gambar_upload_valid = $('input.custom-pemilih-gambar').filter(function() {
                let filesLength = this.files.length;
                return filesLength === 1;
            }).length;
            $(this).attr('type', 'button');

            if ($('img#gambar1').attr('src') === imgPlaceholder) {
                Swal.fire({
                    icon: 'warning',
                    title: 'Oops...',
                    text: 'Mohon Pilih Gambar Produk Utama terlebih dahulu!',
                });
            } else if (gambar_upload_valid > 0 && $('img#gambar1').attr('src') !== imgPlaceholder) {
                if ($('select#idkategori').val() === '') {
                    Swal.fire({
                        icon: 'warning',
                        title: 'Oops...',
                        text: 'Mohon pilih Kategori Produk terlebih dahulu!',
                    });
                } else {
                    $(this).attr('type', 'submit');
                }
            }
        });

    });
</script>