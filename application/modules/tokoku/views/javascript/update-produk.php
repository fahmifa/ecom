<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easy-loading/1.3.0/jquery.loading.js" integrity="sha512-l9jYjbia7nXf4ZpR3dFSAjOOygUAytRrqmT32a5cBZjVpIUdFgBzIPQPPhJ6gh/NwaIerUEsn3vkEVQzQExGag==" crossorigin="anonymous"></script>
<script src="<?= base_url('assets/js/global.js'); ?>"></script>
<script>
    $(document).ready(function() {
        let baseurl = `<?= base_url(); ?>`,
            imgPlaceholder = `https://via.placeholder.com/480x480.png?text=Gambar%20Produk%20`,
            dataKategoriSelect2 = <?= json_encode($kategori); ?>,
            id_produk_post = `<?= $this->uri->segment(3); ?>`;

        dataKategoriSelect2.push({
            id: 12345,
            text: "Lainnya"
        });



        $('.pemilih-gambar').click(function() {
            $(this).prev().trigger('click');
        });

        $('input.custom-pemilih-gambar').change(function() {
            readURL(this, $(this));
        });

        function readURL(input, elm) {
            if (input.files.length > 0) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    elm.next().find('img').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]); // convert to base64 string
            } else {
                elm.next().find('img').attr('src', imgPlaceholder);
            }
        }

        $('select#idkategori').select2({
            placeholder: 'Pilih Kategori Produk',
            data: dataKategoriSelect2,
            width: '100%',
            cache: true
        });

        $('select#idkategori').val(<?= $produk[0]['id_kategori']; ?>).trigger('change');

        $('input#harga').keyup(function() {
            $(this).val(`Rp${formatNumber(this.value)}`);
        });

        $('input#stok').keyup(function() {
            $(this).val(formatNumber(this.value));
        });

        $('button#updateProduk').click(function() {
            let gambar_upload_valid = $('input.custom-pemilih-gambar').filter(function() {
                let filesLength = this.files.length;
                return filesLength === 1;
            }).length;
            $(this).attr('type', 'button');

            if ($('img#gambar1').attr('src') === imgPlaceholder) {
                sweetalert('warning', 'Oops...', 'Mohon Pilih Gambar Produk Utama terlebih dahulu!');
            } else if ($('select#idkategori').val() === '') {
                sweetalert('warning', 'Oops...', 'Mohon pilih Kategori Produk terlebih dahulu!');
            } else {
                $(this).attr('type', 'submit');
            }
        });

        $('button#delete-gambar').click(function() {
            let id_gambar = $(this).parent().find('img').attr('id');
            deleteGambarProduk(id_gambar);
        });

        function deleteGambarProduk(id_produk_gambar) {
            let data = {
                id_produk_gambar,
                id_produk_post
            };

            loadingStart();
            $.ajax({
                url: `${baseurl}tokoku/apideletegambar`,
                method: "POST",
                data: data
            }).done((res) => {
                if (res.kode === '1') {
                    let imgElm = $(`img#${ id_produk_gambar }`);
                    let Toast = Swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 5000,
                        timerProgressBar: true,
                    })
                    imgElm.attr('src', imgPlaceholder);
                    imgElm.closest('.col-6').find('button#delete-gambar').remove();

                    Toast.fire({
                        icon: 'success',
                        title: 'Gambar Produk telah berhasil dihapus!'
                    })
                } else {
                    sweetalert('error', 'Oops...', 'Hapus Gambar Produk gagal!');
                }
            }).fail(() => {
                sweetalert('error', 'Oops...', 'Ada kesalahan pada server!');
            }).always(() => {
                loadingStop();
            });
        };

    });
</script>